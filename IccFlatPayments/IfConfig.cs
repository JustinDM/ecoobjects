using System;
using System.Text.Json;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.IccFlatPayments
{
    
    public class IfConfig : Config
    {
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string ReportSender { get; set; }
        public string[] ReviewerEmails { get; set; }
        public string NoteText { get; set; }
        public string ResourceFolder { get; set; }
        public string ReportName { get;set; }
        public int MaxProcessCount { get; set; }
        public string ReportEmailSubject { get; set; }
        public string CdsQueueName { get; set; }
        public string FamilyQueueName { get; set; }
        public Timeout Timeout { get; set; }
        public Guid? TimeoutId { get; set; }
        public string ReferenceText { get; set; }

        public IfConfig() { }

        public IfConfig(string fileText) 
        {
            var config = IngestConfigFile(fileText);

            Duplicate(config);
        }
        
        public override void Duplicate(Config config)
        {
            var ifConfig = config as IfConfig;

            DelayBefore = ifConfig.DelayBefore;
            DelayAfter = ifConfig.DelayAfter;
            ReportSender = ifConfig.ReportSender;
            ReviewerEmails = ifConfig.ReviewerEmails;
            NoteText = ifConfig.NoteText;
            Timeout = ifConfig.Timeout;
            ResourceFolder = ifConfig.ResourceFolder;
            ReportName = String.Format(ifConfig.ReportName, DateTime.Now.ToString(Configurations.ReportDateTimeFormat));
            MaxProcessCount = ifConfig.MaxProcessCount;
            ReportEmailSubject = ifConfig.ReportEmailSubject;
            CdsQueueName = ifConfig.CdsQueueName;
            FamilyQueueName = ifConfig.FamilyQueueName;
            ReferenceText = ifConfig.ReferenceText;
        }

        public override Config IngestConfigFile(string fileText)
        {
            var config = JsonSerializer.Deserialize<IfConfig>(fileText);

            return config;
        }
    }
}