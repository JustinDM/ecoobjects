using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Economical.EcoObjects.General.Address;
using Economical.EcoObjects.General.Address.Enums;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.IccFlatPayments
{
    public class IfUnitInfo : UnitInfo
    {
        public double ClaimNumber { get; set; }
        public string ClaimModifier { get; set; }
        public string InvoiceId { get; set; }
        public string Insured { get; set; }
        public EcoAddress Address { get; set; }
        // this will cause storage issues - make new table
        // or we store it like an array
        public DateTime DateOfLoss { get; set; }
        public double Amount { get; set; }

        public IfUnitInfo() { }

        // need a constructor for both reconstruction from the report
        // do we? Where do we see this happening, honestly
        // nowhere, really

        // and one for ingestion

        public IfUnitInfo(DataRow row) 
        {
            var claimRow = row["Claim"].ToString();
            
            if (string.IsNullOrWhiteSpace(claimRow))
            {
                ClaimNumber = 0;
            }
            else
            {
                ClaimNumber = Convert.ToDouble(row["Claim"].ToString());
            }

            InvoiceId = (row["InvoiceID"].ToString());
            Insured = row["Insured"].ToString();
            Address = new EcoAddress() { City = row["Insured_City"].ToString() };
            Address.AddressLines.Add(row["Insured_Address"].ToString());
            
            var dateOfLossField = row["Date_of_Loss"].ToString();

            if (!string.IsNullOrWhiteSpace(dateOfLossField)) 
            {
                DateOfLoss = DateTime.ParseExact(
                    row["Date_of_Loss"].ToString(), 
                    "yyyy-MM-dd", 
                    CultureInfo.InvariantCulture);
            }

            Amount = Convert.ToDouble(row["Total"].ToString().Replace("$", ""));

            if (Address.Province == Province.QC)
            {
                ClaimModifier = "-Bien-PD-1";
            }

            else
            {
                ClaimModifier = "-Prop-PD-1"; // something like that
            }

        }
        
        public override void CleanData()
        {
            Insured = "REDACTED";
        }

        public override Dictionary<string, string> GenerateReportRow()
        {
            var reportInfo = new Dictionary<string, string>()
            {
                { "ClaimNumber", ClaimNumber.ToString() },
                { "ClaimModifier", ClaimModifier },
                { "InvoiceID", InvoiceId },
                { "Insured", Insured },
                { "StreetAddress", Address.AddressLines[0] },
                { "City", Address.City },
                { "DateOfLoss", DateOfLoss.ToString(Configurations.PrettyDateFormat)},
                { "Amount", Amount.ToString("0.00") }
            };

            return reportInfo;
        }

        public bool IsCdsClaim()
        {
            if (ClaimNumber >= 1000000)
            {
                return true;
            }

            return false;
        }

        public override void IngestReportRow(DataRow row)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, string> GenerateFrenchReportRow()
        {
            throw new NotImplementedException();
        }
    }
}