using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.IccFlatPayments.DTOs;

namespace Economical.EcoObjects.IccFlatPayments
{
    public class IfMapperProfiles : MapperProfiles
    {
        public IfMapperProfiles() : base()
        {
            CreateMap<IfConfig, IfInboundConfigDto>();

            CreateMap<IfUnitInfo, IfInboundUnitInfoDto>();

            CreateMap<IfInboundConfigDto, IfConfig>();

            CreateMap<IfInboundUnitInfoDto, IfUnitInfo>();

            CreateMap<IfUpdateUnitInfoDto, IfUnitInfo>();

            CreateMap<IfUnitInfo, IfOutboundUnitInfoDto>();

            CreateMap<IfConfig, IfOutboundConfigDto>();
        }
    }
}