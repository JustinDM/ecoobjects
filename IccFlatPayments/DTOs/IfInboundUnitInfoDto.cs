using System;
using Economical.EcoObjects.General.Address;
using Economical.EcoObjects.General.DTOs;

namespace Economical.EcoObjects.IccFlatPayments.DTOs
{
    public class IfInboundUnitInfoDto : InboundUnitInfoDto
    {
        public double ClaimNumber { get; set; }
        public string ClaimModifier { get; set; }
        public string InvoiceId { get; set; }
        public string Insured { get; set; }
        // will we even be able to send it like this?
        public EcoAddress Address { get; set; }
        public DateTime DateOfLoss { get; set; }
        public double Amount { get; set; }
    }
}