using System;
using Economical.EcoObjects.General.Address;

namespace Economical.EcoObjects.IccFlatPayments.DTOs
{
    public class IfUpdateUnitInfoDto
    {
        public double ClaimNumber { get; set; }
        public string ClaimModifier { get; set; }
        public double Item { get; set; }
        public double InvoiceId { get; set; }
        public string Insured { get; set; }
        public EcoAddress Address { get; set; }
        // this will cause storage issues - make new table
        // or we store it like an array
        public DateTime DateOfLoss { get; set; }
        public double Amount { get; set; }
    }
}