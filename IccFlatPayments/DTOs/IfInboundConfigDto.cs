using Economical.EcoObjects.General.DTOs;

namespace Economical.EcoObjects.IccFlatPayments.DTOs
{
    public class IfInboundConfigDto : InboundConfigDto
    {
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string ReportSender { get; set; }
        public string[] ReviewerEmails { get; set; }
        public string NoteText { get; set; }
        public string ResourceFolder { get; set; }
        public string ReportName { get;set; }
        public int MaxProcessCount { get; set; }
        public string ReportEmailSubject { get; set; }
        public string CdsQueueName { get; set; }
        public string FamilyQueueName { get; set; }
        public string ReferenceText { get; set; }
    }
}