using System.Threading.Tasks;
using AutoMapper;
using Economical.EcoObjects.General.DTOs;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;
using Economical.EcoObjects.IccFlatPayments.DTOs;
using Economical.EcoObjects.General.Helpers.RepoCommunicator;

namespace Economical.EcoObjects.IccFlatPayments
{
    public class IfRepoCommunicator : RepoCommunicator
    {
        public IfRepoCommunicator(RepoConfig config, AuthCommunicator authComm, Profile profile)
            : base (config, authComm, profile) { }
        public override async Task<Config> UpdateConfigAsync(Config config)
        {
            var ifConfig = config as IfConfig;

            var loginResponse = await base.LoginAsync();

            var configDto = _mapper.Map<IfInboundConfigDto>(ifConfig);

            var content = GenerateObjectContent(configDto);

            var response = await PostAsync(_config.ConfigPath(ifConfig.Handler.Id), content);

            var outboundDto = IngestResponse<IfOutboundConfigDto>(response);

            _mapper.Map(outboundDto, ifConfig);

            return ifConfig;
        }

        public override Config UpdateConfig (Config config)
        {
            var ifConfig = config as IfConfig;

            var loginResponse = Login();

            var configDto = _mapper.Map<IfInboundConfigDto>(ifConfig);

            var content = GenerateObjectContent(configDto);

            var response = Post(_config.ConfigPath(ifConfig.Handler.Id), content);

            var outboundDto = IngestResponse<IfOutboundConfigDto>(response);

            _mapper.Map(outboundDto, ifConfig);

            return ifConfig;
        }

        public override async Task<UnitInfo> UpdateUnitInfoAsync(UnitInfo info)
        {
            var ifInfo = info as IfUnitInfo;

            var loginResponse = await LoginAsync();

            var infoDto = _mapper.Map<IfInboundUnitInfoDto>(ifInfo);

            var content = GenerateObjectContent(infoDto);

            var response = await PutAsync(_config.UnitInfoPath(ifInfo.Unit.Id), content);

            var transact = IngestResponse<OutboundUnitDto>(response);

            _mapper.Map(transact.UnitInfo as IfUnitInfo, ifInfo);

            return transact.UnitInfo;
        }

        public override UnitInfo UpdateUnitInfo(UnitInfo info)
        {
            var ifInfo = info as IfUnitInfo;

            var loginResponse = Login();

            var infoDto = _mapper.Map<IfInboundUnitInfoDto>(ifInfo);

            var content = GenerateObjectContent(infoDto);

            var response = Put(_config.UnitInfoPath(ifInfo.Unit.Id), content);

            var transact = IngestResponse<OutboundUnitDto>(response);

            _mapper.Map(transact.UnitInfo as IfUnitInfo, ifInfo);

            return transact.UnitInfo;
        }

        public override async Task<Config> WriteConfigAsync(Config config)
        {
            var ifConfig = config as IfConfig;

            var loginResponse = await LoginAsync();

            var configDto = _mapper.Map<IfInboundConfigDto>(ifConfig);

            var content = GenerateObjectContent(configDto);

            var response = await PostAsync(_config.ConfigPath(ifConfig.Handler.Id), content);

            var outboundDto = IngestResponse<IfOutboundConfigDto>(response);

            _mapper.Map(outboundDto, ifConfig);

            return ifConfig;
        }

        public override Config WriteConfig(Config config)
        {
            var ifConfig = config as IfConfig;

            var loginResponse = Login();

            var configDto = _mapper.Map<IfInboundConfigDto>(ifConfig);

            var content = GenerateObjectContent(configDto);

            var response = Post(_config.ConfigPath(ifConfig.Handler.Id), content);

            var outboundDto = IngestResponse<IfOutboundConfigDto>(response);

            _mapper.Map(outboundDto, ifConfig);

            return ifConfig;
        }

        public override async Task<UnitInfo> WriteUnitInfoAsync(UnitInfo info)
        {
            var ifInfo = info as IfUnitInfo;

            var loginResponse = await LoginAsync();

            var infoDto = _mapper.Map<IfInboundUnitInfoDto>(ifInfo);

            var content = GenerateObjectContent(infoDto);

            var response = await PostAsync(_config.UnitInfoPath(ifInfo.Unit.Id), content);

            var transact = IngestResponse<OutboundUnitDto>(response);

            _mapper.Map(transact.UnitInfo as IfUnitInfo, ifInfo);

            return transact.UnitInfo;
        }

        public override UnitInfo WriteUnitInfo(UnitInfo info)
        {
            var ifInfo = info as IfUnitInfo;

            var loginResponse = Login();

            var infoDto = _mapper.Map<IfInboundUnitInfoDto>(ifInfo);

            var content = GenerateObjectContent(infoDto);

            var response = Post(_config.UnitInfoPath(ifInfo.Unit.Id), content);

            var transact = IngestResponse<OutboundUnitDto>(response);

            _mapper.Map(transact.UnitInfo as IfUnitInfo, ifInfo);

            return transact.UnitInfo;
        }
    }
}