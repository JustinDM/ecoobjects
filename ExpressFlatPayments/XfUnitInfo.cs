using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Address;
using Economical.EcoObjects.General.Address.Enums;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.ExpressFlatPayments
{
    public class XfUnitInfo : UnitInfo
    {
        public string ClaimNumber { get; set; }
        public string ClaimModifier { get; set; }
        public string Invoice { get; set; }
        public string Insured { get; set; }
        public DateTime DateOfLoss { get; set; }
        public EcoAddress Address { get; set; }
        public double Amount { get; set; }
        public string DateOfTaskCompleted { get; set; }

        public XfUnitInfo() { }
        
        public XfUnitInfo(DataRow row)
        {
            // there are different modifiers based on the 'ClaimTypeCode' column
            // CARG should be -Bien-PD-1
            // PROP should be -Prop-PD-1   
            // AUTO should be -Auto-PD-1
            // CASL should be -Auto-BI-1
            // A/B should be -Auto-AB-1
            
            // we need to have claim number be a string and check it's validity in the process

            ClaimNumber = row["CDS #"].ToString();

            Insured = row["Insured"].ToString();

            var dateCell = row["DOL"].ToString();

            Invoice = row["Invoice"].ToString();

            if (dateCell.ToLower().Contains("n/a"))
            {
                DateOfLoss = new DateTime();
            }
            else
            {
                try
                {
                    DateOfLoss = DateTime.ParseExact(row["DOL"].ToString(), "M/d/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                }
                catch (System.Exception)
                {
                    DateOfLoss = DateTime.ParseExact(row["DOL"].ToString(), "M/d/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                }
            }

            IngestAddress(row);

            var feeRow = row["Fee"].ToString();
            feeRow = feeRow.Replace("$", "").Replace("-", "").Trim();

            if (!string.IsNullOrWhiteSpace(feeRow))
            {
                Amount = Convert.ToDouble(feeRow.Replace("-", ""));
            }
            else
            {
                Amount = 0;
            }


            if (Address.Province == Province.QC)
            {
                ClaimModifier = "-Bien-PD-1";
            }
            else
            {
                ClaimModifier = "-Prop-PD-1";
            }
            
            DateOfTaskCompleted = row["Date of Task Completed"].ToString();
        }

        private void IngestAddress(DataRow row)
        {
            Address = new EcoAddress();

            Address.AddressLines.Add(row["Loss Address"].ToString());
            Address.City = row["City"].ToString().Trim();
            Address.Province = row["Province"].ToString().ParseEnumFromDescription<Province>();
            Address.PostalCode = new PostalCode(row["Postal Code"].ToString());
        }

        public override Dictionary<string, string> GenerateReportRow()
        {
            var addressLine = Address.AddressLines.Count == 0 ? "" : Address.AddressLines[0];
            var reportInfo = new Dictionary<string, string>()
            {
                { "ClaimNumber", ClaimNumber },
                { "ClaimModifier", ClaimModifier },
                { "Invoice", Invoice },
                { "Insured", Insured },
                { "DateOfLoss", DateOfLoss.ToString(Configurations.ReportDateFormat) },
                { "Address", $"{addressLine}, {Address.City}, {Address.Province}, {Address.PostalCode}" },
                { "Amount", Amount.ToString("0.00")},
                { "DateOfTaskCompleted", DateOfTaskCompleted }
            };

            return reportInfo;
        }

        public bool IsCdsClaim()
        {
            if (ClaimNumber.IsMatch(@"[1][0-9]{6}"))
            {
                return true;
            }

            return false;
        }

        public override void CleanData()
        {
            // nothing to do here
        }

        public override void IngestReportRow(DataRow row)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, string> GenerateFrenchReportRow()
        {
            throw new NotImplementedException();
        }
    }
}