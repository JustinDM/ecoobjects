using System;
using System.Text.Json;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.ExpressFlatPayments
{
    public class XfConfig : Config
    {
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string ReportSender { get; set; }
        public string[] ReviewerEmails { get; set; }
        public string NoteText { get; set; }
        public string ResourceFolder { get; set; }
        public string ReportName { get;set; }
        public int MaxProcessCount { get; set; }
        public string ReportEmailSubject { get; set; }
        public Timeout Timeout { get; set; }
        public Guid? TimeoutId { get; set; }
        public string CdsQueueName { get; set; }
        public string FamilyQueueName { get; set; }
        public string ReferenceText { get; set; }

        public XfConfig() { }

        public XfConfig(string fileText) 
        {
            var config = IngestConfigFile(fileText);

            Duplicate(config);
        }

        public override void Duplicate(Config config)
        {
            var xfConfig = config as XfConfig;

            DelayBefore = xfConfig.DelayBefore;
            DelayAfter = xfConfig.DelayAfter;
            ReportSender = xfConfig.ReportSender;
            ReviewerEmails = xfConfig.ReviewerEmails;
            NoteText = xfConfig.NoteText;
            Timeout = xfConfig.Timeout;
            ResourceFolder = xfConfig.ResourceFolder;
            ReportName = string.Format(xfConfig.ReportName, DateTime.Now.ToString(Configurations.ReportDateTimeFormat));
            ReportEmailSubject = xfConfig.ReportEmailSubject;
            MaxProcessCount = xfConfig.MaxProcessCount;
            CdsQueueName = xfConfig.CdsQueueName;
            FamilyQueueName = xfConfig.FamilyQueueName;
            ReferenceText = xfConfig.ReferenceText;
        }

        public override Config IngestConfigFile(string fileText)
        {
            var config = JsonSerializer.Deserialize<XfConfig>(fileText);

            return config;
        }
    }
}