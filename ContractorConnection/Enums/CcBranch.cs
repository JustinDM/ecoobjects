using System.ComponentModel;

namespace Economical.EcoObjects.ContractorConnection.Enums
{
    public enum CcBranch
    {
        Invalid,
        [Description("Atlantic Region")]
        AtlanticRegion,
        [Description("Ontario Region")]
        OntarioRegion,
        [Description("Quebec Region")]
        QuebecRegion,
        [Description("Western Region")]
        WesternRegion,
        [Description("sonnet Insurance")]
        SonnetInsurance
    }
}
