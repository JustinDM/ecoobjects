﻿using System;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Models;
using Newtonsoft.Json;

namespace Economical.EcoObjects.ContractorConnection
{
    public class CcConfig : Config
    {
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string ReviewerEmail { get; set; }
        public string SenderEmail { get; set; }
        public string LogFolder { get; set; }
        public int NumberOfEmails { get; set; }
        public string SourceEmailAddress { get; set; }
        public double AddressMatchThreshold { get; set; }
        public string NoteText { get; set; }
        public string[] RequiredReportColumns { get; set; }
        public string ReportFolder { get; set; }
        public Guid? TimeoutId { get; set; }
        public Timeout Timeout { get; set; }
        public CcConfig() { } 

        public CcConfig(string fileText)
        {
            var config = IngestConfigFile(fileText) as CcConfig;
            Duplicate(config);
            ValidateEmail();
        }

        public override Config IngestConfigFile(string fileText)
        {
            var config = JsonConvert.DeserializeObject<CcConfig>(fileText);
            return config;
        }

        private void ValidateEmail()
        {
            if (!ReviewerEmail.IsEmail() && ReviewerEmail != "returnToSender")
                throw new ArgumentException($"'{ReviewerEmail}' is not a valid email");

            if (!SourceEmailAddress.IsEmail())
                throw new ArgumentException($"'{SourceEmailAddress}' is not a valid email");
        }

        public override void Duplicate(Config config)
        {
            var ccConfig = config as CcConfig;

            DelayAfter = ccConfig.DelayAfter;
            DelayBefore = ccConfig.DelayBefore;
            ReviewerEmail = ccConfig.ReviewerEmail;
            LogFolder = ccConfig.LogFolder;
            NumberOfEmails = ccConfig.NumberOfEmails;
            SourceEmailAddress = ccConfig.SourceEmailAddress;
            AddressMatchThreshold = ccConfig.AddressMatchThreshold;
            Timeout = ccConfig.Timeout;
            NoteText = ccConfig.NoteText;
            RequiredReportColumns = ccConfig.RequiredReportColumns;
            SenderEmail = ccConfig.SenderEmail;
            ReportFolder = ccConfig.ReportFolder;
        }
    }
}