using System;
using System.Collections.Generic;
using Economical.EcoObjects.ContractorConnection.Enums;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Models;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoExtensions;
using System.Globalization;
using System.Data;
using Economical.EcoObjects.General.Address;
using ClosedXML.Excel;

namespace Economical.EcoObjects.ContractorConnection
{
    public class CcUnitInfo : UnitInfo
    {
        public DateTime TransactionDateTime { get; set; }
        public int SequenceNumber { get; set; }
        public string InsuredName { get; set; }
        public string PolicyNumber { get; set; }
        public double ClaimNumber { get; set; }
        public DateTime LossDate { get; set; }
        public int InvoiceNumber { get; set; }
        public int CallId { get; set; }
        public double InvoiceAmount { get; set; }
        public DateTime InvoiceDate { get; set; }
        public CcFeeType FeeType { get; set; }
        public CcExpenseType ExpenseType { get; set; }
        public CcPayeeType PayeeType { get; set; }
        public string DeskAdjuster { get; set; }
        public CcBranch Branch { get; set; }
        public EconomicalSystem SourceSystem { get; set; }
        public int AddressCallId { get; set; }
        public string AddressAdjuster { get; set; }
        public DateTime CallDate { get; set; }
        public DateTime AddressLossDate { get; set; }
        public double Fee { get; set; }
        public double Tax { get; set; }
        public double Total { get; set; }
        public CcAddress Address { get; set; }

        public override void CleanData()
        {
            Address.FirstName = "REDACTED";
            Address.LastName = "REDACTED";
            InsuredName = "REDACTED";
        }

        public override Dictionary<string, string> GenerateReportRow()
        {
            var reportInfo = new Dictionary<string, string>()
            {
                { "Transaction Date", TransactionDateTime.ToString(General.Helpers.Configurations.ReportDateFormat) },
                { "Transaction TOD", TransactionDateTime.ToString(General.Helpers.Configurations.ReportDateFormat) },
                { "Sequence Number", SequenceNumber.ToString() },
                { "Insured Name", InsuredName },
                { "Policy Number", PolicyNumber },
                { "Claim Number", ClaimNumber.ToString() },
                { "Loss Date", LossDate.ToString(General.Helpers.Configurations.ReportDateFormat) },
                { "Invoice Number", $"IN0{InvoiceNumber}" },
                { "Call ID", CallId.ToString() },
                { "Invoice Amount", InvoiceAmount.ToString("C").Replace("$", "") },
                { "Invoice Date", InvoiceDate.ToString(General.Helpers.Configurations.ReportDateFormat) },
                { "Fee Type", FeeType.ToString() },
                { "Expense Type", ExpenseType.ToString() },
                { "Payee Type", PayeeType.ToString() },
                { "Desk Adjuster", DeskAdjuster },
                { "Branch", Branch.ToString() },
            };

            return reportInfo;
        }

        // generic constructor for ef
        public CcUnitInfo() { }

        public CcUnitInfo(IXLRange range)
        {
            var cellList = new List<IXLCell>();
            
            // gotta be a better way than this
            for (int i = 1; i <= 18; i++)
            {
                cellList.Add(range.Cell(1, i));
            }

            TransactionDateTime = CreateDateTime(cellList[0].Value.ToString().Trim(), cellList[1].Value.ToString().Trim());
            SequenceNumber = Convert.ToInt32(cellList[2].Value.ToString().Trim());
            InsuredName = cellList[3].Value.ToString().Trim();
            PolicyNumber = cellList[4].Value.ToString().Trim();
            ClaimNumber = PullClaimNumber(cellList[5].Value.ToString().Trim().Replace("-", ""));
            LossDate = DateTime.ParseExact(cellList[6].Value.ToString().Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
            InvoiceNumber = Convert.ToInt32(cellList[7].Value.ToString().Trim().Substring(2));
            CallId = Convert.ToInt32(cellList[8].Value.ToString().Trim());
            InvoiceAmount = Convert.ToDouble(cellList[9].Value.ToString().Trim());
            InvoiceDate = DateTime.ParseExact(cellList[10].Value.ToString().Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
            FeeType = cellList[11].Value.ToString().Trim().ParseEnumFromDescription<CcFeeType>();
            ExpenseType = cellList[12].Value.ToString().Trim().ParseEnumFromDescription<CcExpenseType>();
            PayeeType = cellList[13].Value.ToString().Trim().ParseEnumFromDescription<CcPayeeType>();
            DeskAdjuster = cellList[14].Value.ToString().Trim();
            Branch = ParseBranch(cellList[15].Value.ToString().Trim());
            SourceSystem = DetermineSourceSystem();
        }

        public CcUnitInfo (DataRow row)
        {
            TransactionDateTime = CreateDateTime(row[0].ToString().Trim(), row[1].ToString().Trim());
            SequenceNumber = Convert.ToInt32(row[2].ToString().Trim());
            InsuredName = row[3].ToString().Trim();
            PolicyNumber = row[4].ToString().Trim();
            ClaimNumber = PullClaimNumber(row[5].ToString().Trim().Replace("-", ""));
            LossDate = DateTime.ParseExact(row[6].ToString().Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
            InvoiceNumber = Convert.ToInt32(row[7].ToString().Trim().Substring(2));
            CallId = Convert.ToInt32(row[8].ToString().Trim());
            InvoiceAmount = Convert.ToDouble(row[9].ToString().Trim());
            InvoiceDate = DateTime.ParseExact(row[10].ToString().Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
            FeeType = row[11].ToString().Trim().ParseEnumFromDescription<CcFeeType>();
            ExpenseType = row[12].ToString().Trim().ParseEnumFromDescription<CcExpenseType>();
            PayeeType = row[13].ToString().Trim().ParseEnumFromDescription<CcPayeeType>();
            DeskAdjuster = row[14].ToString().Trim();
            Branch = ParseBranch(row[15].ToString().Trim());
            SourceSystem = DetermineSourceSystem();
        }

        private DateTime CreateDateTime(string dateLine, string timeLine)
        {
            var year = Convert.ToInt32(dateLine.Substring(0, 4));
            var month = Convert.ToInt32(dateLine.Substring(4, 2));
            var day = Convert.ToInt32(dateLine.Substring(6, 2));

            var hour = Convert.ToInt32(timeLine.Substring(0, 2));
            var minute = Convert.ToInt32(timeLine.Substring(2, 2));
            var seconds = Convert.ToInt32(timeLine.Substring(4, 2));

            return new DateTime(year, month, day, hour, minute, seconds);
        }

        private double PullClaimNumber(string claimNumberText)
        {
            double claimNumber;

            try
            {
                claimNumber = Convert.ToDouble(claimNumberText.Trim().Replace("-", ""));
            }
            catch (Exception)
            {
                // fix to claim number that doesn't exist
                // just a random number that doesn't return anything in CDS
                claimNumber = 5675675;
            }

            return claimNumber;
        }

        private CcBranch ParseBranch(string value)
        {
            CcBranch enumValue;

            try
            {
                enumValue = value.ParseEnumFromDescription<CcBranch>();
            }

            catch (Exception)
            {
                enumValue = CcBranch.Invalid;
            }

            return enumValue;
        }

        private EconomicalSystem DetermineSourceSystem()
        {
            if (ClaimNumber.ToString().IsMatch(@"\b\d{6}\b"))
                return EconomicalSystem.Family;

            return EconomicalSystem.CDS;
        }

        public override void IngestReportRow(DataRow row)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, string> GenerateFrenchReportRow()
        {
            throw new NotImplementedException();
        }
    }
}