// using System;
// using System.Collections.Generic;
// using System.Data;
// using System.Text.Json;
// using Economical.EcoObjects.ContractorConnection.Models;
// using Economical.EcoObjects.General.Address;
// using Economical.EcoObjects.General.Enums;
// using Economical.EcoObjects.General.Models.Handler;
// using Economical.EcoObjects.General.Models.Trace;
// using Economical.EcoObjects.General.Models.Unit;

// namespace Economical.EcoObjects.ContractorConnection.Helpers
// {
//     public static class CcBuilder
//     {
//         public static Handler GenerateNewHandler(RpaProcess process)
//         {
//             var handler = new Handler()
//             {
//                 StartDate = DateTime.Now,
//                 Status = CompletionStatus.Incomplete,
//                 Process = process,
//                 Units = new List<Unit>(),
//                 Traces = new List<Trace>(),
//                 Config = new CcConfig()
//             };

//             return handler;
//         }

//         public static Trace GenerateNewHandlerTrace(int handlerId)
//         {
//             var trace = new Trace()
//             {
//                 Date = DateTime.Now,
//                 HandlerId = handlerId
//             };

//             return trace;
//         }

//         public static CcTrace GenerateNewUnitTrace(int UnitId)
//         {
//             var trace = new CcTrace()
//             {
//                 Date = DateTime.Now,
//                 UnitId = UnitId
//             };

//             return trace;
//         }

//         public static List<Tuple<CcUnit, EcoAddress>> GenerateUnitCollection(DataTable reportTable, string pdfText)
//         {
//             var Units = new List<CcUnit>();

//             foreach (DataRow i in reportTable.Rows)
//             {
//                 var transact = CcUnitBuilder.GenerateNewUnit(i);
//                 Units.Add(transact);
//             }

//             return CcAddressBuilder.GenerateAddressCollection(pdfText, Units);
//         }

        
//     }
// }