using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Address;
using Economical.EcoObjects.General.Address.Enums;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.ContractorConnection.Helpers
{
    public static class CcAddressBuilder
    {
        public static List<CcAddress> IngestAddressFile(string fileText)
        {
            var addresses = new List<CcAddress>();

            var pdfFileLines = fileText.Split(Environment.NewLine.ToCharArray()[0]);

            var cleanLines = CleanFileLines(pdfFileLines);

            var addressLines = new List<string>();

            for (int i = 0; i < cleanLines.Count; i++)
            {

                // basically we go through each line adding them until we get to what we determine to the start of the new line
                // then we build an address out of what we have, clear the collection and restart
                // issue is new we need to traverse the Units as well so we can fill out the address section as we go

                if (cleanLines[i].IsMatch(@"(.([0-9]{7}).*([0-9]{1,2}(\/)){2}[0-9]{4})"))
                {
                    if (addressLines.Count > 0)
                    {
                        try
                        {
                            var address = GenerateAddressObject(addressLines.ToArray());
                            addresses.Add(address);
                        }
                        
                        catch (SystemException) {}
                        addressLines.Clear();
                    }

                    addressLines.Add(cleanLines[i]);
                }

                else
                {
                    addressLines.Add(cleanLines[i]);
                }

            }

            return addresses;
        }

        public static CcAddress GenerateAddressObject(string[] inputLines)
        {
            if (inputLines.Length != 2 && inputLines.Length != 3)
            {
                throw new ArgumentException("constructor should take two lines only");
            }

            var address = new CcAddress();
            var unitInfo = new CcUnitInfo();


            if (inputLines.Length == 2)
            {
                try
                {
                    var mainLine = CleanLine(inputLines[0].Split(' '));
                    var secondLine = CleanLine(inputLines[1].Split(' '));

                    var result = ProcessMainLine(mainLine);
                    var firstNameMissing = result.Item1;
                    address = result.Item2;

                    address = ProcessSecondLine(secondLine, firstNameMissing, address);
                }
                
                catch (Exception)
                {
                    throw new ArgumentException($"failed to parse address lines for '{inputLines[0].Trim().Split(' ')[0]}'");
                }
            }

            else
            {
                var mainLine = CleanLine(inputLines[0].Split(' '));
                var secondLine = CleanLine(inputLines[2].Split(' '));

                var mainLineResult = ProcessMainLine(mainLine);

                var firstNameMissing = mainLineResult.Item1;

                address = mainLineResult.Item2;

                address = ProcessSecondLine(secondLine, firstNameMissing, address);

                // need to get the address here? Or return it?

                address.AddressLines.Add(inputLines[1]);
            }

            return address;
        }

        public static Tuple<bool, CcAddress> ProcessMainLine(List<string> mainLine)
        {
            var mainLineIndex = mainLine.Count - 1;

            var total = Convert.ToDouble(mainLine[mainLineIndex--].Replace("$", ""));

            var tax = Convert.ToDouble(mainLine[mainLineIndex--].Replace("$", ""));

            var fee = Convert.ToDouble(mainLine[mainLineIndex--].Replace("$", ""));

            var addressLossDate = DateTime.ParseExact(mainLine[mainLineIndex--], "MM/dd/yyyy", CultureInfo.InvariantCulture);

            var callDate = DateTime.ParseExact(mainLine[mainLineIndex--], "MM/dd/yyyy", CultureInfo.InvariantCulture);

            var addressAdjuster = $"{mainLine[mainLineIndex - 1]} {mainLine[mainLineIndex]}";
            mainLineIndex -= 2;

            // need to loop through address items - don't know

            var address = new CcAddress();

            var addressLinesResult = ParseAddressLines(mainLineIndex, mainLine, address);

            mainLineIndex = addressLinesResult.Item1;

            var addressClaimNumber = Convert.ToInt32(mainLine[mainLineIndex--].Replace("-", ""));

            var nameParseResult = ParseNames(mainLineIndex, mainLine, address);

            var firstNameMissing = nameParseResult.Item2;
            mainLineIndex = nameParseResult.Item1;

            // we're reassigning here - but aren't we going to lose the changes?
            // we are returning it so I guess we need to make sure we're constructing a new collection with the updated items?

            var addressCallId = Convert.ToInt32(mainLine[mainLineIndex--]);

            return new Tuple<bool, CcAddress>(firstNameMissing, address);
        }

        private static Tuple<int, bool, CcAddress> ParseNames(int mainLineIndex, List<string> mainLine, CcAddress address)
        {
            var policyIsNumeric = (mainLine[mainLineIndex].IsMatch(@"[0-9]") && !mainLine[mainLineIndex].Contains(","));
            var firstNameMissing = false;

            if (policyIsNumeric)
            {
                address.Policy = mainLine[mainLineIndex];
                mainLineIndex--;
            }
            else
                address.Policy = String.Empty;

            if (mainLine[mainLineIndex].Contains(","))
            {
                firstNameMissing = true;
                address.FirstName = string.Empty;
                var parseResult = ParseLastNames(mainLineIndex, mainLine, address);
                mainLineIndex = parseResult.Item1;
                address = parseResult.Item2;
            }
            else
            {
                // don't think we care about this, so dropping
                address.FirstName = mainLine[mainLineIndex--];
                var parseResult = ParseLastNames(mainLineIndex, mainLine, address);
                mainLineIndex = parseResult.Item1;
                address = parseResult.Item2;
            }
                
            return Tuple.Create<int, bool, CcAddress>(mainLineIndex, firstNameMissing, address);
        }

        private static Tuple<int, CcAddress> ParseAddressLines(int mainLineIndex, List<string> mainLine, CcAddress address)
        {
            var addressItems = new List<string>();

            // need to loop through address items - don't know

            do
            {
                addressItems.Add(mainLine[mainLineIndex--]);
            } while (!mainLine[mainLineIndex].IsMatch(@"1[0-9]{6}") && !mainLine[mainLineIndex].IsMatch(@"5[0-9]{5}"));
            // regex: 7 digit number starting with 1 for CDS claims, 6 digit number starting with 5 for Family claims
            // need to build this more dynamically - hoping addresses don't go this high and claim numbers don't roll over too soon
            // could remove the leading numbers - are they really necessary?

            addressItems.Reverse();

            var streetAddress = "";

            foreach (var i in addressItems)
                streetAddress += i + " ";

            address.AddressLines.Add(streetAddress.Trim());

            return new Tuple<int, CcAddress>(mainLineIndex, address);
        }

        private static IList<string> CleanFileLines(IEnumerable<string> inputLines)
        {
            var cleanLines = new List<string>();
            var captureNextLine = false;

            // regex breakdown:
            // Section 1: .([0-9]{7}).*
            // Look for a 7 digit number (I believe this is the format of the claim numbers, may need to change this to accomodate growing claim numbers)
            // followed by one or more characters and preceded by a whitespace (the whitespace character wasn't working for me so it's any character)

            // Section 2: ([0-9]{1,2}(\/)){2}[0-9]{4})
            // Look for a date in the following format: d/M/yyyy or dd/MM/yyyy

            // regex doesn't get the whole line but limits things down to the long ones we want to look at then we know the we want the next one as well (by flipping captureNextLine)

            foreach (var i in inputLines)
            {
                if (i.Length > 0)
                { 
                    if (i.IsMatch(@"(.([0-9]{7}).*([0-9]{1,2}(\/)){2}[0-9]{4})"))
                    {
                        cleanLines.Add(i);
                        captureNextLine = !captureNextLine;
                    } 
                    else if (captureNextLine)
                    {
                        cleanLines.Add(i);
                        if (i.IsMatch(@"[a-zA-Z]\d[a-zA-Z].?\d[a-zA-Z]\d"))
                        {
                            captureNextLine = !captureNextLine;
                        }
                    }
                    
                    
                }
            }

            return cleanLines;
        }

        // Why am I doing this and not using LINQ? It would be a one-liner in LINQ
        private static List<string> CleanLine(IEnumerable<string> inputLine)
        {
            var cleanItems = new List<string>();

            foreach (var i in inputLine)
            {
                if (!String.IsNullOrWhiteSpace(i))
                {
                    cleanItems.Add(i);
                }
            }

            return cleanItems;
        }

        private static Tuple<int, CcAddress> ParseLastNames(int mainLineIndex, List<string> mainLine, CcAddress address)
        {
            var lastNames = new List<string>();
            do
            {
                lastNames.Add(mainLine[mainLineIndex--].Replace(",", ""));
            } while (!int.TryParse(mainLine[mainLineIndex], out _));

            lastNames.Reverse();

            var lastName = string.Empty;

            foreach (var i in lastNames)
            {
                lastName += " " + i.Trim();
            }

            lastName = lastName.Trim();

            address.LastName = lastName;

            return Tuple.Create<int, CcAddress>(mainLineIndex, address);
        }

        private static CcAddress ProcessSecondLine(List<string> secondLine, bool firstNameMissing, CcAddress address)
        {
            var firstName = string.Empty;

            var secondLineIndex = -1;
            
            if (firstNameMissing)
            {
                if (!secondLine[++secondLineIndex].Contains(","))
                {
                    firstName = secondLine[secondLineIndex].Trim();
                }
                else
                {
                    firstName = String.Empty;
                }
            }

            do
            {
                if (!(firstName == String.Empty))
                {
                    secondLineIndex++;
                }

                address.City += $" {secondLine[secondLineIndex].Replace(",", "")}";

            } while (!secondLine[secondLineIndex].Contains(","));

            address.City = address.City.Trim();

            secondLineIndex++;
            address.Province = (Province)Enum.Parse(typeof(Province), secondLine[secondLineIndex++]);
            address.PostalCode = new PostalCode($"{secondLine[secondLineIndex]} {secondLine[++secondLineIndex]}");

            return address;
        }

    }

        // public static List<Tuple<CcUnitInfo, EcoAddress>> GenerateAddressCollection(string fileText, ICollection<CcUnitInfo> transacts)
        // {
        //     var addresses = new List<Tuple<CcUnitInfo, EcoAddress>>();
        //     // fuck me this is stupid that I have to do this - at least I know that's why it's fucked in UiPath as well
        //     var pdfFileLines = fileText.Split(Environment.NewLine.ToCharArray()[0]);

        //     var cleanLines = CleanFileLines(pdfFileLines);

        //     //for (int i = 0; i < cleanLines.Count; i += 2)
        //     //{
        //     //    Addresses.Add(new CcAddress(new string[] { cleanLines[i], cleanLines[i + 1] }));
        //     //}
        //     var addressLines = new List<string>();

        //     for (int i = 0; i < cleanLines.Count; i++)
        //     {

        //         // basically we go through each line adding them until we get to what we determine to the start of the new line
        //         // then we build an address out of what we have, clear the collection and restart
        //         // issue is new we need to traverse the Units as well so we can fill out the address section as we go

        //         if (cleanLines[i].IsMatch(@"(.([0-9]{7}).*([0-9]{1,2}(\/)){2}[0-9]{4})"))
        //         {
        //             if (addressLines.Count > 0)
        //             {
        //                 try
        //                 {
        //                     var address = GenerateAddressObject(addressLines.ToArray(), transacts);
        //                     addresses.Add(new Tuple<CcUnitInfo, EcoAddress>(address.Item1, address.Item2));
        //                 }
                        
        //                 catch (SystemException) {}
        //                 addressLines.Clear();
        //             }

        //             addressLines.Add(cleanLines[i]);
        //         }

        //         else
        //         {
        //             addressLines.Add(cleanLines[i]);
        //         }

        //     }

        //     return addresses;
        // }

        // public static Tuple<CcUnitInfo, CcAddress> GenerateAddressObject(string[] inputLines, ICollection<CcUnitInfo> unitInfos)
        // {
        //     if (inputLines.Length != 2 && inputLines.Length != 3)
        //     {
        //         throw new ArgumentException("constructor should take two lines only");
        //     }

        //     var address = new CcAddress();
        //     var unitInfo = new CcUnitInfo();


        //     if (inputLines.Length == 2)
        //     {
        //         try
        //         {
        //             var mainLine = CleanLine(inputLines[0].Split(' '));
        //             var secondLine = CleanLine(inputLines[1].Split(' '));

        //             var result = ProcessMainLine(mainLine, unitInfos);
        //             var firstNameMissing = result.Item1;
        //             unitInfo = result.Item2;
        //             address = result.Item3;

        //             address = ProcessSecondLine(secondLine, firstNameMissing, address);
        //         }
                
        //         catch (Exception)
        //         {
        //             throw new ArgumentException($"failed to parse address lines for '{inputLines[0].Trim().Split(' ')[0]}'");
        //         }
        //     }

        //     else
        //     {
        //         var mainLine = CleanLine(inputLines[0].Split(' '));
        //         var secondLine = CleanLine(inputLines[2].Split(' '));

        //         var mainLineResult = ProcessMainLine(mainLine, unitInfos);

        //         var firstNameMissing = mainLineResult.Item1;

        //         unitInfo = mainLineResult.Item2;

        //         address = mainLineResult.Item3;

        //         address = ProcessSecondLine(secondLine, firstNameMissing, address);

        //         // need to get the address here? Or return it?

        //         address.AddressLines.Add(inputLines[1]);
        //     }

        //     return new Tuple<CcUnitInfo, EcoAddress>(unitInfo, address);
        // }

        

        // here's the tricky part - will need to determine what the claim number and match before that>
        // or do the search in here?
        // also will need to actually return the object

        // private static Tuple<bool, CcUnitInfo, EcoAddress> ProcessMainLine(List<string> mainLine, ICollection<CcUnitInfo> unitInfos)
        // {
        //     var mainLineIndex = mainLine.Count - 1;

        //     var total = Convert.ToDouble(mainLine[mainLineIndex--].Replace("$", ""));

        //     var tax = Convert.ToDouble(mainLine[mainLineIndex--].Replace("$", ""));

        //     var fee = Convert.ToDouble(mainLine[mainLineIndex--].Replace("$", ""));

        //     var addressLossDate = DateTime.ParseExact(mainLine[mainLineIndex--], "MM/dd/yyyy", CultureInfo.InvariantCulture);

        //     var callDate = DateTime.ParseExact(mainLine[mainLineIndex--], "MM/dd/yyyy", CultureInfo.InvariantCulture);

        //     var addressAdjuster = $"{mainLine[mainLineIndex - 1]} {mainLine[mainLineIndex]}";
        //     mainLineIndex -= 2;

        //     // need to loop through address items - don't know

        //     var address = new EcoAddress();

        //     var addressLinesResult = ParseAddressLines(mainLineIndex, mainLine, address);

        //     mainLineIndex = addressLinesResult.Item1;

        //     var addressClaimNumber = Convert.ToInt32(mainLine[mainLineIndex--].Replace("-", ""));

        //     var unitInfo = unitInfos.First(x => x.ClaimNumber == addressClaimNumber);

        //     var nameParseResult = ParseNames(mainLineIndex, mainLine, unitInfo);

        //     var firstNameMissing = nameParseResult.Item2;
        //     mainLineIndex = nameParseResult.Item1;
        //     unitInfo = nameParseResult.Item3;

        //     // we're reassigning here - but aren't we going to lose the changes?
        //     // we are returning it so I guess we need to make sure we're constructing a new collection with the updated items?

        //     var addressCallId = Convert.ToInt32(mainLine[mainLineIndex--]);

        //     unitInfo.Total = total;
        //     unitInfo.Tax = tax;
        //     unitInfo.Fee = fee;
        //     unitInfo.AddressLossDate = addressLossDate;
        //     unitInfo.CallDate = callDate;
        //     unitInfo.AddressAdjuster = addressAdjuster;
        //     unitInfo.AddressClaim = addressClaimNumber;
        //     unitInfo.AddressCallId = addressCallId;

        //     return new Tuple<bool, CcUnitInfo, EcoAddress>(firstNameMissing, unitInfo, address);
        // }

        

        

}