using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using ClosedXML.Excel;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.ContractorConnection.Helpers
{
    public static class CcUnitBuilder
    {
        // public static List<Unit> GenerateUnits(IFormFile unitFile)
        // {
        //     var fileType = Path.GetExtension(unitFile.FileName);

        //     switch (fileType)
        //     {
        //         case ".xlsx":
        //             var Units = new List<Unit>();

        //             using (var workbook = new XLWorkbook(unitFile.OpenReadStream()))
        //             {

        //                 var sheet = workbook.Worksheet(1);

        //                 var rowCount = 1;
        //                 var pastHeaderRow = false;

        //                 do
        //                 {
        //                     var range = sheet.Range($"A{rowCount}:P{rowCount}");
        //                     if (range.FirstCell().Value.ToString().Contains("Unit Date"))
        //                         pastHeaderRow = true;

        //                     else if (pastHeaderRow)
        //                         Units.Add(GenerateUnit(range));

        //                 } while (!string.IsNullOrWhiteSpace(sheet.Cell(++rowCount, 1).Value.ToString()));
        //             }
                    
        //             return Units;
                
        //         default:
        //             throw new ArgumentException($"File type '{fileType.FormatForErrorOutput(10)}' is not a supported file type for ingestion");
        //     }
        // }

        public static Unit GenerateUnit(IXLRange range)
        {
            var Unit = new Unit()
            {
                BotId = Environment.MachineName,
                Process = RpaProcess.ContractorConnection,
                Status = CompletionStatus.Incomplete,
                UnitInfo = new CcUnitInfo(range),

            };

            return Unit;
        }

        public static Unit GenerateUnit(DataRow row)
        {
            var Unit = new Unit()
            {
                BotId = Environment.MachineName,
                Process = RpaProcess.ContractorConnection,
                Status = CompletionStatus.Incomplete,
                UnitInfo = new CcUnitInfo(row),
            };

            return Unit;
        }

        public static List<Unit> AssociateUnitAddresses(List<Unit> units, List<CcAddress> addresses)
        {
            foreach (var i in units)
            {
                var unitInfo = i.UnitInfo as CcUnitInfo;
                var address = addresses.FirstOrDefault(x => x.ClaimNumber == unitInfo.ClaimNumber);
                if (address != null)
                {
                    unitInfo.Address = address;
                }
            }

            return units;
        }
    }
}