using System;

namespace Economical.EcoObjects.ContractorConnection.DTOs
{
    public class CcInboundHandlerDto
    {
        // handler info
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string RpaProcessString { get; set; }
        public string CompletionStatusString { get; set; }

        // config info
        public int DelayAfter { get; set; }
        public int DelayBefore { get; set; }
        public string SenderEmail { get; set; }
        public string ReviewerEmail { get; set; }
        public string LogFolder { get; set; }
        public int NumberOfEmails { get; set; }
        public string SourceEmailAddress { get; set; }
        public double AddressMatchThreshold { get; set; }
        public string NoteText { get; set; }
        public string[] RequiredColumnArray { get; set; }
        public int TimeoutShort { get; set; }
        public int TimeoutMedium { get; set; }
        public int TimeoutLong { get; set; }
    }
}