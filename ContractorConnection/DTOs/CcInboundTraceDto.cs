using System;

namespace Economical.EcoObjects.ContractorConnection.DTOs
{
    public class CcInboundTraceDto
    {
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public string PolarityString { get; set; }
        public string SystemString { get; set; }
        public string TypeString { get; set; }
        public int? UnitId { get; set; }
        public int? HandlerId { get; set; }
    }
}