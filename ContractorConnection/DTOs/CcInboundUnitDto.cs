using System;

namespace Economical.EcoObjects.ContractorConnection.DTOs
{
    public class CcInboundUnitDto
    {
        // general Unit log
        public int HandlerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string BotId { get; set; }
        public string ProcessString { get; set; }
        public string StatusString { get; set; }

        // queue log
        public string QueueString { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime DeferDate { get; set; }
        public string PriorityString { get; set; }

        // CcUnit
        public DateTime UnitDateTime { get; set; }
        public int SequenceNumber { get; set; }
        public string InsuredName { get; set; }
        public string PolicyNumber { get; set; }
        public int ClaimNumber { get; set; }
        public DateTime LossDate { get; set; }
        public int InvoiceNumber { get; set; }
        public int CallId { get; set; }
        public double InvoiceAmount { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string FeeTypeString { get; set; }
        public string ExpenseTypeString { get; set; }
        public string PayeeTypeString { get; set; }
        public string DeskAdjuster { get; set; }
        public string BranchString { get; set; }
        public string SourceSystemString { get; set; }
        public int AddressCallId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AddressPolicy { get; set; }
        public int AddressClaimNumber { get; set; }
        public string AddressAdjuster { get; set; }
        public DateTime CallDate { get; set; }
        public DateTime AddressLossDate { get; set; }
        public double Fee { get; set; }
        public double Tax { get; set; }
        public double Total { get; set; }

    }
}