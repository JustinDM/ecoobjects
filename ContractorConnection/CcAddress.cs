using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Address;

namespace Economical.EcoObjects.ContractorConnection
{
    public class CcAddress : EcoAddress
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ClaimNumber { get; set; }
        public string Policy { get; set; }
    }
}