using System.Threading.Tasks;
using AutoMapper;
using Economical.EcoObjects.General.DTOs;
using Economical.EcoObjects.General.DTOs.NovaScotiaClosing;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;
using Economical.EcoObjects.General.Helpers.RepoCommunicator;
using System;

namespace Economical.EcoObjects.NovaScotiaClosing
{
    public class NcRepoCommunicator : RepoCommunicator
    {
        public NcRepoCommunicator(
            RepoConfig config, AuthCommunicator authComm, Profile profile)
            : base (config, authComm, profile) { }

        public override async Task<Config> WriteConfigAsync(Config config)
        {
            var ncConfig = config as NcConfig;

            var loginResponse = await LoginAsync();

            var configDto = _mapper.Map<NcInboundConfigDto>(ncConfig);

            var content = GenerateObjectContent(configDto);

            var response = await PostAsync(_config.ConfigPath(ncConfig.Handler.Id), content);

            var outboundDto = IngestResponse<NcOutboundConfigDto>(response);

            _mapper.Map(outboundDto, ncConfig);

            return ncConfig;
        }

        public override async Task<Config> UpdateConfigAsync(Config config)
        {
            var ncConfig = config as NcConfig;

            var loginResponse = await LoginAsync();

            var configDto = _mapper.Map<NcInboundConfigDto>(ncConfig);

            var content = GenerateObjectContent(configDto);

            var response = await PutAsync(_config.ConfigPath(ncConfig.Handler.Id), content);

            var outboundDto = IngestResponse<NcOutboundConfigDto>(response);

            _mapper.Map(outboundDto, ncConfig);

            return ncConfig;
        }

        public override async Task<UnitInfo> WriteUnitInfoAsync(UnitInfo info)
        {
            var ncInfo = info as NcUnitInfo;

            var loginResponse = await LoginAsync();

            var infoDto = _mapper.Map<NcInboundUnitInfoDto>(ncInfo);

            var content = GenerateObjectContent(infoDto);

            // need additional endpoints

            var response = await PostAsync(_config.UnitInfoPath(ncInfo.Unit.Id), content);

            var transact = IngestResponse<OutboundUnitDto>(response);

            _mapper.Map(transact.UnitInfo as NcUnitInfo, ncInfo);

            return transact.UnitInfo;
        }

        public override async Task<UnitInfo> UpdateUnitInfoAsync(UnitInfo info)
        {
            var ncInfo = info as NcUnitInfo;

            var loginResponse = await LoginAsync();

            var infoDto = _mapper.Map<NcInboundUnitInfoDto>(ncInfo);

            var content = GenerateObjectContent(infoDto);

            var response = await PutAsync(_config.UnitInfoPath(ncInfo.Unit.Id), content);

            var transact = IngestResponse<OutboundUnitDto>(response);

            // does our mapper cover this?
            _mapper.Map(transact.UnitInfo as NcUnitInfo, ncInfo);

            return transact.UnitInfo;
        }

        public async Task<NcConfig> WriteTimeoutAsync(Timeout timeout)
        {
            var loginResponse = await LoginAsync();

            var timeoutDto = _mapper.Map<InboundTimeoutDto>(timeout);

            var content = GenerateObjectContent(timeoutDto);

            // DOESN'T WORK

            var response = await PostAsync(_config.TimeoutPath(Guid.NewGuid()), content);

            var responseConfig = IngestResponse<NcOutboundConfigDto>(response);

            var config = _mapper.Map<NcConfig>(responseConfig);

            return config;
        }

        public override UnitInfo WriteUnitInfo(UnitInfo info)
        {
            throw new System.NotImplementedException();
        }

        public override UnitInfo UpdateUnitInfo(UnitInfo info)
        {
            throw new System.NotImplementedException();
        }

        public override Config WriteConfig(Config config)
        {
            throw new System.NotImplementedException();
        }

        public override Config UpdateConfig(Config config)
        {
            throw new System.NotImplementedException();
        }
    }
}