using Economical.EcoExtensions;
using Economical.EcoObjects.CDS.Enums;
using Economical.EcoObjects.General.DTOs.NovaScotiaClosing;
using Economical.EcoObjects.General.Helpers;

namespace Economical.EcoObjects.NovaScotiaClosing
{
    public class NcMapperProfiles : MapperProfiles
    {
        public NcMapperProfiles() : base()
        {
            CreateMap<NcConfig, NcInboundConfigDto>();

            CreateMap<NcUnitInfo, NcInboundUnitInfoDto>()
                .ForMember(dest => dest.ClaimStatusString, opt => {
                    opt.MapFrom(src => src.ClaimStatus.ToString());
                });

            CreateMap<NcInboundConfigDto, NcConfig>();

            CreateMap<NcInboundUnitInfoDto, NcUnitInfo>()
                .ForMember(dest => dest.ClaimStatus, opt => opt.MapFrom(
                    src => src.ClaimStatusString.ParseEnumFromDescription<ClaimStatus>()
                ));
            
            CreateMap<NcUpdateUnitInfoDto, NcUnitInfo>()
                .ForMember(dest => dest.ClaimStatus, opt => opt.MapFrom(
                    src => src.StatusString.ParseEnumFromDescription<ClaimStatus>()
                ));

            CreateMap<NcUnitInfo, NcOutboundUnitInfoDto>().ReverseMap();

            CreateMap<NcConfig, NcOutboundConfigDto>().ReverseMap();

        }
    }
}