using System;
using System.Text.Json;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.NovaScotiaClosing
{
    public class NcConfig : Config
    {
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string[] ReviewerEmails { get; set; }
        public string NoteText { get; set; }
        public Timeout Timeout { get; set; }
        public Guid? TimeoutId { get; set; }
        public string ResourceFolder { get; set; }
        public string FallbackEmail { get; set; }

        public NcConfig() { }
        
        public NcConfig(string fileText)
        {
            var config = IngestConfigFile(fileText);

            Duplicate(config);
        }
        public override void Duplicate(Config config)
        {
            var ncConfig = config as NcConfig;

            DelayBefore = ncConfig.DelayBefore;
            DelayAfter = ncConfig.DelayAfter;
            ReviewerEmails = ncConfig.ReviewerEmails;
            NoteText = ncConfig.NoteText;
            Timeout = ncConfig.Timeout;
            ResourceFolder = ncConfig.ResourceFolder;
            FallbackEmail = ncConfig.FallbackEmail;
        }

        public override Config IngestConfigFile(string fileText)
        {
            var config = JsonSerializer.Deserialize<NcConfig>(fileText);

            return config;
        }
    }
}