using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.NovaScotiaClosing
{
    public static class NcUnitGenerator
    {
        public static List<Unit> GenerateUnits (DataTable table, bool fromCds)
        {
            var logs = new List<Unit>();

            foreach (DataRow i in table.Rows)
            {
                if (i[0].ToString().Contains("-"))
                {
                    Unit log;

                    if (fromCds)
                    {
                        log = GenerateLogFromCds(i);
                    }

                    else
                    {
                        log = GenerateLogFromReport(i);
                    }

                    logs.Add(log);
                }
            }

            return logs;
        }

        private static Unit GenerateLogFromCds(DataRow row)
        {

            var claimNumber = row["CLAIMNUMBER"].ToString();

            var claimNumberItems = claimNumber.Split('-');

            var log = new Unit() 
            {
                UnitInfo = new NcUnitInfo()
                {
                    ClaimNumber = Convert.ToDouble(claimNumberItems[0]),
                    ClaimModifier = String.Join("-", claimNumberItems.Skip(1))
                },
                Status = CompletionStatus.Incomplete,
                BotId = Environment.MachineName,
                Process = RpaProcess.NovaScotiaOpening
            };

            return log;
        }

        private static Unit GenerateLogFromReport(DataRow row)
        {
            var log = new Unit()
            {
                UnitInfo = new NcUnitInfo()
                {
                    ClaimNumber = Convert.ToDouble(row["ClaimNumber"].ToString()),
                    ClaimModifier = row["ClaimModifier"].ToString(),
                    IsUberPolicy = Convert.ToBoolean(row["IsUberPolicy"].ToString()),
                    IsCommercialOffline = Convert.ToBoolean(row["IsCommercialOffline"].ToString()),
                    Company = row["Company"].ToString(),
                    LetterDate = DateTime.ParseExact(
                        row["LetterDate"].ToString(), 
                        Configurations.ReportDateFormat, 
                        CultureInfo.InvariantCulture),
                    DateOfLoss = DateTime.ParseExact(
                        row["DateOfLoss"].ToString(),
                        Configurations.ReportDateFormat,
                        CultureInfo.InvariantCulture
                    ),
                    Policy = row["Policy"].ToString(),
                    Adjuster = row["Adjuster"].ToString(),
                    CustomerEmail = row["CustomerEmail"].ToString(),
                    ApprovedForEmailing = Convert.ToBoolean(row["ApprovedForEmailing"])
                }
            };

            return log;
        }
    }
}