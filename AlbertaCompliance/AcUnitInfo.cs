using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Economical.EcoExtensions;
using Economical.EcoObjects.CDS.Enums;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.AlbertaCompliance
{
    public class AcUnitInfo : UnitInfo
    {
        public DateTime StartDate { get; set; }
        public string BotId { get; set; }
        public ClaimStatus ClaimStatus { get; set; }
        public double ClaimNumber { get; set; }
        public bool IsUberPolicy { get; set; }
        public string Company { get; set; }
        public DateTime LetterDate { get; set; }
        public DateTime DateOfLoss { get; set; }
        public string Policy { get; set; }
        public string Adjuster { get; set; }
        public string HandlerId { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CustomerEmail { get; set; }
        public bool ApprovedForEmailing { get; set; }
        public bool IsCommercialOffline { get; set; }
        public string StarRating { get; set; }
        public override void CleanData()
        {
            CustomerEmail = "REDACTED";
        }

        // generic constructor for EF
        public AcUnitInfo() { }

        public override Dictionary<string, string> GenerateReportRow()
        {
            var reportInfo = new Dictionary<string, string>()
            {
                { "ClaimNumber", ClaimNumber.ToString() },
                { "IsUberPolicy", IsUberPolicy.ToString() },
                { "IsCommercialOffline", IsCommercialOffline.ToString() },
                { "Company", Company },
                { "LetterDate", LetterDate.ToString(General.Helpers.Configurations.ReportDateFormat) },
                { "DateOfLoss", DateOfLoss.ToString(General.Helpers.Configurations.ReportDateFormat) },
                { "Policy", Policy },
                { "Adjuster", Adjuster },
                { "CustomerEmail", CustomerEmail },
                { "ApprovedForEmailing", "" },
            };

            return reportInfo;
        }

        public override void IngestReportRow(DataRow row)
        {
            ClaimNumber = Convert.ToInt32(row["ClaimNumber"].ToString());
            IsUberPolicy = row["IsUberPolicy"].ToString() == "False" ? false : true;
            IsCommercialOffline = row["IsCommercialOffline"].ToString() == "False" ? false : true;
            Company = row["Company"].ToString();
            LetterDate = DateTime.ParseExact(row["LetterDate"].ToString(), General.Helpers.Configurations.ReportDateFormat, CultureInfo.InvariantCulture);
            DateOfLoss = DateTime.ParseExact(row["DateOfLoss"].ToString(), General.Helpers.Configurations.ReportDateFormat, CultureInfo.InvariantCulture);
            Policy = row["Policy"].ToString();
            Adjuster = row["Adjuster"].ToString();
            StarRating = row["StarRating"].ToString();
            CustomerEmail = row["CustomerEmail"].ToString();
            ApprovedForEmailing = false;
        }

        public override Dictionary<string, string> GenerateFrenchReportRow()
        {
            throw new NotImplementedException();
        }
    }
}