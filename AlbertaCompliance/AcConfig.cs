using System;
using System.Text.Json;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.EventArgs;
using Economical.EcoObjects.General.Models;
using Newtonsoft.Json;

namespace Economical.EcoObjects.AlbertaCompliance
{
    public class AcConfig : Config
    {
        private int delayBefore;
        public int DelayBefore
        {
            get { return delayBefore; }
            set 
            { 
                delayBefore = value;
                CallUpdateEvent(this, new PropertyUpdateEventArgs("DelayBefore", value.ToString()));
            }
        }

        private int delayAfter;
        public int DelayAfter
        {
            get { return delayAfter; }
            set 
            { 
                delayAfter = value;
                CallUpdateEvent(this, new PropertyUpdateEventArgs("DelayAfter", value.ToString())); 
            }
        }

        private string reviewerEmail;
        public string ReviewerEmail
        {
            get { return reviewerEmail; }
            set 
            { 
                reviewerEmail = value;
                CallUpdateEvent(this, new PropertyUpdateEventArgs("ReviewerEmail", value.ToString()));
            }
        }

        private string logFolder;
        public string LogFolder
        {
            get { return logFolder; }
            set 
            { 
                logFolder = value;
                CallUpdateEvent(this, new PropertyUpdateEventArgs("LogFolder", value.ToString()));
            }
        }

        private string noteText;
        public string NoteText
        {
            get { return noteText; }
            set 
            { 
                noteText = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("NoteText", value.ToString()));
            }
        }

        private Timeout timeout;
        public Timeout Timeout
        {
            get { return timeout; }
            set 
            { 
                timeout = value;
                CallInitializeEvent(this, new PropertyInitializedEventArgs("AcConfig.Timeout", value));
                timeout.PropertyUpdated += CallUpdateEvent;
            }
        }
        private string resourceFolder;
        public string ResourceFolder
        {
            get { return resourceFolder; }
            set 
            { 
                resourceFolder = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("ResourceFolder", value.ToString()));
            }
        }

        public Guid? TimeoutId { get; set; }

        // generic constructor for EF
        public AcConfig() { }

        public AcConfig(string fileText)
        {
            var config = IngestConfigFile(fileText) as AcConfig;

            Duplicate(config);
        }

        public override void Duplicate(Config config)
        {
            var acConfig = config as AcConfig;

            DelayBefore = acConfig.DelayBefore;
            DelayAfter = acConfig.DelayAfter;
            ReviewerEmail = acConfig.ReviewerEmail;
            LogFolder = acConfig.LogFolder;
            NoteText = acConfig.NoteText;
            Timeout = acConfig.Timeout;
            ResourceFolder = acConfig.ResourceFolder;
        }

        public override Config IngestConfigFile(string fileText)
        {
            var config = JsonConvert.DeserializeObject<AcConfig>(fileText);

            return config;
        }

        private void ValidateEmail()
        {
            if (!ReviewerEmail.IsEmail())
            {
                throw new ArgumentException($"Email '{ReviewerEmail.FormatForErrorOutput(20)}' is not recognized as a valid email");
            }
        }

        
    }
}