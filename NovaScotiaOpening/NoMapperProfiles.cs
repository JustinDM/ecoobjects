using AutoMapper;
using Economical.EcoExtensions;
using Economical.EcoObjects.CDS.Enums;
using Economical.EcoObjects.General.DTOs.NovaScotiaOpening;
using Economical.EcoObjects.General.Helpers;

namespace Economical.EcoObjects.NovaScotiaOpening
{
    public class NoMapperProfiles : MapperProfiles
    {
        public NoMapperProfiles() : base()
        {
            CreateMap<NoConfig, NoInboundConfigDto>();

            CreateMap<NoUnitInfo, NoInboundUnitInfoDto>()
                .ForMember(dest => dest.ClaimStatusString, opt => {
                    opt.MapFrom(src => src.ClaimStatus.ToString());
                });

            CreateMap<NoInboundConfigDto, NoConfig>();

            CreateMap<NoInboundUnitInfoDto, NoUnitInfo>()
                .ForMember(dest => dest.ClaimStatus, opt => opt.MapFrom(
                    src => src.ClaimStatusString.ParseEnumFromDescription<ClaimStatus>()
                ));
            
            CreateMap<NoUpdateUnitInfoDto, NoUnitInfo>()
                .ForMember(dest => dest.ClaimStatus, opt => opt.MapFrom(
                    src => src.StatusString.ParseEnumFromDescription<ClaimStatus>()
                ));

            CreateMap<NoUnitInfo, NoOutboundUnitInfoDto>().ReverseMap();

            CreateMap<NoConfig, NoOutboundConfigDto>().ReverseMap();
        }
    }
}