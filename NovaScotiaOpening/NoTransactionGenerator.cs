using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Economical.EcoExtensions;
using Economical.EcoObjects.CDS.Enums;
using Economical.EcoObjects.General.Enums;
using System.Linq;
using Economical.EcoObjects.General.Models;
using Economical.EcoObjects.General.Helpers;

namespace Economical.EcoObjects.NovaScotiaOpening
{
    public static class NoUnitGenerator
    {
        public static List<Unit> GenerateUnits(DataTable table, bool fromCds)
        {
            var logs = new List<Unit>();

            foreach (DataRow i in table.Rows)
            {
                if (i[0].ToString().Contains("-"))
                {
                    Unit log;

                    if (fromCds)
                    {
                        log = GenerateLogFromCds(i);
                    }

                    else
                    {
                        log = GenerateLogFromReport(i);
                    }

                    logs.Add(log);
                }
                
            }

            return logs;
        }

        private static Unit GenerateLogFromCds(DataRow row)
        {
            var claimNumberItems = row["Claim"].ToString().Split('-');

            var claimNumber = claimNumberItems[0];
            var claimNumberModifier = String.Join("-", claimNumberItems.Skip(1));

            var log = new Unit() 
            {
                UnitInfo = new NoUnitInfo()
                {
                    ClaimNumber = Convert.ToDouble(claimNumber),
                    ClaimNumberModifier = claimNumberModifier,
                    ClaimStatus = row["Status"].ToString().ParseEnumFromDescription<ClaimStatus>(),
                    DateOfLoss = DateTime.ParseExact(row["Date Of Loss"].ToString(), "dd-MMM-yyyy", CultureInfo.InvariantCulture),

                },
                Status = CompletionStatus.Incomplete,
                BotId = Environment.MachineName,
                Process = RpaProcess.NovaScotiaOpening
            };

            return log;
        }

        private static Unit GenerateLogFromReport(DataRow row)
        {
            var log = new Unit()
            {
                UnitInfo = new NoUnitInfo()
                {
                    ClaimNumber = Convert.ToDouble(row["ClaimNumber"].ToString()),
                    ClaimNumberModifier = row["ClaimNumberModifier"].ToString(),
                    IsUberPolicy = Convert.ToBoolean(row["IsUberPolicy"].ToString()),
                    IsCommercialOffline = Convert.ToBoolean(row["IsCommercialOffline"].ToString()),
                    Company = row["Company"].ToString(),
                    LetterDate = DateTime.ParseExact(
                        row["LetterDate"].ToString(), 
                        Configurations.ReportDateFormat, 
                        CultureInfo.InvariantCulture),
                    DateOfLoss = DateTime.ParseExact(
                        row["DateOfLoss"].ToString(),
                        Configurations.ReportDateFormat,
                        CultureInfo.InvariantCulture
                    ),
                    Policy = row["Policy"].ToString(),
                    Adjuster = row["Adjuster"].ToString(),
                    CustomerEmail = row["CustomerEmail"].ToString(),
                    ApprovedForEmailing = Convert.ToBoolean(row["ApprovedForEmailing"])
                }
            };

            return log;
        }
    }
}