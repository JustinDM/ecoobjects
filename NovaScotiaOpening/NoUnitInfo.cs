using System;
using System.Collections.Generic;
using System.Data;
using Economical.EcoExtensions;
using Economical.EcoObjects.CDS.Enums;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.NovaScotiaOpening
{
    public class NoUnitInfo : UnitInfo
    {
        public ClaimStatus ClaimStatus { get; set; }
        public double ClaimNumber { get; set; }
        public string ClaimNumberModifier { get; set; }
        public bool IsUberPolicy { get; set; }
        public string Company { get; set; }
        public DateTime LetterDate { get; set; }
        public DateTime DateOfLoss { get; set; }
        public string Policy { get; set; }
        public string Adjuster { get; set; }
        public string CustomerEmail { get; set; }
        public bool ApprovedForEmailing { get; set; }
        public bool IsCommercialOffline { get; set; }

        public override void CleanData()
        {
            // should maybe clean customer email but we actually need to store until we're done
        }

        public override Dictionary<string, string> GenerateFrenchReportRow()
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, string> GenerateReportRow()
        {
            var reportInfo = new Dictionary<string, string>()
            {
                { "ClaimNumber", ClaimNumber.ToString() },
                { "ClaimNumberModifier", ClaimNumberModifier },
                { "IsUberPolicy", IsUberPolicy.ToString() },
                { "IsCommercialOffline", IsCommercialOffline.ToString() },
                { "Company", Company },
                { "LetterDate", LetterDate.ToString(Configurations.ReportDateFormat) },
                { "DateOfLoss", DateOfLoss.ToString(Configurations.ReportDateFormat) },
                { "Policy", Policy },
                { "Adjuster", Adjuster },
                { "CustomerEmail", CustomerEmail },
                { "ApprovedForEmailing", ApprovedForEmailing.ToString() },
            };

            return reportInfo;
        }

        public override void IngestReportRow(DataRow row)
        {
            throw new NotImplementedException();
        }
    }
}