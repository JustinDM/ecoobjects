using System;
using System.Text.Json;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.NovaScotiaOpening
{
    public class NoConfig : Config
    {
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string[] ReviewerEmails { get; set; }
        public string NoteText { get; set; }
        public Timeout Timeout { get; set; }
        public Guid? TimeoutId { get; set; }
        public string ResourceFolder { get; set; }
        public string FallbackEmail { get; set; }
        
        public NoConfig() { }

        public NoConfig(string fileText)
        {
            var config = IngestConfigFile(fileText) as NoConfig;

            Duplicate(config);
        }
        
        public override void Duplicate(Config config)
        {
            var noConfig = config as NoConfig;

            DelayBefore = noConfig.DelayBefore;
            DelayAfter = noConfig.DelayAfter;
            ReviewerEmails = noConfig.ReviewerEmails;
            NoteText = noConfig.NoteText;
            Timeout = noConfig.Timeout;
            ResourceFolder = noConfig.ResourceFolder;
            FallbackEmail = noConfig.FallbackEmail;
        }

        public override Config IngestConfigFile(string fileText)
        {
            var config = JsonSerializer.Deserialize<NoConfig>(fileText);

            return config;
        }
    }
}