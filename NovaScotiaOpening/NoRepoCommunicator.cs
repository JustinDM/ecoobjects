using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Economical.EcoObjects.General.DTOs;
using Economical.EcoObjects.General.DTOs.NovaScotiaOpening;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;
using Economical.EcoObjects.General.Helpers.RepoCommunicator;

namespace Economical.EcoObjects.NovaScotiaOpening
{
    // update endpoint stuff
    // add timeout method
    public class NoRepoCommunicator : RepoCommunicator
    {
        public NoRepoCommunicator(RepoConfig config, AuthCommunicator authComm, Profile profile) 
            : base (config, authComm, profile) { }

        public override async Task<Config> WriteConfigAsync(Config config)
        {
            var noConfig = config as NoConfig;

            var loginResponse = await LoginAsync();

            var configDto = _mapper.Map<NoInboundConfigDto>(noConfig);

            var content = GenerateObjectContent(configDto);

            var response = await PostAsync(_config.ConfigPath(noConfig.Handler.Id), content);

            var outboundDto = IngestResponse<NoOutboundConfigDto>(response);

            _mapper.Map(outboundDto, noConfig);

            return noConfig;
        }

        public override async Task<Config> UpdateConfigAsync(Config config)
        {
            var noConfig = config as NoConfig;

            var loginResponse = await LoginAsync();

            var configDto = _mapper.Map<NoInboundConfigDto>(noConfig);

            var content = GenerateObjectContent(configDto);

            var response = await PutAsync(_config.ConfigPath(noConfig.Handler.Id), content);

            var outboundDto = IngestResponse<NoOutboundConfigDto>(response);

            _mapper.Map(outboundDto, noConfig);

            return noConfig;
        }

        public override async Task<UnitInfo> WriteUnitInfoAsync(UnitInfo info)
        {
            var noInfo = info as UnitInfo;

            var loginResponse = await LoginAsync();

            var infoDto = _mapper.Map<NoInboundUnitInfoDto>(noInfo);

            var content = GenerateObjectContent(infoDto);

            // need additional endpoints

            var response = await PostAsync(_config.UnitInfoPath(noInfo.UnitId), content);

            var transact = IngestResponse<OutboundUnitDto>(response);

            _mapper.Map(transact.UnitInfo as NoUnitInfo, noInfo);

            return transact.UnitInfo;
        }

        public override async Task<UnitInfo> UpdateUnitInfoAsync(UnitInfo info)
        {
            var noInfo = info as NoUnitInfo;

            var loginResponse = await LoginAsync();

            var infoDto = _mapper.Map<NoInboundUnitInfoDto>(noInfo);

            var content = GenerateObjectContent(infoDto);

            var response = await PutAsync(_config.UnitInfoPath(noInfo.UnitId), content);

            var transact = IngestResponse<OutboundUnitDto>(response);

            _mapper.Map(transact.UnitInfo as NoUnitInfo, noInfo);

            return transact.UnitInfo;
        }

        public async Task<NoConfig> WriteTimeout(Timeout timeout)
        {
            var loginResponse = await LoginAsync();

            var timeoutDto = _mapper.Map<InboundTimeoutDto>(timeout);

            var content = GenerateObjectContent(timeoutDto);

            var response = await PostAsync(_config.TimeoutPath(System.Guid.NewGuid()), content);

            var responseConfig = IngestResponse<NoOutboundConfigDto>(response);

            var config = _mapper.Map<NoConfig>(responseConfig);

            return config;
        }

        public override UnitInfo WriteUnitInfo(UnitInfo info)
        {
            throw new System.NotImplementedException();
        }

        public override UnitInfo UpdateUnitInfo(UnitInfo info)
        {
            throw new System.NotImplementedException();
        }

        public override Config WriteConfig(Config config)
        {
            throw new System.NotImplementedException();
        }

        public override Config UpdateConfig(Config config)
        {
            throw new System.NotImplementedException();
        }
    }
}