using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Address;

namespace Economical.EcoObjects.FIS
{
    public static class FisHelpers
    {
        public static string ClaimNumberRegex = @"(?<=an(=|-))([a-zA-Z]|\d){1}\d{7,}";
        public static string PolicyNumberRegex = @"(AC|HC)([0-9]|[a-zA-Z])\s{0,1}([0-9]|[a-zA-Z]){2}(-|)[0-9]{8}";
        public static DateTime ParseDate(string dateText)
        {
            if (string.IsNullOrWhiteSpace(dateText))
            {
                throw new ArgumentException($"'{nameof(dateText)}' cannot be null or whitespace.", nameof(dateText));
            }
            // 18-Jun-2021
            return DateTime.ParseExact(dateText.CleanDateString(), "ddMMMyyyy", CultureInfo.InvariantCulture);
        }

        public static DateTime ParseDateTime(string dateText)
        {
            if (string.IsNullOrWhiteSpace(dateText))
            {
                throw new ArgumentException($"'{nameof(dateText)}' cannot be null or whitespace.", nameof(dateText));
            }
            // 18-Jun-2021 11:05
            // is this 24-hour based or am/pm?
            // I don't see am/pm so assuming 24H
            return DateTime.ParseExact(dateText.CleanDateString(), "ddMMMyyyy HH:mm", CultureInfo.InvariantCulture);
        }

        public static string CleanDateString(this string input)
        {
            input = input.CleanOutliers();

            input = input.Replace("-", "");
            input = input.Replace(" ", "");
            input = input.Replace(",", "");

            // shouldn't replace the letters from the month
            // "1 3-N0SF2020" -> this causes issues
            // after cleaning we get 13NOSF2020
            // take the first two as day, last four as year, rest as month?
            var day = String.Join("", input.Take(2));
            // regular length is 9, length - 4
            var year = String.Join("", input.Skip(input.Length - 4));
            var month = input.Replace(year, "").Replace(day, "");

            day = day.CleanNumberString();
            year = year.CleanNumberString();
            month = month.CleanMonthString();            

            input = day + month + year;

            input = input.Trim();

            return input;
        }

        public static string CleanOutliers(this string input)
        {
            input = input.Replace("omul", "06-Jul");

            return input;
        }

        public static string CleanNumberString(this string input)
        {
            input = input.ToLower();
            input = input.Replace("i", "1").Replace("|", "1");
            input = input.Replace("l", "1");
            input = input.Replace("a", "8").Replace("&", "8");
            input = input.Replace("o", "0");
            input = input.Replace("f", "7");
            input = input.Replace("•", "");
            input = input.Replace("d", "6");
            input = input.Replace("g", "9");
            input = input.Replace("c)", "0");
            input = input.Replace("c", "0");
            input = input.Replace("m", "0");
            input = input.Replace("b", "6");
            input = input.Replace("t", "1");
            input = input.Replace("q", "2");
            input = input.Replace("ß", "0");

            return input;
        }

        public static string CleanMonthString(this string input)
        {
            input = LevParseMonth(input);
            input = input.Replace("01.11", "Jul");
            input = input.Replace("K)", "O");
            input = input.Replace("±", "ct");
            input = input.Replace("0", "O");
            input = input.Replace("SF", "V");
            input = input.Replace("\\r", "v");
            input = input.Replace("sp", "v");
            input = input.Replace("&", "v");

            return input;
        }

        public static string CleanAmountString(this string input)
        {
            // sometimes reads the "," as a "."
            // it seems pretty good at getting the right number of characters
            // we know that the first two characters from the right are the decimal points
            // after that we could have a few things denoting the decimal point
            // then we have a variable amount of characters denoting the non-decimal number, with the potential for a thousand separator
            // and at the front we have the potential for a $ sign
            // S -> $ - !!! should only fire on the first character
            // actually, can have a negative amount
            // the negative seems pretty 

            input = input.Replace("_", "");
            input = input.Replace(",", "");
            input = input.Replace(".", "");
            input = input.Replace(" ", "");

            var inputItems = input.ToArray();

            var negativeValue = false;

            if (inputItems[0] == '-')
            {
                negativeValue = true;
                inputItems = inputItems.Skip(1).ToArray();
            }

            // last two are the decimal value
            var decimalString = string.Join("", inputItems.Skip(inputItems.Length - 2));
            decimalString = decimalString.CleanNumberString();

            // everything except the last two characters and the first character make up the rest of the number
            var skippedFirstCharItems = inputItems.Skip(1);
            var nonDecimalNumString = string.Join("", skippedFirstCharItems.Take(inputItems.Length - 3));
            nonDecimalNumString = nonDecimalNumString.CleanNumberString();

            var fullNumString = $"{nonDecimalNumString}.{decimalString}";

            input = fullNumString.CleanNumberString();

            if (negativeValue)
            {
                input = "-" + input;
            }

            return input;
        }

        // Theft/BurgIary from Vehicles -> Theft/Burglary from Vehicles
        // this is why you use serif fonts people!
        // calculate lev distance and pick the one that has the shortest as long as below some threshold distance, maybe 10 (or based on length of phrase being checked?)
        public static T ParseOcrEnum<T>(this string input) where T : Enum
        {
            // get all names and descriptions
            // check each one to calculate lev

            var enumNames = Enum.GetNames(typeof(T));
            var enumDescriptions = EnumExtensions.GetDescriptions<T>();
            var enumNameDescriptions = enumNames.Concat(enumDescriptions);

            switch(typeof(T).Name)
            {
                case "TransactionType":
                    input = input.CommonTransactionTypeSwap();
                    break;

                case "LossType":
                    input = input.CommonLossTypeSwap();
                    break;

                case "LossItem":
                    input = input.CommonLossItemSwap();
                    break;

                default:
                    break;
            }

            var maxDistance = int.MaxValue;
            var maxString = "";

            foreach (var i in enumNameDescriptions)
            {
                var nameDescriptionItems = i.Split(' ');

                if (nameDescriptionItems.Length > 2)
                {
                    var distance = input.CalcLevDistance(i, true);

                        if (distance < maxDistance)
                        {
                            maxString = i;
                            maxDistance = distance;
                        }
                }

                else
                {
                    foreach (var j in nameDescriptionItems)
                    {
                        var distance = input.CalcLevDistance(j, true);

                        if (distance < maxDistance)
                        {
                            maxString = i;
                            maxDistance = distance;
                        }
                    }
                }

                
                
            }

            if (maxDistance > input.Length * 3 / 4)
            {
                throw new ArgumentException($"closest match '{maxString}' had a distance greater than allowed threshold");
            }

            T returnEnum;

            try
            {
                returnEnum = maxString.ParseEnumFromDescription<T>();
            }
            catch (ArgumentException)
            {
                return default(T);
            }


            return returnEnum;
        }

        private static string CheckForIncompleteInput(this string input, string[] nameDescriptions)
        {
            foreach (var i in nameDescriptions)
            {
                if (i.SimilarTo(input, true, true))
                {
                    return i;
                }
            }

            return "";
        }

        private static string CommonLossItemSwap(this string input)
        {
            var cleanInput = input.ToLower();

            if (cleanInput.Contains("main"))
            {
                return "MainDwelling";
            }

            if (cleanInput.Contains("additional") || cleanInput.Contains("living") || cleanInput.Contains("expense"))
            {
                return "AdditionalLivingExpense";
            }

            if (cleanInput.Contains("contents"))
            {
                return "Contents";
            }

            if (cleanInput.Contains("trees") || cleanInput.Contains("plants") || cleanInput.Contains("lawns"))
            {
                return "TreesPlantsLawns";
            }

            if (cleanInput.Contains("loss") || cleanInput.Contains("rental") || cleanInput.Contains("income"))
            {
                return "LossOfRentalIncome";
            }

            if (cleanInput.Contains("money") || cleanInput.Contains("securities"))
            {
                return "MoneySecurities";
            }

            return input;
        }

        private static string CommonLossTypeSwap(this string input)
        {
            var cleanInput = input.ToLower();

            if (cleanInput.Contains("premises") && (cleanInput.Contains("burglary") || cleanInput.Contains("theft")))
            {
                return "TheftBurglaryFromPremises";
            }

            if (cleanInput.Contains("escape"))
            {
                return "WaterEscape";
            }

            if (cleanInput.Contains("vehicles") && (cleanInput.Contains("burglary") || cleanInput.Contains("theft")))
            {
                return "TheftBurglaryFromVehicles";
            }

            if 
            (
                cleanInput.Contains("fire") 
                || cleanInput.Contains("lightening") 
                || cleanInput.Contains("explosion") 
                || cleanInput.Contains("smoke") 
                || cleanInput.Contains("damage")
            )
            {
                return "FireLighteningExplosionSmokeDamage";
            }

            if (cleanInput.Contains("windstorm") || cleanInput.Contains("hail"))
            {
                return "WindstormHail";
            }

            if (cleanInput.Contains("falling") || cleanInput.Contains("object"))
            {
                return "FallingObject";
            }

            if (cleanInput.Contains("sewer") || cleanInput.Contains("backup"))
            {
                return "SewerBackup";
            }

            if (cleanInput.Contains("impact"))
            {
                return "ImpactByVehicle";
            }

            if (cleanInput.Contains("ground"))
            {
                return "GroundWater";
            }

            if (cleanInput.Contains("flood") || cleanInput.Contains("pluvial"))
            {
                return "FloodPluvialRain";
            }

            return input;
        }

        private static string CommonTransactionTypeSwap(this string input)
        {
            var cleanInput = input.ToLower();

            //openclaimwithreserve vs reopenclaimwithreserve

            if (cleanInput.Contains("re-open"))
            {
                return "ReopenClaimWithReserve";
            }

            if 
            (
                cleanInput.Contains("open") 
                || cleanInput.Contains("with")
            )
            {
                return "OpenClaimWithReserve";
            }

            if (cleanInput.Contains("partial"))
            {
                return "PartialPayment";
            }

            if (cleanInput.Contains("change"))
            {
                return "ReserveChange";
            }

            if (cleanInput.Contains("appraiser"))
            {
                return "AppraiserExpense";
            }

            if (cleanInput.Contains("expert"))
            {
                return "ExpertExpensePayment";
            }

            if (cleanInput.Contains("first") || cleanInput.Contains("final") || cleanInput.Contains("supplemental"))
            {
                if (cleanInput.Contains("first") || cleanInput.Contains("supplemental"))
                {
                    return "FirstAndFinalOrSupplementalPayment";
                }
                else 
                {
                    return "FinalPayment";
                }
            }

            if (cleanInput.Contains("repayment"))
            {
                return "ClaimRepayment";
            }

            return input;

        }

        public static List<FisClaim> ParsePolicyClaims(string claimsText)
        {
            var claims = new List<FisClaim>();

            var policyMatches = claimsText.GetMatches(@"(HC|AC)\d{3}-\d{8}");

            var lines = claimsText.Split(policyMatches, StringSplitOptions.RemoveEmptyEntries);

            // var claimsTextLines = claimsText.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            // // the first line pulls an 'R' it seems, corresponding to the car?
            // // so what happens with a homecover?

            foreach (var i in lines)
            {
                if (i.IsMatch(ClaimNumberRegex))
                {
                    var claim = new FisClaim();
                    claim.DateOfLoss = ParseDateOfLoss(i);
                    claims.Add(claim);
                    claim.WorkAuthorizationNumber = ParseWorkAuthNum(i);
                }
                
            }

            return claims;
        }

        public static int ParseWorkAuthNum(string line)
        {
            var workAuthString = Regex.Match(line, ClaimNumberRegex).Value;

            return Convert.ToInt32(CleanNumberString(workAuthString.Replace("G", "0")));
        }

        public static DateTime ParseDateOfLoss(string claimsLine)
        {
            var dateMatches = claimsLine.GetMatches(@"\d{2}(-|)[a-zA-Z]{3}(-|)\d{4}");

            var dateOfLossString = dateMatches[0].ToString();

            return ParseDate(dateOfLossString);
        }

        private static string LevParseMonth(string monthText)
        {
            // removing months whose abbreviations already only have a distance of 1
            // ex: if we have juw as a potential month, the distance is 1 for both jun and jul

            monthText = monthText.ToLower();

            if (monthText.Contains("ju"))
            {
                return monthText;
            }

            var months = new string[] 
            {
                "jan",
                "feb",
                "apr",
                "aug",
                "sep",
                "oct",
                "nov",
                "dec",
                "jul",
                "jun"
            };

            foreach (var i in months)
            {
                var distance = i.CalcLevDistance(monthText, true);

                if (distance <= 1)
                {
                    return i;
                }
            }

            return monthText;
        }

        public static bool PostalCodeEqual(this PostalCode x, PostalCode y)
        {
            // clean the codes
            // ANA NAN - pattern

            if (x.Code.Length < 6 || y.Code.Length < 6)
            {
                return false;
            }

            var cleanX = CleanPostalCode(x);
            var cleanY = CleanPostalCode(y);

            return cleanX.Equals(cleanY);
            
        }

        public static string CleanAlphaString(string input)
        {
            input = input.ToLower();
            input = input.Replace("0", "o");
            // we don't know if a 1 is I or L

            return input;
        }

        public static PostalCode CleanPostalCode(PostalCode input)
        {
            var newCode = 
                CleanAlphaString(input.Code[0].ToString()) 
                + CleanNumberString(input.Code[1].ToString())
                + CleanAlphaString(input.Code[2].ToString())
                + CleanNumberString(input.Code[3].ToString())
                + CleanAlphaString(input.Code[4].ToString())
                + CleanNumberString(input.Code[5].ToString());

            return new PostalCode(newCode);
        }

        public static string CleanPolicyString(string policyString)
        {
            policyString = policyString.ToUpper();

            if (policyString.Contains("AC") || policyString.Contains("HC"))
            {
                return policyString;
            }

            if (policyString.CharacterCount('-') >= 2)
            {
                return policyString;
            }
            
            policyString = policyString.Replace("-", "");

            var firstSection = policyString[0].ToString();

            var endSection = policyString.Substring(policyString.Length - 7);

            var middleSection = policyString.Replace(endSection, "").Substring(1);

            return $"{firstSection}-{middleSection}-{endSection}";
        }
    }
}