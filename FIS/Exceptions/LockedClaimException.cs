using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Economical.EcoObjects.FIS
{
    [Serializable]
    public class LockedClaimException : ApplicationException
    {
        public LockedClaimException()
        {
        }

        public LockedClaimException(string message) : base(message)
        {
        }

        public LockedClaimException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LockedClaimException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}