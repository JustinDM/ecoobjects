using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.FIS.Exceptions
{
    [Serializable]
    public class NoValidTransactionProfileException : BusinessException
    {
        public NoValidTransactionProfileException()
        {
        }

        public NoValidTransactionProfileException(string message) : base(message)
        {
        }

        public NoValidTransactionProfileException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoValidTransactionProfileException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}