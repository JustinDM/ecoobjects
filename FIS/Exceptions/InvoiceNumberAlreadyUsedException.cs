using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.FIS.Exceptions
{
    [Serializable]
    public class InvoiceNumberAlreadyUsedException : BusinessException
    {
        public InvoiceNumberAlreadyUsedException()
        {
        }

        public InvoiceNumberAlreadyUsedException(string message) : base(message)
        {
        }

        public InvoiceNumberAlreadyUsedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvoiceNumberAlreadyUsedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}