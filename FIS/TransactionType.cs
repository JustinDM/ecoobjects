using System.ComponentModel;

namespace Economical.EcoObjects.FIS
{
    public enum TransactionType
    {
        // !!! if you're modifying this, 
        // check the FisHelpers.CommonTransactionTypeSwap method to make sure the trigger words are still unique
        // also check the FisClaim.GenerateTransactionSummary method to add handling for that transaction type
        Invalid,
        [Description("Open Claim with Reserve")]
        OpenClaimWithReserve,
        [Description("Partial Payment")]
        PartialPayment,
        [Description("Reserve Change")]
        ReserveChange,
        [Description("Appraiser Expense")]
        AppraiserExpense,
        [Description("Expert Expense Payment")]
        ExpertExpensePayment,
        [Description("First and Final or Supplemental Payment")]
        FirstAndFinalOrSupplementalPayment,
        [Description("Claim Repayment")]
        ClaimRepayment,
        [Description("Re-open Claim with Reserve")]
        ReopenClaimWithReserve,
        [Description("Final Payment")]
        FinalPayment
    }
}
