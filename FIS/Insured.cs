using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Address;

namespace Economical.EcoObjects.FIS
{
    public class Insured
    {
        public string Name { get; set; }
        public string AdditionalName { get; set; }
        public EcoAddress Address { get; set; } = new EcoAddress();
        public string Telephone { get; set; }
        public ObservableCollection<FisPolicy> Policies { get; set; } = new ObservableCollection<FisPolicy>();

        public Insured()
        {
            Policies.CollectionChanged += HandleCollectionChanged;
        }

        private void HandleCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach (var i in e.NewItems)
            {
                if (i is FisPolicy policy)
                {
                    policy.Insured = this;
                }
            }
        }

        public ObservableCollection<FisPolicy> ParsePolicies(string rawPolicyText)
        {
            var policies = new List<FisPolicy>();

            var policyNumbers = rawPolicyText.GetMatches(FisHelpers.PolicyNumberRegex);

            var textLines = rawPolicyText.Split(policyNumbers, StringSplitOptions.RemoveEmptyEntries);

            // assumes there are newlines - need to mirror logic from ParsePolicyClaims

            foreach (var i in policyNumbers)
            {
                var policy = new FisPolicy();
                policy.Number = i.Trim();
                Policies.Add(policy);
            }

            var policyCount = 0;

            for (int i = 0; i < textLines.Length; i++)
            {
                var dateMatches = textLines[i].GetMatches(@"\S{2}-\S{3}-\S{4}");

                if (dateMatches.Length >= 2)
                {
                    Policies[policyCount].StartDate = FisHelpers.ParseDate(dateMatches[0].ToString());
                    Policies[policyCount].EndDate = FisHelpers.ParseDate(dateMatches[1].ToString());
                    policyCount++;
                }
            }

            return Policies;
        }

        private string CleanPolicyString(string dirtyPolicy)
        {
            // HC90740451124
            // HC907-40451124

            dirtyPolicy = dirtyPolicy.Replace("-", "").Replace(" ", "");

            var typeCode = string.Join("", dirtyPolicy.Take(2));
            dirtyPolicy = string.Join("", dirtyPolicy.Skip(2));

            var firstSection = string.Join("", dirtyPolicy.Take(3));
            dirtyPolicy = string.Join("", dirtyPolicy.Skip(3));
            firstSection = FisHelpers.CleanNumberString(firstSection);

            dirtyPolicy = FisHelpers.CleanNumberString(dirtyPolicy);

            return $"{typeCode}{firstSection}-{dirtyPolicy}";
        }
    }
}