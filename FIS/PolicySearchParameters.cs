namespace Economical.EcoObjects.FIS
{
    public struct PolicySearchParameters
    {
        public string FirstName { get; }
        public string LastName { get; }
        public string AdditionalFirstName { get; }
        public string AdditionalLastName { get; }
        public PolicyNameSearchOption NameSearchOption { get; }

        public PolicySearchParameters(string firstName, string lastName, PolicyNameSearchOption searchOption, string additionalFirstName = "", string additionalLastName = "")
        {
            FirstName = firstName;
            LastName = lastName;
            NameSearchOption = searchOption;
            AdditionalFirstName = additionalFirstName;
            AdditionalLastName = additionalLastName;
        }

    }
}