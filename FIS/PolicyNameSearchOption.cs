namespace Economical.EcoObjects.FIS
{
    public enum PolicyNameSearchOption
    {
        Invalid,
        Match,
        Start,
        Contain
    }
}
