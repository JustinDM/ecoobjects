using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoExtensions;
using Economical.EcoObjects.General;
using Economical.EcoObjects.General.Address;
using Economical.EcoObjects.General.Address.Enums;

namespace Economical.EcoObjects.FIS
{
    enum DecPagePolicyType
    {
        Invalid,
        [Description("Private Passenger")]
        PrivatePassenger
    }
    public class DecPage
    {
        public string BrokerName { get; set; }
        public EcoAddress BrokerAddress { get; set; } = new EcoAddress();
        public Vehicle Vehicle { get; set; }
        public EcoAddress InsuredAddress { get; set; } = new EcoAddress();


        // this breaks it:
        // the lessor is there so additional information is present

        // INSURED'S NAME AND ADDRESS (Insured as per SEF No. 5)
        // Konnor  Richmond
        // 206-101 Morrissey Road INSURED - LESSOR
        // Port Moody, BC TOYOTA CREDIT CANADA INC.
        // Canada, V3H 0E6 200-80 MICRO CRT
        // MARKHAM, ON
        // Canada, L3R 9Z5

        public DecPage(string decPageText)
        {
            var decPageLines = decPageText.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            
            var nextLineBrokerName = false;
            var startOfAddress = false;
            var insuredAddressLines = new List<string>();

            var vehicleLine = decPageText.GetMatches(@"(?<=Make\/Model:).+((?=Driver's License No)|(?=Postal Code: ))")[0];
            Vehicle = ParseVehicle(vehicleLine);

            var vehicleYearLine = decPageText.GetMatches(@"(?<=Year:).+((?=Postal Code:)|(?=Additional Drivers)|\S)")[0];

            vehicleYearLine = vehicleYearLine.GetMatches(@"\d{4}")[0];

            Vehicle.Year = Convert.ToInt32(vehicleYearLine.Trim());

            var vinLine = decPageText.GetMatches(@"(?<=Vehicle Identification No.:).+((?=Experience \(Canada\/USA\):)|\S)")[0].Trim();

            if (vinLine.Contains("Experience"))
            {
                var experienceIndex = vinLine.IndexOf("Experience");

                vinLine = vinLine.Substring(0, experienceIndex);
            }
            else if (vinLine.Contains("Driver's"))
            {
                var driverIndex = vinLine.IndexOf("Driver's");

                vinLine = vinLine.Substring(0, driverIndex);
            }

            Vehicle.Vin = vinLine;


            foreach (var i in decPageLines)
            {
                if (i.Contains("AGENT POLICY TYPE INSURER"))
                {
                    nextLineBrokerName = true;
                }
                else if (nextLineBrokerName && string.IsNullOrWhiteSpace(BrokerName))
                {
                    BrokerName = ParseBrokerName(i);
                    nextLineBrokerName = false;
                }

                if (i.Contains("POLICY NUMBER") && string.IsNullOrWhiteSpace(BrokerAddress.City))
                {
                    BrokerAddress.City = ParseCity(i);
                }

                if (i.Contains("INSURED'S NAME AND ADDRESS") && insuredAddressLines.Count == 0)
                {
                    startOfAddress = true;
                }
                else if (startOfAddress)
                {

                    // it's possible that there are multiple insureds
                    // if we take all the items up until we hit the blank line, we go backwards and take the last 3 items
                    // do we have a blank line?

                    if (string.IsNullOrWhiteSpace(i))
                    {
                        startOfAddress = false;
                    }
                    else
                    {
                        insuredAddressLines.Add(i);
                    }

                }

            }
            // want to skip the last line if it contains a postal code - if there are multiples

            var postalCodeMatches = insuredAddressLines.Where(x => x.IsMatch(@"[a-zA-Z]\d[a-zA-Z] \d[a-zA-Z]\d"));

            if (postalCodeMatches.Count() > 1)
            {
                insuredAddressLines.RemoveAt(insuredAddressLines.Count - 1);
            }

            InsuredAddress = ParseInsuredAddress(insuredAddressLines.Skip(insuredAddressLines.Count - 3).ToList());
        }

        private EcoAddress ParseInsuredAddress(List<string> addressLines)
        {
            var address = new EcoAddress();
            // first line is address line
            address.AddressLines.Add(addressLines[0]);

            // second is province and city
            var lineItems = addressLines[1].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            address.City = lineItems[0].Trim();
            address.Province = lineItems[1].Trim().Substring(0, 2).ParseEnumFromDescription<Province>();

            // third line is postal code and country
            // we don't care about country
            lineItems = addressLines[2].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            address.PostalCode = new PostalCode(lineItems[1].GetMatches(@"[a-zA-Z]\d[a-zA-Z] \d[a-zA-Z]\d")[0].Trim());

            return address;
        }

        private string ParseBrokerName(string brokerLine)
        {
            var broker = brokerLine.Replace("Definity Insurance Company", "");

            var enumDescriptions = EnumExtensions.GetDescriptions<DecPagePolicyType>();

            foreach (var i in enumDescriptions)
            {
                broker = broker.Replace(i, "");
            }

            return broker.Trim();
        }

        private string ParseCity(string cityLine)
        {
            var lineItems = cityLine.Split(',');

            return lineItems[0];
        }

        private Vehicle ParseVehicle(string vehicleLine)
        {
            var lineItems = vehicleLine.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            var vehicle = new Vehicle();

            vehicle.Make = lineItems[0].Trim();

            vehicle.Model = string.Join(" ", lineItems.Skip(1)).Trim();

            return vehicle;
        }
    }
}