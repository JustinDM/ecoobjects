using System.ComponentModel;

namespace Economical.EcoObjects.FIS
{
    public enum LossType
    {
        // !!! if you're modifying this, 
        // check the FisHelpers.CommonLossTypeSwap method to make sure the trigger words are still unique
        Invalid,
        [Description("Theft/Burglary from Premises")]
        TheftBurglaryFromPremises,
        [Description("Water Escape")]
        WaterEscape,
        [Description("Theft/Burglary from Vehicles")]
        TheftBurglaryFromVehicles,
        [Description("Fire, Lightening, Explosion, Smoke Damage")]
        FireLighteningExplosionSmokeDamage,
        [Description("Falling Object")]
        FallingObject,
        [Description("Windstorm/Hail")]
        WindstormHail,
        [Description("Sewer Backup")]
        SewerBackup,
        [Description("Impact by Vehicle")]
        ImpactByVehicle,
        [Description("Ground Water")]
        GroundWater,
        [Description("Flood Pluvial (Rain)")]
        FloodPluvialRain,
        Other,
        Collapse,
        Freezing,
        Comprehensive

    }
}
