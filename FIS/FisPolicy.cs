using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Economical.EcoObjects.FIS
{
    public class FisPolicy
    {
        public string Number { get; set; }
        public PolicyType Type { get; set; }
        public Insured Insured { get; set; } = new Insured();
        public ObservableCollection<FisClaim> Claims { get; set; } = new ObservableCollection<FisClaim>();
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public FisPolicy()
        {
            Claims.CollectionChanged += HandleCollectionChanged;
        }

        private void HandleCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach (var i in e.NewItems)
            {
                if (i is FisClaim claim)
                {
                    claim.Policy = this;
                }
            }
        }
    }
}