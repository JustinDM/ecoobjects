using Economical.EcoObjects.General.Address;

namespace Economical.EcoObjects.FIS
{
    public class ThirdPartyDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public EcoAddress Address { get; set; }
        public string Telephone { get; set; }
        public string VehicleLicense { get; set; }
        public string Injury { get; set; }
        public string ThirdPartyNotes { get; set; }
    }
}