using System.ComponentModel;

namespace Economical.EcoObjects.FIS
{
    public enum ClaimSection
    {
        Invalid,
        [Description("General Claim Information")]
        GeneralClaimInformation,
        [Description("Loss Description")]
        LossDescription,
        [Description("Claim Transactions")]
        ClaimTransactions,
        [Description("Intray Entry")]
        IntrayEntry,
        [Description("Police Details")]
        PoliceDetails,
        [Description("Driver Details")]
        DriverDetails,
        [Description("Third Party Details")]
        ThirdPartyDetails,
        [Description("ThirdPartyInsurerDetails")]
        ThirdPartyInsurerDetails,
        [Description("Witness Details")]
        WitnessDetails
    }
}
