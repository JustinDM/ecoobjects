using Economical.EcoObjects.General.Address;

namespace Economical.EcoObjects.FIS
{
    public class ThirdPartyInsurerDetails
    {
        public string Insurer { get; set; }
        public EcoAddress Address { get; set; }
        public string Telephone { get; set; }
        public string AdjusterName { get; set; }
        public string FileNumber { get; set; }
        public int RecoveryPercentage { get; set; }
        public double RecoveryAmount { get; set; }
        public string InsurerNotes { get; set; }
    }
}