namespace Economical.EcoObjects.FIS
{
    public enum ClaimStatus
    {
        Invalid,
        Closed,
        Open
    }
}
