using System.ComponentModel;

namespace Economical.EcoObjects.FIS
{
    public enum PolicyType
    {
        Invalid,
        Tenant,
        Home,
        Condominium,
        [Description("Strata Titled")]
        StrataTitled,
        Secondary,
        [Description("Mobile Home")]
        MobileHome,
        Autocover
    }
}
