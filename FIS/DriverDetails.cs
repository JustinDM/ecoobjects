using System;

namespace Economical.EcoObjects.FIS
{
    public class DriverDetails
    {
        public string DriverName { get; set; }
        public string DriverLicense { get; set; }
        public DateTime ExpiryDate { get; set; }
        public uint YearsExperience { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string VehicleLicense { get; set; }
        public string SerialNumber { get; set; }
        public string DeclaredValue { get; set; }

    }
}