using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Economical.EcoObjects.General.EventArgs;
using Economical.EcoObjects.General.Interfaces;

namespace Economical.EcoObjects.FIS
{
    public class FisClaim
    {
        public FisPolicy Policy { get; set; } = new FisPolicy();
        public string BrokerPolicyNumber { get; set; }
        public string CustomerReference { get; set; }
        // this is the claim number
        public int WorkAuthorizationNumber { get; set; }
        public string DriversLicense { get; set; }
        public string VehicleLicense { get; set; }
        public string Telephone { get; set; }
        public ClaimStatus Status { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateReported { get; set; }
        public DateTime DateOfLoss { get; set; }
        public DateTime DateClosed { get; set; }
        public string FamilyAdjuster { get; set; }
        public string ExternalAssignment { get; set; }
        public string CustomerEmailAddress { get; set; }
        public string Catastrophe { get; set; }
        public string LossDescription { get; set; }
        public List<FisTransaction> Transactions { get; set; } = new List<FisTransaction>();
        public List<TransactionSummary> TransactionSummaries { get; set; } = new List<TransactionSummary>();
        public PoliceDetails PoliceDetails { get; set; } = new PoliceDetails();
        public DriverDetails DriverDetails { get; set; } = new DriverDetails();
        public ThirdPartyDetails ThirdPartyDetails { get; set; } = new ThirdPartyDetails();
        public ThirdPartyInsurerDetails ThirdPartyInsurerDetails { get; set; } = new ThirdPartyInsurerDetails();
        public WitnessDetails WitnessDetails { get; set; } = new WitnessDetails();

        public List<TransactionSummary> GenerateTransactionSummaries()
        {
            if (Transactions.Count == 0)
            {
                throw new ArgumentException("no transactions in claim!");
            }
            
            foreach (var i in Transactions)
            {
                // check existing transaction summaries for one with the same
                var targetSummary = TransactionSummaries.FirstOrDefault(
                    x => x.LossItem == i.LossItem 
                    && x.LossType == i.LossType);

                if (targetSummary == null)
                {
                    var summary = new TransactionSummary()
                    {
                        LossType = i.LossType,
                        LossItem = i.LossItem
                    };

                    TransactionSummaries.Add(summary);

                    targetSummary = summary;

                }

                if (i.Amount > 0)
                {
                    // missing reopen claim with reserve -> I assume that's a reserve change
                    if (i.Type == TransactionType.AppraiserExpense || i.Type == TransactionType.ExpertExpensePayment)
                    {
                        targetSummary.Expense += i.Amount;
                    }

                    else if (i.Type == TransactionType.OpenClaimWithReserve || i.Type == TransactionType.ReserveChange || i.Type == TransactionType.ReopenClaimWithReserve)
                    {
                        targetSummary.Reserve += i.Amount;
                    }

                    else if (i.Type == TransactionType.PartialPayment || i.Type == TransactionType.FirstAndFinalOrSupplementalPayment || i.Type == TransactionType.FinalPayment || i.Type == TransactionType.ClaimRepayment)
                    {
                        targetSummary.Payment += i.Amount;
                    }

                    else
                    {
                        throw new ArgumentException("Transaction type not recognized");
                    }
                }
                    
            }

            return TransactionSummaries;
        }

        public List<FisTransaction> IngestTransactions(DataTable table)
        {
            foreach (DataRow i in table.Rows)
            {
                var transaction = new FisTransaction(i);
                Transactions.Add(transaction);
            }

            return Transactions;
        }
    }
}