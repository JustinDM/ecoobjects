namespace Economical.EcoObjects.FIS
{
    public class PoliceDetails
    {
        public string FileNumber { get; set; }
        public string Department { get; set; }
        public string OfficeName { get; set; }
        public string Charges { get; set; }
    }
}