using System;
using System.Data;
using Economical.EcoExtensions;

namespace Economical.EcoObjects.FIS
{
    public class FisTransaction
    {
        public DateTime Date { get; set; }
        public TransactionType Type { get; set; }
        public LossType LossType { get; set; }
        public LossItem LossItem { get; set; }
        public double Amount { get; set; }
        public string Invoice { get; set; }
        public string Vendor { get; set; }

        public FisTransaction() { }
        public FisTransaction(DataRow row)
        {
            // can read headers, maybe shouldn't trust
            // currently don't see a need for knowing the date so will allow it to read incorrectly
            var dateString = row[0].ToString();
            if (!string.IsNullOrWhiteSpace(dateString))
            {
                Date = FisHelpers.ParseDate(dateString);
            }
            else
            {
                Date = new DateTime();
            }
            Type = row[1].ToString().ParseOcrEnum<TransactionType>();
            LossType = row[2].ToString().ParseOcrEnum<LossType>();
            LossItem = row[3].ToString().ParseOcrEnum<LossItem>();
            Amount = Convert.ToDouble(row[4].ToString().CleanAmountString().Replace("$", ""));
        }

        public FisTransaction(TransactionType type, LossType lossType, LossItem lossItem, Double amount, string vendor, string invoice)
        {
            Type = type;
            LossType = lossType;
            LossItem = lossItem;
            Amount = amount;
            Vendor = vendor;
            Invoice = invoice;
        }
    }
}