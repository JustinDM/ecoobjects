using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Helpers;

namespace Economical.EcoObjects.FIS
{
    public class FisTransactionComparer : IEqualityComparer<FisTransaction>
    {
        public bool Equals(FisTransaction x, FisTransaction y)
        {
            var potentialDateMatch = x.Date == y.Date || x.Date == new DateTime() || y.Date == new DateTime();

            Console.WriteLine(potentialDateMatch);

            if (
                potentialDateMatch
                && x.Type == y.Type
                && x.LossType == y.LossType
                && x.LossItem == y.LossItem
                && x.Amount.ToString("0.00") == y.Amount.ToString("0.00")
            )
            {
                return true;
            }

            return false;
        }

        public int GetHashCode(FisTransaction obj)
        {
            // can't use date in hash because we're saying dates don't need to match to return true
            long hCode = Convert.ToInt64(obj.Amount) ^ ((uint)obj.Type) ^ ((uint)obj.LossItem) ^ ((uint)obj.LossType);

            return hCode.GetHashCode();
        }
    }
}