using System;
using System.Data;

namespace Economical.EcoObjects.FIS
{
    public class TransactionSummary
    {
        public LossType LossType { get; set; }
        public LossItem LossItem { get; set; }
        public double Incurred { get; set; }
        public double Reserve { get; set; }
        public double Payment { get; set; }
        public double Outstanding { get; set; }
        public double Expense { get; set; }
        public double Recovery { get; set; }   
        public double Fee { get; set; }
        public TransactionSummary(DataRow row)
        {
            LossType = row[0].ToString().ParseOcrEnum<LossType>();
            LossItem = row[1].ToString().ParseOcrEnum<LossItem>();
            Incurred = Convert.ToDouble(row[2].ToString().CleanAmountString());
            Reserve = Convert.ToDouble(row[3].ToString().CleanAmountString());
            Payment = Convert.ToDouble(row[4].ToString().CleanAmountString());
            Outstanding = Convert.ToDouble(row[5].ToString().CleanAmountString());
            Expense = Convert.ToDouble(row[6].ToString().CleanAmountString());
            Recovery = Convert.ToDouble(row[7].ToString().CleanAmountString());
            Fee = Convert.ToDouble(row[8].ToString().CleanAmountString());
        }

        public TransactionSummary() { }
    }
}