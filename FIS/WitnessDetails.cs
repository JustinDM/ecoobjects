namespace Economical.EcoObjects.FIS
{
    public class WitnessDetails
    {
        public string Witness1 { get; set; }
        public string Witness2 { get; set; }
        public string Witness3 { get; set; }
    }
}