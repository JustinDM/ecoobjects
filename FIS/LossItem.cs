using System.ComponentModel;

namespace Economical.EcoObjects.FIS
{
    public enum LossItem
    {
        // !!! if you're modifying this, 
        // check the FisHelpers.CommonLossItemSwap method to make sure the trigger words are still unique
        Invalid,
        Contents,
        [Description("Main Dwelling")]
        MainDwelling,
        [Description("Additional Living Expense")]
        AdditionalLivingExpense,
        [Description("Trees/Plants/Lawns")]
        TreesPlantsLawns,
        [Description("Loss of Rental Income")]
        LossOfRentalIncome,
        Outbuilding,
        [Description(@"Money/Securities")]
        MoneySecurities,
        Other
    }
}
