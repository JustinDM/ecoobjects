using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.CnlrLabellingKeying.DTOs;
using Economical.EcoObjects.General.Helpers;

namespace Economical.EcoObjects.CnlrLabellingKeying
{
    public class LkMapperProfiles : MapperProfiles
    {
        public LkMapperProfiles() : base()
        {
            CreateMap<LkConfig, LkInboundConfigDto>();

            CreateMap<LkUnitInfo, LkInboundUnitInfoDto>();

            CreateMap<LkInboundConfigDto, LkConfig>();

            CreateMap<LkInboundUnitInfoDto, LkUnitInfo>();

            CreateMap<LkUpdateUnitInfoDto, LkUnitInfo>();

            CreateMap<LkUnitInfo, LkOutboundUnitInfoDto>();

            CreateMap<LkConfig, LkOutboundConfigDto>();
        }
    }
}