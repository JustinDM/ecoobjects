using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Address;
using Economical.EcoObjects.General.DTOs;

namespace Economical.EcoObjects.CnlrLabellingKeying.DTOs
{
    public class LkOutboundUnitInfoDto : OutboundUnitInfoDto
    {
        public string CompanyString { get; set; }
        public string NoticePolicyNumber { get; set; }
        public string SystemPolicyNumber { get; set; }
        public string NoticeFirstName { get; set; }
        public string NoticeLastName { get; set; }
        public string SystemFirstName { get; set; }
        public string SystemLastName { get; set; }
        public DateTime DateOfLoss { get; set; }
        public EcoAddress InsuredAddress { get; set; }
        public string LossDescription { get; set; }
        public string ClaimType { get; set; }
        public string SubjectLine { get; set; }
        public DateTime ReceivedDate { get; set; }
    }
}