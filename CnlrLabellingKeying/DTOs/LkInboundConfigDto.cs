using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.DTOs;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.CnlrLabellingKeying.DTOs
{
    public class LkInboundConfigDto : InboundConfigDto
    {
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string QueueName { get; set; }
        public string ResourceFolder { get; set; }
        public string LabellingReportName { get; set; }
        public string KeyingReportName { get; set; }
    }
}