using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.DTOs;

namespace Economical.EcoObjects.CnlrLabellingKeying.DTOs
{
    public class LkOutboundConfigDto : OutboundConfigDto
    {
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string QueueName { get; set; }
        public string ResourceFolder { get; set; }
        public string LabellingReportName { get; set; }
        public string KeyingReportName { get; set; }
    }
}