using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Economical.EcoObjects.CnlrLabellingKeying.DTOs;
using Economical.EcoObjects.General.DTOs;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;
using Economical.EcoObjects.General.Helpers.RepoCommunicator;

namespace Economical.EcoObjects.CnlrLabellingKeying
{
    public class LkRepoCommunicator : RepoCommunicator
    {
        public LkRepoCommunicator(RepoConfig config, AuthCommunicator authComm, Profile profile) 
            : base (config, authComm, profile)
        {
            
        }

        public override async Task<Config> WriteConfigAsync(Config config)
        {
            var LkConfig = config as LkConfig;

            var loginResponse = await LoginAsync();

            var configDto = _mapper.Map<LkInboundConfigDto>(LkConfig);

            var content = GenerateObjectContent(configDto);

            var response = await PostAsync(_config.ConfigPath(LkConfig.Handler.Id), content);

            var outboundDto = IngestResponse<LkOutboundConfigDto>(response);

            _mapper.Map(outboundDto, LkConfig);

            return LkConfig;
        }

        public override async Task<Config> UpdateConfigAsync(Config config)
        {
            var LkConfig = config as LkConfig;

            var loginResponse = await LoginAsync();

            var configDto = _mapper.Map<LkInboundConfigDto>(LkConfig);

            var content = GenerateObjectContent(configDto);

            var response = await PutAsync(_config.ConfigPath(LkConfig.Handler.Id), content);

            var outboundDto = IngestResponse<LkOutboundConfigDto>(response);

            _mapper.Map(outboundDto, LkConfig);

            return LkConfig;
        }

        public override async Task<UnitInfo> WriteUnitInfoAsync(UnitInfo info)
        {
            var ncInfo = info as LkUnitInfo;

            var loginResponse = await LoginAsync();

            var infoDto = _mapper.Map<LkInboundUnitInfoDto>(ncInfo);

            var content = GenerateObjectContent(infoDto);

            // need additional endpoints

            var response = await PostAsync(_config.UnitInfoPath(ncInfo.Unit.Id), content);

            var transact = IngestResponse<OutboundUnitDto>(response);

            _mapper.Map(transact.UnitInfo as LkUnitInfo, ncInfo);

            return transact.UnitInfo;
        }

        public override async Task<UnitInfo> UpdateUnitInfoAsync(UnitInfo info)
        {
            var ncInfo = info as LkUnitInfo;

            var loginResponse = await LoginAsync();

            var infoDto = _mapper.Map<LkInboundUnitInfoDto>(ncInfo);

            var content = GenerateObjectContent(infoDto);

            var response = await PutAsync(_config.UnitInfoPath(ncInfo.Unit.Id), content);

            var transact = IngestResponse<OutboundUnitDto>(response);

            // does our mapper cover this?
            _mapper.Map(transact.UnitInfo as LkUnitInfo, ncInfo);

            return transact.UnitInfo;
        }

        public async Task<LkConfig> WriteTimeoutAsync(Timeout timeout)
        {
            var loginResponse = await LoginAsync();

            var timeoutDto = _mapper.Map<InboundTimeoutDto>(timeout);

            var content = GenerateObjectContent(timeoutDto);

            var response = await PostAsync(_config.TimeoutPath(Guid.NewGuid()), content);

            var responseConfig = IngestResponse<LkOutboundConfigDto>(response);

            var config = _mapper.Map<LkConfig>(responseConfig);

            return config;
        }

        public override UnitInfo WriteUnitInfo(UnitInfo info)
        {
            throw new System.NotImplementedException();
        }

        public override UnitInfo UpdateUnitInfo(UnitInfo info)
        {
            throw new System.NotImplementedException();
        }

        public override Config WriteConfig(Config config)
        {
            throw new System.NotImplementedException();
        }

        public override Config UpdateConfig(Config config)
        {
            throw new System.NotImplementedException();
        }

        
    }
}