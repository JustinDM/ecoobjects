using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.EventArgs;
using Economical.EcoObjects.General.Models;
using Newtonsoft.Json;

namespace Economical.EcoObjects.CnlrLabellingKeying
{
    public class LkConfig : Config
    {
        private int delayBefore;
        public int DelayBefore
        {
            get { return delayBefore; }
            set 
            { 
                delayBefore = value;
                CallUpdateEvent(this, new PropertyUpdateEventArgs("DelayBefore", value.ToString()));
            }
        }

        private int delayAfter;
        public int DelayAfter
        {
            get { return delayAfter; }
            set 
            { 
                delayAfter = value;
                CallUpdateEvent(this, new PropertyUpdateEventArgs("DelayAfter", value.ToString())); 
            }
        }

        private string queueName;
        public string QueueName
        {
            get { return queueName; }
            set 
            { 
                queueName = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("QueueName", value.ToString()));
            }
        }

        private string resourceFolder;
        public string ResourceFolder
        {
            get { return resourceFolder; }
            set 
            { 
                resourceFolder = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("ResourceFolder", value.ToString()));
            }
        }

        private string labellingReportName;
        public string LabellingReportName
        {
            get { return labellingReportName; }
            set 
            { 
                labellingReportName = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("LabellingReportName", value.ToString()));
            }
        }

        private string keyingReportName;
        public string KeyingReportName
        {
            get { return keyingReportName; }
            set 
            { 
                keyingReportName = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("KeyingReportName", value.ToString()));
            }
        }
        

        public Guid? TimeoutId { get; set; }

        private Timeout timeout;
        public Timeout Timeout
        {
            get { return timeout; }
            set 
            { 
                timeout = value;
                CallInitializeEvent(this, new PropertyInitializedEventArgs("LkConfig.Timeout", value));
                timeout.PropertyUpdated += CallUpdateEvent;
            }
        }

        public LkConfig() { }

        public LkConfig(string fileText)
        {
            var config = IngestConfigFile(fileText) as LkConfig;

            Duplicate(config);
        }

        public override void Duplicate(Config config)
        {
            var lkConfig = config as LkConfig;

            DelayBefore = lkConfig.DelayBefore;
            DelayAfter = lkConfig.DelayAfter;
            Timeout = lkConfig.Timeout;
            QueueName = lkConfig.QueueName;
            ResourceFolder = lkConfig.ResourceFolder;
            LabellingReportName = lkConfig.LabellingReportName;
            KeyingReportName = lkConfig.KeyingReportName;
        }

        public override Config IngestConfigFile(string fileText)
        {
            var config = JsonConvert.DeserializeObject<LkConfig>(fileText);

            return config;
        }
        
    }
}