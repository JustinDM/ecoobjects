using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Address;
using Economical.EcoObjects.General.Address.Enums;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;
using Economical.EcoObjects.General;
using Economical.EcoObjects.General.EventArgs;

namespace Economical.EcoObjects.CnlrLabellingKeying
{
    public class LkUnitInfo : UnitInfo
    {
        private Company company;
        public Company Company
        {
            get { return company; }
            set 
            { 
                company = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("Company", value.ToString()));
            }
        }

        private string noticePolicyNumber;
        public string NoticePolicyNumber
        {
            get { return noticePolicyNumber; }
            set 
            { 
                noticePolicyNumber = value;
                CallUpdateEvent(this, new PropertyUpdateEventArgs("NoticePolicyNumber", value));
            }
        }

        private string systemPolicyNumber;
        public string SystemPolicyNumber
        {
            get { return systemPolicyNumber; }
            set 
            { 
                systemPolicyNumber = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("SystemPolicyNumber", value));
            }
        }

        private string noticeFirstName;
        public string NoticeFirstName
        {
            get { return noticeFirstName; }
            set 
            { 
                noticeFirstName = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("NoticeFirstName", value));
            }
        }

        private string noticeLastName;
        public string NoticeLastName
        {
            get { return noticeLastName; }
            set 
            { 
                noticeLastName = value;
                CallUpdateEvent(this, new PropertyUpdateEventArgs("NoticeLastName", value));
            }
        }

        private string systemFirstName;
        public string SystemFirstName
        {
            get { return systemFirstName; }
            set 
            { 
                systemFirstName = value;
                CallUpdateEvent(this, new PropertyUpdateEventArgs("SystemFirstName", value)); 
            }
        }

        private string systemLastName;
        public string SystemLastName
        {
            get { return systemLastName; }
            set 
            { 
                systemLastName = value;
                CallUpdateEvent(this, new PropertyUpdateEventArgs("SystemLastName", value)); 
            }
        }

        private DateTime dateOfLoss;
        public DateTime DateOfLoss
        {
            get { return dateOfLoss; }
            set 
            { 
                dateOfLoss = value;
                CallUpdateEvent(this, new PropertyUpdateEventArgs("DateOfLoss", value.ToString(Configurations.PrettyDateFormat)));
            }
        }

        private EcoAddress insuredAddress;
        public EcoAddress InsuredAddress
        {
            get { return insuredAddress; }
            set 
            { 
                insuredAddress = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("InsuredAddress", value.ToString()));
            }
        }

        private string lossDescription;
        public string LossDescription
        {
            get { return lossDescription; }
            set 
            { 
                lossDescription = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("LossDescription", value));
            }
        }

        private string phoneNumber;
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set 
            { 
                phoneNumber = value;
                CallUpdateEvent(this, new PropertyUpdateEventArgs("PhoneNumber", value));
            }
        }

        private string claimType;
        public string ClaimType
        {
            get { return claimType; }
            set 
            { 
                claimType = value;
                CallUpdateEvent(this, new PropertyUpdateEventArgs("ClaimType", value.ToString()));
            }
        }

        private string subjectLine;
        public string SubjectLine
        {
            get { return subjectLine; }
            set 
            { 
                subjectLine = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("SubjectLine", value.ToString()));
            }
        }

        private DateTime receivedDate;
        public DateTime ReceivedDate
        {
            get { return receivedDate; }
            set 
            { 
                receivedDate = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("ReceivedDate", value.ToString(Configurations.PrettyDateFormat)));
            }
        }
        
        private string catCode;
        public string CatCode
        {
            get { return catCode; }
            set 
            { 
                catCode = value; 
                CallUpdateEvent(this, new PropertyUpdateEventArgs("CatCode", value.ToString()));    
            }
        }

        private List<string> catKeywords;
        public List<string> CatKeywords
        {
            get { return catKeywords; }
            set { catKeywords = value; }
        }
                

        public LkUnitInfo() 
        {
            InsuredAddress = new EcoAddress();
            NoticePolicyNumber = string.Empty;
            NoticeFirstName = string.Empty;
            NoticeLastName = string.Empty;
            SystemPolicyNumber = string.Empty;
            SystemFirstName = string.Empty;
            SystemLastName = string.Empty;
            PhoneNumber = string.Empty;
            ClaimType = string.Empty;
            SubjectLine = string.Empty;
            CatCode = string.Empty;
            CatKeywords = new List<string>();
        }

        public LkUnitInfo(string emailBodyText) : base()
        {
            emailBodyText = emailBodyText.Replace(Environment.NewLine, " ").Replace("  ", " ");

            var ingestFailed = false;

            try 
            {
                IngestAvivaNotice(emailBodyText);
            }
            catch(Exception)
            {
                ingestFailed = true;
            }

            if (ingestFailed)
            {
                ingestFailed = false;
                
                try
                {
                    IngestCooperatorsNotice(emailBodyText);
                }
                catch (System.Exception)
                {
                    ingestFailed = true;
                }
            }

            if (ingestFailed)
            {
                ingestFailed = false;

                try
                {
                    IngestIntactNotice(emailBodyText);
                }
                catch (System.Exception)
                {
                    ingestFailed = true;
                }
            }

            if (ingestFailed)
            {
                throw new ArgumentException($"All ingestion attempts failed");
            }
              
        }

        private void IngestCooperatorsNotice(string emailBodyText)
        {
            ParseInsured(emailBodyText, @"(?<=Your Insured:).+(?=Your Driver)");
            ParsePolicy(emailBodyText, @"(?<=Your Policy #:).+(?=Your Insured)");
            ParseDateOfLoss(emailBodyText, @"(?<=Date of Loss:).+(?=Our Policy)", "MMMM d, yyyy");
            ParseLossDescription(emailBodyText, @"(?<=Loss Details:).+(?=This letter)");
        }
        
        private void IngestAvivaNotice(string emailBodyText)
        {
            ParseInsured(emailBodyText, @"(?<=TP Insured:).+(?=TP Driver)");
            ParsePolicy(emailBodyText, @"(?<=TP Policy:).+(?=TP Claim)");
            ParseDateOfLoss(emailBodyText, @"(?<=Date of Loss:).+(?=Location)", "yyyy-MM-dd");
            ParseLossDescription(emailBodyText, @"(?<=Details:).+(?=Fault)");
        }

        private void IngestIntactNotice(string emailBodyText)
        {
            ParseInsured(emailBodyText, @"(?<=Your Insured:).+(?=Your Vehicle)");
            ParseDateOfLoss(emailBodyText, @"(?<=Date of Loss:).+(?=Location of Loss)", "dd/MM/yyyy h:mm tt");
            ParseLossDescription(emailBodyText, @"(?<=Description of Loss:).+$");
        }

        private void ParseInsured(string emailBodyText, string regex)
        {
            var insuredMatches = emailBodyText.GetMatches(regex);

            if (insuredMatches.Length == 0)
            {
                throw new ArgumentException("failed to find a match for insured name");
            }

            var insured = insuredMatches[0];
            insured = insured.Trim().Replace("  ", " ");

            var insuredNameItems = insured.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            NoticeFirstName = insuredNameItems[0].Trim();

            if (insuredNameItems.Length > 1)
            {
                NoticeLastName = insuredNameItems[1].Trim();
            }
        }

        private void ParsePolicy(string emailBodyText, string regex)
        {
            var policyMatches = emailBodyText.GetMatches(regex);

            if (policyMatches.Length == 0)
            {
                throw new ArgumentException("failed to find a match for insured policy number");
            }

            NoticePolicyNumber = policyMatches[0].Trim();
        }

        private void ParseDateOfLoss(string emailBodyText, string regex, string dateFormat)
        {
            var dateOfLossMatches = emailBodyText.GetMatches(regex);

            if (dateOfLossMatches.Length == 0)
            {
                throw new ArgumentException("failed to find date of loss matches");
            }

            DateOfLoss = DateTime.ParseExact(dateOfLossMatches[0].Trim(), dateFormat, CultureInfo.InvariantCulture);
        }

        private void ParseLossDescription(string emailBodyText, string regex)
        {
            var lossDescriptionMatches = emailBodyText.GetMatches(regex);

            if (lossDescriptionMatches.Length == 0)
            {
                throw new ArgumentException("failed to find loss description matches");
            }

            LossDescription = lossDescriptionMatches[0].Trim();
        }

        public override void CleanData()
        {
            // no cleaning required
        }

        public string GenerateNoticeFileName()
        {
            var fileName = $"{ReceivedDate.ToString(Configurations.FileDateTimeFormat)}_{NoticeFirstName}_{NoticeLastName}_NoticeOfLoss";

            return StringExtensions.CleanFileName(fileName);
        }

        public string GenerateEmailFileName()
        {
            var fileName = $"{ReceivedDate.ToString(Configurations.FileDateTimeFormat)}_{NoticeFirstName}_{NoticeLastName}_SupportingEmail";

            return StringExtensions.CleanFileName(fileName);
        }

        public override Dictionary<string, string> GenerateReportRow()
        {
            var reportRow = new Dictionary<string, string>()
            {
                { "SubjectLine", SubjectLine },
                { "ReceivedDate", ReceivedDate.ToString(Configurations.PrettyDateTimeFormat) },
                { "Company", Company.ToString() },
                { "NoticePolicyNumber", NoticePolicyNumber },
                { "SystemPolicyNumber", SystemPolicyNumber },
                { "NoticeFirstName", NoticeFirstName },
                { "NoticeLastName", NoticeLastName },
                { "SystemFirstName", SystemFirstName },
                { "SystemLastName", SystemLastName },
                { "DateOfLoss", DateOfLoss.ToString(Configurations.PrettyDateFormat) },
                { "InsuredAddress", InsuredAddress.ToString() },
                { "LossDescription", LossDescription },
                { "PhoneNumber", PhoneNumber },
                { "ClaimType", ClaimType },
                { "CatCode", CatCode },
                { "CatKeywords", string.Join(", ", CatKeywords).Trim() }
                
            };

            return reportRow;
        }

        public override void IngestReportRow(DataRow row)
        {
            SubjectLine = row["SubjectLine"].ToString();
            ReceivedDate = DateTime.ParseExact(row["ReceivedDate"].ToString(),Configurations.PrettyDateTimeFormat, CultureInfo.InvariantCulture);
            Company = row["Company"].ToString().ParseEnumFromDescription<Company>();
            NoticePolicyNumber = row["NoticePolicyNumber"].ToString();
            SystemPolicyNumber = row["SystemPolicyNumber"].ToString();
            NoticeFirstName = row["NoticeFirstName"].ToString();
            NoticeLastName = row["NoticeLastName"].ToString();
            SystemFirstName = row["SystemFirstName"].ToString();
            SystemLastName = row["SystemLastName"].ToString();
            DateOfLoss = DateTime.ParseExact(row["DateOfLoss"].ToString(), Configurations.PrettyDateFormat, CultureInfo.InvariantCulture);
            InsuredAddress = new EcoAddress(row["InsuredAddress"].ToString());
            LossDescription = row["LossDescription"].ToString();
            PhoneNumber = row["PhoneNumber"].ToString();
            ClaimType = row["ClaimType"].ToString();
            CatCode = row["CatCode"].ToString();
            CatKeywords = row["CatKeywords"].ToString().Split(new string[] {","}, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public override Dictionary<string, string> GenerateFrenchReportRow()
        {
            throw new NotImplementedException();
        }
    }
}