using System;
using System.Text.Json;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.IccLossPayments
{
    public class IlConfig : Config
    {
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string NoteText { get; set; }
        public string ReferenceText { get; set; }
        public string ResourceFolder { get; set; }
        public int MaxProcessCount { get; set; }
        public string CdsQueueName { get; set; }
        public string FamilyQueueName { get; set; }
        public Timeout Timeout { get; set; }
        public Guid? TimeoutId { get; set; }
        public string ReportFolder { get; set; }

        public IlConfig() { }

        public IlConfig(string fileText)
        {
            var config = IngestConfigFile(fileText);

            Duplicate(config);
        }

        public override void Duplicate(Config config)
        {
            var ilConfig = config as IlConfig;

            DelayBefore = ilConfig.DelayBefore;
            DelayAfter = ilConfig.DelayAfter;
            NoteText = ilConfig.NoteText;
            ReferenceText = ilConfig.ReferenceText;
            ResourceFolder = ilConfig.ResourceFolder;
            MaxProcessCount = ilConfig.MaxProcessCount;
            CdsQueueName = ilConfig.CdsQueueName;
            FamilyQueueName = ilConfig.FamilyQueueName;
            Timeout = ilConfig.Timeout;
            ReportFolder = ilConfig.ReportFolder;
        }

        public override Config IngestConfigFile(string fileText)
        {
            var config = JsonSerializer.Deserialize<IlConfig>(fileText);

            return config;
        }
    }
}