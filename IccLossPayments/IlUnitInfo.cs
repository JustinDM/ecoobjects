using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Address.Enums;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.IccLossPayments
{
    public class IlUnitInfo : UnitInfo
    {
        public int ClaimNumber { get; set; }
        public string ClaimModifier { get; set; }
        public int InvoiceId { get; set; }
        public DateTime DateOfLoss { get; set; }
        public string Insured { get; set; }
        public double Amount { get; set; }
        public int ReportRow { get; set; }
        public IlUnitInfo() { }
        public IlUnitInfo(DataRow row, int reportRow)
        {
            ClaimNumber = Convert.ToInt32(row["Claim #"].ToString());
            
            var province = row["Prov"].ToString().ParseEnumFromDescription<Province>();

            if (province == Province.QC)
            {
                ClaimModifier = "-Bien-PD-1";
            }
            else
            {
                ClaimModifier = "-Prop-PD-1";
            }

            InvoiceId = Convert.ToInt32(row["InvoiceID"].ToString());
            DateOfLoss = DateTime.ParseExact(row["Date of Loss"].ToString(),"MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            Insured = row["Insured"].ToString();
            Amount = Convert.ToDouble(row["Total"].ToString().Replace("$", "").Replace(",", ""));
            ReportRow = reportRow;
        }

        public override void CleanData()
        {
            // nothing to clean
        }

        public override Dictionary<string, string> GenerateReportRow()
        {
            var reportInfo = new Dictionary<string, string>()
            {
                { "ClaimNumber", ClaimNumber.ToString() },
                { "ClaimModifier", ClaimModifier },
                { "InvoiceID", InvoiceId.ToString() },
                { "DateOfLoss",  DateOfLoss.ToString(General.Helpers.Configurations.PrettyDateFormat) },
                { "Insured", Insured },
                { "Amount", Amount.ToString("0.00") },
                { "ReportRow", ReportRow.ToString() }
            };

            return reportInfo;
        }

        public bool IsCdsClaim()
        {
            if (ClaimNumber >= 1000000)
            {
                return true;
            }

            return false;
        }

        public override void IngestReportRow(DataRow row)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, string> GenerateFrenchReportRow()
        {
            throw new NotImplementedException();
        }
    }
}