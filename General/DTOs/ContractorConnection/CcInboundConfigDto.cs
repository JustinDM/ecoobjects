using System;

namespace General.DTOs.ContractorConnection
{
    public class CcInboundConfigDto
    {
        public Guid HandlerId { get; set; }
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string ReviewerEmail { get; set; }
        public string SenderEmail { get; set; }
        public string LogFolder { get; set; }
        public int NumberOfEmails { get; set; }
        public string SourceEmailAddress { get; set; }
        public double AddressMatchThreshold { get; set; }
        public string NoteText { get; set; }
        public string[] RequiredReportColumns { get; set; }
    }
}