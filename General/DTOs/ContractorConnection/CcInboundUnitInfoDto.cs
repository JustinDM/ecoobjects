using System;

namespace Economical.EcoObjects.General.DTOs.ContractorConnection
{
    public class CcInboundUnitInfoDto
    {
        public DateTime UnitDateTime { get; set; }
        public int SequenceNumber { get; set; }
        public string InsuredName { get; set; }
        public string PolicyNumber { get; set; }
        public double ClaimNumber { get; set; }
        public DateTime LossDate { get; set; }
        public int InvoiceNumber { get; set; }
        public int CallId { get; set; }
        public double InvoiceAmount { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string FeeTypeString { get; set; }
        public string ExpenseTypeString { get; set; }
        public string PayeeTypeString { get; set; }
        public string DeskAdjuster { get; set; }
        public string BranchString { get; set; }
        public string SourceSystemString { get; set; }
        public int AddressCallId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AddressPolicy { get; set; }
        public int AddressClaim { get; set; }
        public string AddressAdjuster { get; set; }
        public DateTime CallDate { get; set; }
        public DateTime AddressLossDate { get; set; }
        public double Fee { get; set; }
        public double Tax { get; set; }
        public double Total { get; set; }
    }
}