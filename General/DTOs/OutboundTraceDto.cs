using System;

namespace Economical.EcoObjects.General.DTOs
{
    public class OutboundTraceDto
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public string PolarityString { get; set; }
        public string SystemString { get; set; }
        public string TypeString { get; set; }
    }
}