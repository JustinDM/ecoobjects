namespace Economical.EcoObjects.General.DTOs
{
    public class OutboundUnitInfoDto
    {
        public int Guid { get; set; }
        public int AttemptNumber { get; set; }
    }
}