using System;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.General.DTOs
{
    public class OutboundUnitDto
    {
        public Guid Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string BotId { get; set; }
        public string ProcessString { get; set; }
        public string StatusString { get; set; }
        public UnitInfo UnitInfo { get; set; }
    }
}