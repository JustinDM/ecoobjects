using System;
using Economical.EcoObjects.General.Enums;

namespace Economical.EcoObjects.General.DTOs.Update
{
    public class UpdateHandlerDto
    {
        public DateTime? EndDate { get; set; }
        public string StatusString { get; set; }
    }
}