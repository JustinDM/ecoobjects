using System;

namespace Economical.EcoObjects.General.DTOs
{
    public class OutboundConfigDto
    {
        public Guid Id { get; set; }
    }
}