namespace Economical.EcoObjects.General.DTOs.NovaScotiaOpening
{
    public class NoInboundConfigDto
    {
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string[] ReviewerEmails { get; set; }
        public string NoteText { get; set; }
        public string ResourceFolder { get; set; }
    }
}