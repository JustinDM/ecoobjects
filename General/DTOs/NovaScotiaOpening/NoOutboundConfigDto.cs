using System;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.General.DTOs.NovaScotiaOpening
{
    public class NoOutboundConfigDto
    {
        public Guid Id { get; set; }
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string[] ReviewerEmails { get; set; }
        public string NoteText { get; set; }
        public Timeout Timeout { get; set; }
        public string ResourceFolder { get; set; }
    }
}