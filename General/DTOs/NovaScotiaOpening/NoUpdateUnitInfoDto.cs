using System;

namespace Economical.EcoObjects.General.DTOs.NovaScotiaOpening
{
public class NoUpdateUnitInfoDto
    {
        public string StatusString { get; set; }
        public double ClaimNumber { get; set; }
        public bool IsUberPolicy { get; set; }
        public string Company { get; set; }
        public DateTime LetterDate { get; set; }
        public DateTime DateOfLoss { get; set; }
        public string Policy { get; set; }
        public string Adjuster { get; set; }
        public string CustomerEmail { get; set; }
        public bool ApprovedForEmailing { get; set; }
        public bool IsCommercialOffline { get; set; }
    }
}