namespace Economical.EcoObjects.General.DTOs
{
    public class InboundTimeoutDto
    {
        public int Short { get; set; }
        public int Medium { get; set; }
        public int Long { get; set; }
    }
}