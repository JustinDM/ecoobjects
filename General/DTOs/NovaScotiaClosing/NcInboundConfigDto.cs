namespace Economical.EcoObjects.General.DTOs.NovaScotiaClosing
{
    public class NcInboundConfigDto
    {
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string[] ReviewerEmails { get; set; }
        public string NoteText { get; set; }
        public string ResourceFolder { get; set; }
    
    }
}