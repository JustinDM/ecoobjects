using System.Text.Json;

namespace Economical.EcoObjects.General.Email
{
    public class EmailConfig
    {
        public string EmailAddress { get; set; }
        public string Domain { get; set; }
        public string MailFolder { get; set; }
        
        public EmailConfig(string fileText)
        {
            LoadConfig(fileText);
        }

        public EmailConfig() { }

        private void LoadConfig(string fileText)
        {
            var config = JsonSerializer.Deserialize<EmailConfig>(fileText);
            Duplicate(config);
        }

        private void Duplicate(EmailConfig config)
        {
            EmailAddress = config.EmailAddress;
            Domain = config.Domain;
            MailFolder = config.MailFolder;
        }
    }
}