
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.General.Interfaces
{
    public interface IConfigLoader
    {
        Config IngestConfigFile(string fileText);
        void Duplicate(Config config);
    }
}