using System.Data;

namespace Economical.EcoObjects.General.Interfaces
{
    public interface IReportGenerator
    {
        DataTable GenerateReportTable();
    }
}