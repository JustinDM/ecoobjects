using System;
using Economical.EcoObjects.General.EventArgs;

namespace Economical.EcoObjects.General.Interfaces
{
    public interface INotifyPropertyUpdated
    {
        event EventHandler<PropertyUpdateEventArgs> PropertyUpdated;
    }
}