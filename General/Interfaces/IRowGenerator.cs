using System.Collections.Generic;
using System.Data;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.General.Interfaces
{
    public interface IRowGenerator
    {
        Dictionary<string, string> GenerateReportRow();

        void IngestReportRow(DataRow row);
    }
}