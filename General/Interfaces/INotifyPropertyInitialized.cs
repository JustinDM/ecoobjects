using System;
using Economical.EcoObjects.General.EventArgs;

namespace Economical.EcoObjects.General.Interfaces
{
    public interface INotifyPropertyInitialized
    {
        event EventHandler<PropertyInitializedEventArgs> PropertyInitialized;
    }
}