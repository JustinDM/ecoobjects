namespace Economical.EcoObjects.General.Interfaces
{
    public interface ICleanable
    {
        void CleanData();
    }
}