namespace Economical.EcoObjects.General.Interfaces
{
    public interface IStateChangeLogger
    {
        void LogStateChange(object obj);
    }
}