namespace Economical.EcoObjects.General.EventArgs
{
    public class PropertyInitializedEventArgs
    {
        public string PropertyName { get; set; }
        public object Property { get; set; }
        public PropertyInitializedEventArgs(string propertyName, object property)
        {
            PropertyName = propertyName;
            Property = property;
        }
    }
}