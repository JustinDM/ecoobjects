namespace Economical.EcoObjects.General.EventArgs
{
    public class PropertyUpdateEventArgs
    {
        public string PropertyName { get; set; }
        public string Value { get; set; }

        public PropertyUpdateEventArgs(string propertyName, string value)
        {
            PropertyName = propertyName;
            Value = value;
        }
    }
}