using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class InvalidClaimStatusException : BusinessException
    {
        public InvalidClaimStatusException()
        {
        }

        public InvalidClaimStatusException(string message) : base(message)
        {
        }

        public InvalidClaimStatusException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidClaimStatusException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}