using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class DestructionDateNotEnteredException : BusinessException
    {
        public DestructionDateNotEnteredException()
        {
        }

        public DestructionDateNotEnteredException(string message) : base(message)
        {
        }

        public DestructionDateNotEnteredException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DestructionDateNotEnteredException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}