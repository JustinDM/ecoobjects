using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class BlankClaimIdException : BusinessException
    {
        public BlankClaimIdException()
        {
        }

        public BlankClaimIdException(string message) : base(message)
        {
        }

        public BlankClaimIdException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BlankClaimIdException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}