using System;
using System.Runtime.Serialization;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class DuplicatePaymentPresentException : BusinessException
    {
        public DuplicatePaymentPresentException()
        {
        }

        public DuplicatePaymentPresentException(string message) : base(message)
        {
        }

        public DuplicatePaymentPresentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DuplicatePaymentPresentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}