using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class InvalidPolicyNumberException : BusinessException
    {
        public InvalidPolicyNumberException()
        {
        }

        public InvalidPolicyNumberException(string message) : base(message)
        {
        }

        public InvalidPolicyNumberException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidPolicyNumberException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}