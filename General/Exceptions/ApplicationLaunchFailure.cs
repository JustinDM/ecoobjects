using System;
using System.Runtime.Serialization;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class ApplicationLaunchFailure : ApplicationException
    {
        public ApplicationLaunchFailure()
        {
        }

        public ApplicationLaunchFailure(string message) : base(message)
        {
        }

        public ApplicationLaunchFailure(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ApplicationLaunchFailure(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}