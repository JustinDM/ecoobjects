using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class PolicyNotFoundException : BusinessException
    {
        public PolicyNotFoundException()
        {
        }

        public PolicyNotFoundException(string message) : base(message)
        {
        }

        public PolicyNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PolicyNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}