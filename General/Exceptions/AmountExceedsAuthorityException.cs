using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class AmountExceedsAuthorityException : BusinessException
    {
        public AmountExceedsAuthorityException()
        {
        }

        public AmountExceedsAuthorityException(string message) : base(message)
        {
        }

        public AmountExceedsAuthorityException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AmountExceedsAuthorityException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}