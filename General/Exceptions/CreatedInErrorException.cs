using System;
using System.Runtime.Serialization;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class CreatedInErrorException : BusinessException
    {
        public CreatedInErrorException()
        {
        }

        public CreatedInErrorException(string message) : base(message)
        {
        }

        public CreatedInErrorException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CreatedInErrorException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}