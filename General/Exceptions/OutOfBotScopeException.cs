using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class OutOfBotScopeException : BusinessException
    {
        public OutOfBotScopeException()
        {
        }

        public OutOfBotScopeException(string message) : base(message)
        {
        }

        public OutOfBotScopeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected OutOfBotScopeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}