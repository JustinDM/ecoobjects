using System;
using System.Runtime.Serialization;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class PostalCodeNotMatchedException : AddressNotMatchedException
    {
        public PostalCodeNotMatchedException()
        {
        }

        public PostalCodeNotMatchedException(string message) : base(message)
        {
        }

        public PostalCodeNotMatchedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PostalCodeNotMatchedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}