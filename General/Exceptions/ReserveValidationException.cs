using System;
using System.Runtime.Serialization;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class ReserveValidationException : BusinessException
    {
        public ReserveValidationException()
        {
        }

        public ReserveValidationException(string message) : base(message)
        {
        }

        public ReserveValidationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ReserveValidationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}