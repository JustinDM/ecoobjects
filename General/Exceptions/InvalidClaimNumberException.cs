using System;
using System.Runtime.Serialization;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class InvalidClaimNumberException : BusinessException
    {
        public InvalidClaimNumberException()
        {
        }

        public InvalidClaimNumberException(string message) : base(message)
        {
        }

        public InvalidClaimNumberException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidClaimNumberException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}