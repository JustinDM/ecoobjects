using System;
using System.Runtime.Serialization;

namespace Economical.EcoObjects.General
{
    [Serializable]
    public class AddressNotMatchedException : BusinessException
    {
        public AddressNotMatchedException()
        {
        }

        public AddressNotMatchedException(string message) : base(message)
        {
        }

        public AddressNotMatchedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AddressNotMatchedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}