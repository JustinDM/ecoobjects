using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General.Attributes
{
    public class FrenchNameAttribute : Attribute
    {
        public string FrenchName { get; }

        public FrenchNameAttribute(string name)
        {
            FrenchName = name;
        }
    }
}