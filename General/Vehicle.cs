using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoExtensions;

namespace Economical.EcoObjects.General
{
    public class Vehicle
    {
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Vin { get; set; }

        public override string ToString()
        {
            return this.ClassToString();
        }
    }
}