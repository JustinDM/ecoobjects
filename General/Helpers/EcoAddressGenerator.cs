using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Address;
using Economical.EcoObjects.General.Address.Enums;

namespace Economical.EcoObjects.General.Helpers
{
    [Obsolete("Use the EcoAddress constructor instead with enablePartial set to true")]
    public static class EcoAddressGenerator
    {
        public static EcoAddress GenerateAddressFromPolicyLine(string addressLine)
        {
            // 1520 HWY 537,, WAHNAPITAE, ON, P0M 3C0
            // 27 LAIRD AVE, ESSEX, ON, N8M 1R5
            // 1118 VICTORIA AVE., BATHURST, N.B., E2A 3J9
            // 653 ST ANNE STREET, BATHURST,NEW BRUNSWICK, E2A 2M5

            var lineItems = addressLine.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            var address = new EcoAddress()
            {
                PostalCode = new PostalCode(lineItems[lineItems.Length - 1].Trim()),
                Province = lineItems[lineItems.Length - 2].Trim().Replace(".", "").ParseEnumFromDescription<Province>(),
                City = lineItems[lineItems.Length - 3].Trim(),
                AddressLines = new List<string>(lineItems.Take(lineItems.Length - 3))
            };

            return address;
        }
    }
}