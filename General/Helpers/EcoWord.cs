using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General.Helpers
{
    public static class EcoWord
    {
        public static string WriteToWord(string filePath, string text)
        {
            var app = new NetOffice.WordApi.Application();

            var doc = app.Documents.Open(filePath);
            Exception failureException = null;

            try
            {
                var range = doc.Range(0, 0);
                range.Text = text;
                doc.Save();
            }
            catch (System.Exception ex)
            {
                failureException = ex;
            }
            finally
            {
                doc.Close();
                app.Quit();
                if (failureException != null)
                {
                    throw failureException;
                }
            }

            return filePath;
        }
    }
}