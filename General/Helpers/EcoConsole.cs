using System.Diagnostics;

namespace Economical.EcoObjects.General.Helpers
{
    public static class EcoConsole
    {
        public static string CallConsoleCommand(string command)
        {
            var process = new Process();

            process.StartInfo.FileName = "cmd.exe";
            process.StartInfo.Arguments = $"/C {command}";

            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.Start();

            var output = process.StandardOutput.ReadToEnd();

            process.Close();

            return output;
        }
    }
}