using AutoMapper;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.DTOs;
using Economical.EcoObjects.General.DTOs.Update;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.General.Helpers
{
    public class MapperProfiles : Profile
    {
        public MapperProfiles()
        {
            CreateMap<InboundTimeoutDto, Timeout>();

            CreateMap<Timeout, InboundTimeoutDto>();

            CreateMap<UpdateHandlerDto, Handler>()
                .ForMember(h => h.Status, opt => opt.MapFrom(
                    src => src.StatusString.ParseEnumFromDescription<CompletionStatus>()
                )).ReverseMap();

            CreateMap<UpdateUnitDto, Unit>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(
                    src => src.StatusString.ParseEnumFromDescription<CompletionStatus>()))
                    .ReverseMap()
                    .ForMember(dest => dest.StatusString, opt => opt.MapFrom(
                        src => src.Status.ToString()
                    ));

            CreateMap<Unit, OutboundUnitDto>()
                .ForMember(dest => dest.ProcessString, opt => opt.MapFrom(
                    src => src.Process.ToString()))
                .ForMember(dest => dest.StatusString, opt => opt.MapFrom(
                    src => src.Status.ToString()))
                .ReverseMap()
                    .ForMember(dest => dest.Process, opt => opt.MapFrom(
                        src => src.ProcessString.ParseEnumFromDescription<RpaProcess>()
                    ))
                    .ForMember(dest => dest.Status, opt => opt.MapFrom(
                        src => src.StatusString.ParseEnumFromDescription<CompletionStatus>()
                    ));


        }
    }
}