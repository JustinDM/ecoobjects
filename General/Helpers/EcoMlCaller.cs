using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General.Helpers
{
    public static class EcoMlCaller
    {
        public static string FisAutoLossTypePrediction(string lossDescription)
        {
            var lossType = string.Empty;

            using (var client = GenerateClient())
            {
                var stringContent = new StringContent($"\"{lossDescription}\"", Encoding.UTF8, "application/json");

                var response = client.PostAsync("losstype/predict", stringContent).Result;

                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                    throw new HttpRequestException($"Request failed with code '{response.StatusCode}'");
                }

                lossType = response.Content.ReadAsStringAsync().Result;
            }

            return lossType;
        }

        private static HttpClient GenerateClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(Configurations.EcoMlBase);

            return client;
        }
    }

    
}