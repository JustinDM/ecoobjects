using Economical.EcoObjects.General.Helpers.RepoCommunicator;
using Newtonsoft.Json;

namespace Economical.EcoObjects.General.Helpers
{
    public static class Configurations
    {
        public static string ReportDateFormat { get; } = "yyyyMMdd";
        public static string ReportTimeFormat { get; } = "hhmmss";
        public static string PrettyDateFormat { get; } = "yyyy-MM-dd";
        public static string PrettyTimeFormat { get; } = "HH:mm:ss";
        public static string PrettyDateTimeFormat { get; } = $"{PrettyDateFormat} {PrettyTimeFormat}";
        public static string CdsDateFormat { get; } = "dd-MMM-yyyy";
        public static string ReportDateTimeFormat { get { return ReportDateFormat + ReportTimeFormat; } }
        public static string FileDateFormat { get; } = "yyyy_MM_dd";
        public static string FileTimeFormat { get; } = "HH_mm_ss";
        public static string FileDateTimeFormat { get { return $"{FileDateFormat}_{FileTimeFormat}"; } }

        public static JsonSerializerSettings EcoLoggerJsonSettings { get { return new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }; }}
        public static string EcoUtilsBase { get; } = "http://pmrk1inrpa01.eig.ecogrp.ca/ecoutils/";
        public static string EcoMlBase { get; } = "http://pmrk1inrpa01.eig.ecogrp.ca/ecoml/";
        public static JsonSerializerSettings LogRepoJsonSettings { get 
        { 
            return new JsonSerializerSettings() 
            { 
                TypeNameHandling = TypeNameHandling.Objects, 
                SerializationBinder = new LogSerializationBinder(), 
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore 
            };
        }}
    }
}