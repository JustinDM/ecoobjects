using System;
using System.IO;
using iText.IO.Image;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;

namespace Economical.EcoObjects.General.Helpers
{
    public static class DocPrinter
    {
        public static string PrintToPdf(string filePath, string outputDirectory = null)
        {
            string outputFilePath;

            if (outputDirectory == null)
            {
                // if output directory is null, just output to same directory
                outputFilePath = Path.Combine(Path.GetDirectoryName(filePath), Path.GetFileNameWithoutExtension(filePath)) +  ".pdf";
            }
            else
            {
                outputFilePath = Path.Combine(outputDirectory, Path.GetFileNameWithoutExtension(filePath)) + ".pdf";
            }

            var app = new NetOffice.WordApi.Application();

            var doc = app.Documents.Open(filePath);

            var failed = false;

            try
            {
                //doc.SaveAs(outputFilePath);

                doc.ExportAsFixedFormat(outputFilePath, NetOffice.WordApi.Enums.WdExportFormat.wdExportFormatPDF);
            }
            catch (Exception)
            {
                failed = true;
            }
            finally
            {
                doc.Close();
                app.Quit();
                if (failed)
                {
                    throw new Exception("unable to export file");
                }
            }

            return outputFilePath;
        }

        public static string ImageToPdf(string filePath)
        {
            string outputFilePath;

            outputFilePath = Path.ChangeExtension(filePath, ".pdf");

            // these should all be usings
            
            using (PdfWriter writer = new PdfWriter(outputFilePath))
            using (var pdfDoc = new PdfDocument(writer))
            using (var doc = new Document(pdfDoc))
            {
                try
                {
                    var imageData = ImageDataFactory.Create(filePath);
                    Image image = new Image(imageData);
                    doc.SetMargins(0, 0, 0, 0);
                    if (image.GetImageWidth() > image.GetImageHeight())
                    {
                        image.SetRotationAngle(Math.PI / 180 * 270);
                    }
                    doc.Add(image);
                }
                catch (Exception ex)
                {
                    doc.Close();
                    throw ex;
                }

            }
            
            return outputFilePath;
        }
    }
}