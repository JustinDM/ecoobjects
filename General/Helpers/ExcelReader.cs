using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ClosedXML.Excel;

namespace Economical.EcoObjects.General.Helpers
{
    public class ExcelReader
    {
        public static DataTable ReadRange(string filePath, string sheetName = "", int skip = 0)
        {
            var wb = new XLWorkbook(filePath);

            IXLRangeRows rowRange;

            if (string.IsNullOrWhiteSpace(sheetName))
            {
                rowRange = wb.Worksheet(1).RangeUsed().RowsUsed();
            }

            else
            {
                rowRange = wb.Worksheet(sheetName).RangeUsed().RowsUsed();
            }

            IEnumerable<IXLRangeRow> workingRange;

            if (skip != 0)
            {
                workingRange = rowRange.Skip(skip - 1);
            }

            else
            {
                workingRange = rowRange.Skip(0);
            }

            var headerRow = workingRange.Take(1);

            var db = IngestHeaderRow(headerRow);

            var dataRange = workingRange.Skip(1);

            IngestData(db, dataRange);

            return db;
        }

        private static DataTable IngestHeaderRow(IEnumerable<IXLRangeRow> rr)
        {
            var cellValue = string.Empty;
            var db = new DataTable();

            var headerRow = rr.First();

            var colCount = 1;

            do
            {
                cellValue = headerRow.Cell(colCount).Value.ToString();

                if (!string.IsNullOrWhiteSpace(cellValue))
                {
                    db.Columns.Add(cellValue);
                    colCount++;
                }

            } while (!string.IsNullOrWhiteSpace(cellValue));

            return db;
        }


        private static void IngestData(DataTable db, IEnumerable<IXLRangeRow> rows)
        {
            var numCols = db.Columns.Count;

            foreach (var i in rows)
            {
                var dbRow = db.NewRow();

                for (var x = 1; x <= numCols; x++)
                {
                    dbRow[x - 1] = i.Cell(x).Value.ToString();
                }

                db.Rows.Add(dbRow);
            }
        }

        public static string WriteRange(DataTable db, string filePath, string sheetName = "Sheet1")
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentNullException("filePath is null");
            }

            var wb = new XLWorkbook();
            wb.Worksheets.Add(db, sheetName);
            wb.SaveAs(filePath);

            return filePath;
        }

        public static string WriteCell(string value, string filePath, int row, string col, string sheetName = "Sheet1")
        {
            if (string.IsNullOrWhiteSpace(filePath) || string.IsNullOrWhiteSpace(col))
            {
                throw new ArgumentException("filePath or col is null");
            }

            var wb = new XLWorkbook(filePath);

            var sheet = wb.Worksheet(sheetName);
            
            sheet.Cell(row, col).Value = value;
            wb.Save();

            return value;
        }

        public static string WriteCell(string value, string filePath, int row, string col, int sheetIndex = 1)
        {
            if (string.IsNullOrWhiteSpace(filePath) || string.IsNullOrWhiteSpace(col))
            {
                throw new ArgumentException("filePath or col is null");
            }

            var wb = new XLWorkbook(filePath);

            var sheet = wb.Worksheet(sheetIndex);
            
            sheet.Cell(row, col).Value = value;
            wb.Save();

            return value;
        }

        public static string ReadCell(string filePath, int row, string col, string sheetName = "Sheet1")
        {
            if (string.IsNullOrWhiteSpace(filePath) || string.IsNullOrWhiteSpace(col))
            {
                throw new ArgumentException("filePath or col is null");
            }

            var wb = new XLWorkbook(filePath);

            var sheet = wb.Worksheet(sheetName);

            return sheet.Cell(row, col).Value.ToString();
        }

        public static string ReadCell(string filePath, int row, string col, int sheetIndex = 1)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentException($"'{nameof(filePath)}' cannot be null or empty.", nameof(filePath));
            }

            if (string.IsNullOrEmpty(col))
            {
                throw new ArgumentException($"'{nameof(col)}' cannot be null or empty.", nameof(col));
            }

            var wb = new XLWorkbook(filePath);

            var sheet = wb.Worksheet(sheetIndex);

            return sheet.Cell(row, col).Value.ToString();
        }
    }
}