using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Economical.EcoObjects.General.Models;
using Newtonsoft.Json;

namespace Economical.EcoObjects.General.Helpers
{
    public static class EcoLogger
    {
        // read all
        public static List<Handler> ReadFiles(string logRoot)
        {
            var handlers = ReadFiles(new DateTime(), DateTime.Now, logRoot);

            return handlers;
        }

        // can we read based off types?
        // we want to read all logs from a specific time range?
        public static List<Handler> ReadFiles(DateTime startDate, DateTime endDate, string logRoot)
        {
            var handlers = new List<Handler>();

            var logFiles = IngestYearFolders(startDate, endDate, logRoot);

            foreach (var file in logFiles)
            {
                var fileLines = File.ReadAllLines(file);

                foreach (var line in fileLines)
                {
                    var handler = JsonConvert.DeserializeObject<Handler>(line);
                    handlers.Add(handler);
                }
            }

            return handlers;
        }

        private static List<string> IngestYearFolders(DateTime startDate, DateTime endDate, string root)
        {
            var logFiles = new List<string>();

            var yearFolders = Directory.GetDirectories(root);

            // should probably order them
            foreach (var i in yearFolders)
            {
                var pathItems = i.Split(new char[] {'/'}, StringSplitOptions.RemoveEmptyEntries);

                var year = Convert.ToInt32(pathItems.Last());

                if (year >= startDate.Year)
                {
                    IngestMonthFolders(startDate, endDate, i);
                }
            }

            return logFiles;
        }

        private static List<string> IngestMonthFolders(DateTime startDate, DateTime endDate, string root)
        {
            var logFiles = new List<string>();

            var monthFolders = Directory.GetDirectories(root);

            foreach (var i in monthFolders)
            {
                var month = Convert.ToInt32(GetParentFolderName(i));

                if (month >= startDate.Month)
                {
                    IngestLogFiles(startDate, endDate, i);
                }
            }


            return logFiles;
        }

        private static List<string> IngestLogFiles(DateTime startDate, DateTime endDate, string root)
        {
            var logFiles = new List<string>();

            var rootFiles = Directory.GetFiles(root);

            foreach (var i in rootFiles)
            {
                var fileDay = Convert.ToInt32(GetFileDate(i));

                if (fileDay >= startDate.Day)
                {
                    logFiles.Add(i);
                }
            }

            return logFiles;
        }

        // won't work for files - do not use outside until fixed
        private static string GetParentFolderName(string path)
        {
            var pathItems = path.Split(new char[] {'/'}, StringSplitOptions.RemoveEmptyEntries);
            var parentFolder = pathItems.Last();

            return parentFolder;
        }

        private static string GetFileDate(string path)
        {
            var fileName = Path.GetFileName(path);

            return fileName.Replace(".log", "").Replace("Handlers", "");
        }

        

        public static Handler WriteHandler(Handler handler, string rootPath)
        {
            var logFolder = GenerateFolders(rootPath);
            var handlerFile = Path.Combine(logFolder, $"Handlers{DateTime.Now.Day}.log");
            LogItem(handler, handlerFile);

            return handler;
        }

        private static void LogItem(object item, string logFile)
        {
            var itemString = JsonConvert.SerializeObject(item, item.GetType(), Configurations.EcoLoggerJsonSettings);

            var fileExist = File.Exists(logFile);

            using (var writer = new StreamWriter(logFile, fileExist))
            {
                writer.WriteLine(itemString);
            }
        }

        private static string GenerateFolders(string root)
        {
            var date = DateTime.Now;

            var logFolder = Path.Combine(root, date.Year.ToString(), date.Month.ToString());

            Directory.CreateDirectory(logFolder);

            return logFolder;
        }
    
    }
}