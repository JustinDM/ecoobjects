using System;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.General.Helpers
{
    public static class TraceExtensions
    {
        public static Trace GenerateTrace(ITraceGenerator input, string message)
        {
            var log = new Trace(message);

            // the below feels ugly - I think there's a cleaner way but would nee to have a unified parentId in Trace and I don't think EF will handle that
            // should be able to test what type the ITraceGenerator object is and split based on if we get Handler or UnitLo

            if (input.GetType() == typeof(Handler))
            {
                log.Handler = input as Handler;
                log.HandlerId = input.Id;
            }

            else if (input.GetType() == typeof(Unit))
            {
                log.Unit = input as Unit;
                log.UnitId = input.Id;
            }

            else
            {
                throw new ArgumentException("the given ITraceGenerator object can only be of type 'Handler' or 'Unit'");
            }

            return log;
        }

        public static Trace GeneratePropChangedTrace(string property, string value)
        {
            var trace = new Trace($"Property '{property}' set to value '{value}'");

            return trace;
        }

        public static Trace GenerateUnitAddedTrace(Guid UnitId, Handler parent)
        {
            var trace = new Trace($"Unit '{UnitId}' added");

            trace.Handler = parent;
            trace.HandlerId = parent.Id;


            return trace;
        }

        public static Trace GeneratePropertyInitializedTrace(string propName, ITraceGenerator parent)
        {
            var trace = new Trace($"Property '{propName}' initialized");

            switch (parent)
            {
                case Handler handler:
                    trace.Handler = handler;
                    trace.HandlerId = handler.Id;
                    break;

                case Unit Unit:
                    trace.Unit = Unit;
                    trace.UnitId = Unit.Id;
                    break;
                default:
                    throw new ArgumentException($"Parent type '{parent.GetType()}' is invalid");
            }


            return trace;
        }

        public static Trace GeneratePositiveTrace(this ITraceGenerator input, string message, LogType type, EconomicalSystem system)
        {
            return GenerateTrace(input, message);
        }

        public static Trace GenerateNegativeTrace(this ITraceGenerator input, string message, LogType type, EconomicalSystem system)
        {
            return GenerateTrace(input, message);
        }

        public static Trace GenerateNeutralTrace(this ITraceGenerator input, string message, LogType type, EconomicalSystem system)
        {
            return GenerateTrace(input, message);
        }
    }
}