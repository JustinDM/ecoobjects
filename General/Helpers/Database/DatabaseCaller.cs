using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General.Helpers.Database
{
    public class DatabaseCaller
    {
        private DbEnvironment DbEnv { get; set; }
        private string Username { get; set; }
        private string Password { get; set; }

        public DatabaseCaller(DbEnvironment env, string username, string password)
        {
            Username = username;
            Password = password;
            DbEnv = env;
        }

        public DataTable CallDatabase(string queryText)
        {
            var dataSource = string.Empty;

            switch (DbEnv)
            {
                case DbEnvironment.Dbar:
                    dataSource = "dwat1uxoa05:1539/EBSST3";
                    break;

                case DbEnvironment.Prod:
                    dataSource = "pwat1uxod99:1521/SSQUERYP";
                    break;

                default:
                    throw new ArgumentException($"DbEnvironment '{DbEnv.ToString()}' is not supported.");
            }

            var connectionString = CreateConnection(dataSource);

            var queryTable = QueryDatabase(connectionString, queryText);

            return queryTable;
        }

        private OracleConnection CreateConnection (string dataSource)
        {
            var connectionString = new OracleConnectionStringBuilder
            {
                UserID = Username,
                Password = Password,
                DataSource = dataSource
            };


            return new OracleConnection(connectionString.ToString());
        }

        private DataTable QueryDatabase(OracleConnection connection, string queryText)
        {
            connection.Open();

            Exception exc = new Exception();

            var callFailed = false;

            var table = new DataTable();

            try
            {
                var command = new OracleCommand(queryText, connection);
                var reader = command.ExecuteReader();

                var firstRead = true;

                while (reader.Read())
                {
                    var fieldCount = reader.FieldCount;

                    if (firstRead)
                    {
                        for (int i = 0; i < fieldCount; i++)
                        {
                            table.Columns.Add(reader.GetName(i));
                        }
                    }

                    var row = table.NewRow();

                    for (int i = 0; i < fieldCount; i++)
                    {
                        row[i] = reader.GetString(i);
                    }

                    table.Rows.Add(row);

                    firstRead = false;
                }

            }
            catch (Exception ex)
            {
                exc = ex;
                callFailed = true;
            }
            finally
            {
                connection.Close();
            }

            if (callFailed)
            {
                throw exc;
            }

            return table;

        }
    }
}
