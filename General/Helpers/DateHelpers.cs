using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nager.Date;

namespace Economical.EcoObjects.General.Helpers
{
    public static class DateHelpers
    {
        // 12/7/2022 15:21
        // assigned same format
        // return decimal of full day
        // rcvd to assign hours
        // assigned to complete - day
        // assigned to complete - hours
        public static void DetermineIfHoliday()
        {
            var newYears = new DateTime(2022, 1, 2);

            var holidays = DateSystem.GetPublicHolidays(2022, CountryCode.CA);

            foreach (var i in holidays)
            {
                Console.WriteLine(i.Date.ToString());
            }

        }
    }
}