using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General.Helpers
{
    public class AuthCommunicator : HttpClient
    {
        public AuthConfig Config { get; set; }

        public AuthCommunicator(AuthConfig config)
        {
            Config = config;
            BaseAddress = new Uri(Config.BaseAddress);
        }

        public async Task<AuthLoginResponse> Login()
        {
            var credentialDictionary = new Dictionary<string, string>()
            {
                { "username", Config.Username },
                { "password", Config.Password }
            };

            var stringContent = new StringContent(JsonSerializer.Serialize(credentialDictionary), Encoding.UTF8, "application/json");

            var response  = await PostAsync(Config.AuthEndpoint, stringContent);

            if (!response.IsSuccessStatusCode)
            {
                throw new ArgumentException("Unable to login");
            }

            var responseContent = await response.Content.ReadAsStringAsync();

            var loginResponse = JsonSerializer.Deserialize<AuthLoginResponse>(responseContent, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });

            return loginResponse;
        }
        
    }
}