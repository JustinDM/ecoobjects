namespace Economical.EcoObjects.General.Helpers
{
    public class AuthUser
    {
        public int Id { get; set; }
        public string Username { get; set; }
    }
}