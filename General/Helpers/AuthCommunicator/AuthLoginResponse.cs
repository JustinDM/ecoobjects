namespace Economical.EcoObjects.General.Helpers
{
    public class AuthLoginResponse
    {
        public string Token { get; set; }
        public AuthUser User { get; set; }
    }
}