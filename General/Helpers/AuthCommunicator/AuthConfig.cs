namespace Economical.EcoObjects.General.Helpers
{
    public class AuthConfig
    {
        public string BaseAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string AuthEndpoint { get; set; }
    }
}