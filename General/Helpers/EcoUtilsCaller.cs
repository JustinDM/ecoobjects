using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Economical.EcoObjects.General.EcoUtils;
using Economical.EcoObjects.General.Helpers;
using Newtonsoft.Json;

namespace Economical.EcoObjects.General.Helpers
{
    public static class EcoUtilsCaller
    {
        public static User[] GetUserByCharId(string charId)
        {
            User[] users;

            using(var client = GenerateUserClient())
            {
                var response = client.GetAsync($"charid/{charId}").Result;

                var responseContent = response.Content.ReadAsStringAsync().Result;

                users = JsonConvert.DeserializeObject<User[]>(responseContent);
            }    

            return users;
        }

        public static User[] GetUsersByName(string name)
        {
            User[] users;

            using(var client = GenerateUserClient())
            {
                var response = client.GetAsync($"name/{name}").Result;

                var responseContent = response.Content.ReadAsStringAsync().Result;

                users = JsonConvert.DeserializeObject<User[]>(responseContent);
            }

            return users;
        }

        public static User GetUserEmail(User user)
        {
            var userString = JsonConvert.SerializeObject(user);
            var stringContent = new StringContent(userString, Encoding.UTF8, "application/json");

            using (var client = GenerateUserClient())
            {
                var response = client.PostAsync("email", stringContent).Result;

                user = JsonConvert.DeserializeObject<User>(response.Content.ReadAsStringAsync().Result);
            }

            return user;
        }

        public static User GetUserManager(User user)
        {
            var userString = JsonConvert.SerializeObject(user);
            var stringContent = new StringContent(userString, Encoding.UTF8, "application/json");

            using (var client = GenerateUserClient())
            {
                var response = client.PostAsync("manager", stringContent).Result;

                user = JsonConvert.DeserializeObject<User>(response.Content.ReadAsStringAsync().Result);
            }

            return user;
        }

        private static HttpClient GenerateUserClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(Configurations.EcoUtilsBase + "user/");

            return client;
        }

        public static string[] GetEmailByCharacterId(string charId)
        {
            ValidateCharId(charId);

            string[] emails;

            using (var client = GenerateClient())
            {
                var response = client.GetAsync($"emaillookup/{charId}").Result;

                var responseContent = response.Content.ReadAsStringAsync().Result;

                emails = responseContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            }

            return emails;
        }

        public static async Task<string[]> GetEmailByCharacterIdAsync(string charId)
        {
            ValidateCharId(charId);

            string[] emails;

            using (var client = GenerateClient())
            {
                var response = await client.GetAsync($"emaillookup/{charId}");

                var responseContent = await response.Content.ReadAsStringAsync();

                emails = responseContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            }

            return emails;
        }

        public static string[] GetManagerEmail(string charId)
        {
            ValidateCharId(charId);

            string[] emails;

            using (var client = GenerateClient())
            {
                var response = client.GetAsync($"manageremaillookup/{charId}").Result;

                var responseContent = response.Content.ReadAsStringAsync().Result;
                
                emails = responseContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            }  

            return emails;
        }

        public static async Task<string[]> GetManagerEmailAsync(string charId)
        {
            ValidateCharId(charId);

            string[] emails;

            using (var client = GenerateClient())
            {
                var response = await client.GetAsync($"manageremaillookup/{charId}");

                var responseContent = await response.Content.ReadAsStringAsync();
                
                emails = responseContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            }  

            return emails;
        }

        public static string[] GetEmailByName(string userName)
        {
            string[] emails;

            using (var client = GenerateClient())
            {
                var response = client.GetAsync($"nameemaillookup/{userName}").Result;

                var responseContent = response.Content.ReadAsStringAsync().Result;

                emails = responseContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            }

            return emails;
        }

        public static async Task<string[]> GetEmailByNameAsync(string userName)
        {
            string[] emails;

            using (var client = GenerateClient())
            {
                var response = await client.GetAsync($"nameemaillookup/{userName}");

                var responseContent = await response.Content.ReadAsStringAsync();

                emails = responseContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            }

            return emails;
        }

        private static void ValidateCharId(string charId)
        {
            if (charId.Length != 3)
            {
                throw new ArgumentException("charId should be 3 characters long");
            }
        }

        private static HttpClient GenerateClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(Configurations.EcoUtilsBase + "console/");

            return client;
        }

    }
}