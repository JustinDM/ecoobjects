using System;
using System.Data;
using System.Linq;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Address;
using Economical.EcoObjects.General.Address.Enums;

namespace Economical.EcoObjects.General.Helpers
{
    public static class DataParser
    {
        public static DataTable IngestRawTable(char delimiter, string rawTable, bool headerPresent = false)
        {
            var rawTableRows = rawTable.Split(Environment.NewLine.ToCharArray());

            rawTableRows = rawTableRows.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

            var table = new DataTable();

            if (headerPresent)
            {
                foreach (var i in rawTableRows[0].Split(delimiter))
                {
                    table.Columns.Add(i.Trim());
                }

                rawTableRows = rawTableRows.Skip(1).ToArray();
            }
            else
            {
                foreach (var i in rawTableRows[0].Split(delimiter))
                {
                    table.Columns.Add();
                }
            }

            foreach (var i in rawTableRows)
            {
                var rowItems = i.Split(delimiter);

                var row = table.NewRow();

                for (int j = 0; j < rowItems.Length; j++)
                {
                    row[j] = rowItems[j];
                }

                table.Rows.Add(row);
            }

            return table;
        }

        public static EcoAddress PullDetailedNoteAddress(string note)
        {
            note = note.Replace("\n", Environment.NewLine);

            var lastAddressIndex = note.LastIndexOf("address:");
            note = note.Substring(lastAddressIndex);

            // sometimes province is the last line, sometimes it isn't

            var lastEndlineIndex = note.LastIndexOf(Environment.NewLine) + 2;

            var lastLine = note.Substring(lastEndlineIndex);

            if (lastLine.Contains("province:"))
            {
                note += Environment.NewLine;
            }

            var addressLine1 = note.RetrieveFieldValue("addressLine1:", Environment.NewLine);

            var addressLine2 = note.RetrieveFieldValue("addressLine2:", Environment.NewLine);

            var city = note.RetrieveFieldValue("city:", Environment.NewLine);

            PostalCode postalCode = new PostalCode(note.RetrieveFieldValue("postalCode:", Environment.NewLine));

            var provinceText = note.RetrieveFieldValue("province:", Environment.NewLine);

            var province = Province.QC;

            if (!string.IsNullOrWhiteSpace(provinceText))
            {
                province = provinceText.ParseEnumFromDescription<Province>();
            }

            var address = new EcoAddress()
            {
                City = city,
                Province = province,
                PostalCode = postalCode
            };

            address.AddressLines.Add(addressLine1);
            address.AddressLines.Add(addressLine2);

            return address;
        }

        public static string RetrieveUberNameOwnership(string noteText)
        {
            var fieldName = "nameOwnership:";

            // seems the pull replaces \r\n with \\n, replacing with Environment.NewLine

            noteText = noteText.Replace("\\n", Environment.NewLine);

            var nameOwnership = noteText.RetrieveFieldValue(fieldName, Environment.NewLine);

            return nameOwnership;
        }
    }
}