using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Models;
using Economical.EcoObjects.LogRepo;
using Newtonsoft.Json;

namespace Economical.EcoObjects.General.Helpers.RepoCommunicator
{
    public class RepoCaller : HttpClient
    {
        private readonly AuthCommunicator _authComm;

        public RepoCaller(string endpoint)
        {
            BaseAddress = new Uri(endpoint);
        }

        public RepoCaller(AuthCommunicator authComm, string endpoint): this(endpoint)
        {
            _authComm = authComm;
        }

        private JsonSerializerSettings GenerateSettings()
        {
            var settings = new JsonSerializerSettings() 
            { 
                TypeNameHandling = TypeNameHandling.Objects, 
                SerializationBinder = new LogSerializationBinder(), 
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore 
            };

            return settings;
        }

        public async Task<Handler> UploadHandlerAsync(Handler handler)
        {
            var handlerParent = Converter.ConvertToHandlerParent(handler);

            var content = new StringContent(JsonConvert.SerializeObject(handlerParent, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }), Encoding.UTF8, "application/json");

            var response = await PostAsync("log/handler", content);
            var responseContent = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {

                var responseHandlerParent = JsonConvert.DeserializeObject<HandlerParent>(responseContent);
                
                var returnHandler = Converter.ConvertToHandler(responseHandlerParent);
                
                return returnHandler;
            }

            else
            {
                throw new HttpRequestException($"Call failed with code: '{response.StatusCode}' - '{responseContent}'");                
            }            
        }

        public Handler UploadHandler(Handler handler)
        {
            var handlerParent = Converter.ConvertToHandlerParent(handler);
            var content = new StringContent(JsonConvert.SerializeObject(handlerParent, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }), Encoding.UTF8, "application/json");

            var response = PostAsync("log/handler", content).Result;
            var responseContent = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {

                var responseHandlerParent = JsonConvert.DeserializeObject<HandlerParent>(responseContent);
                
                var returnHandler = Converter.ConvertToHandler(responseHandlerParent);
                
                return returnHandler;
            }

            else
            {
                throw new HttpRequestException($"Call failed with code: '{response.StatusCode}' - '{responseContent}'");                
            }     
        }

        // public async Task<Handler> UploadHandlerAsync(Handler handler)
        // {
        //     var settings = GenerateSettings();

        //     var content = new StringContent(JsonConvert.SerializeObject(handler, settings), Encoding.UTF8, "application/json");

        //     var response = await PostAsync("log/handler", content);
        //     var responseContent = await response.Content.ReadAsStringAsync();


        //     if (response.IsSuccessStatusCode)
        //     {

        //         var returnHandler = JsonConvert.DeserializeObject<Handler>(responseContent, settings);

        //         return returnHandler;
        //     }

        //     else
        //     {
        //         throw new HttpRequestException($"Call failed with code: '{response.StatusCode}' - '{responseContent}'");                
        //     }

        // }

        // public Handler UploadHandler(Handler handler)
        // {
        //     // I hate this but the call doesn't work with just awaits
        //     var settings = GenerateSettings();

        //     var content = new StringContent(JsonConvert.SerializeObject(handler, settings), Encoding.UTF8, "application/json");

        //     var response = PostAsync("log/handler", content).Result;
        //     var responseContent = response.Content.ReadAsStringAsync().Result;


        //     if (response.IsSuccessStatusCode)
        //     {

        //         var returnHandler = JsonConvert.DeserializeObject<Handler>(responseContent, settings);

        //         return returnHandler;
        //     }

        //     else
        //     {
        //         throw new HttpRequestException($"Call failed with code: '{response.StatusCode}' - '{responseContent}'");                
        //     }
        // }

        public async Task<Handler[]> GetHandlersAsync()
        {
            var settings = GenerateSettings();

            var response = await GetAsync("log/report");
            var responseContent = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var returnHandler = JsonConvert.DeserializeObject<Handler[]>(responseContent, settings);

                return returnHandler;
            }

            else
            {
                throw new HttpRequestException($"Call failed with code: '{response.StatusCode}' - '{responseContent}'");                
            }

        }

        public Handler[] GetHandlers()
        {
            var settings = GenerateSettings();

            var response = GetAsync("log/handler").Result;
            var responseContent = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {
                var responseHandlerParents = JsonConvert.DeserializeObject<HandlerParent[]>(responseContent, Configurations.LogRepoJsonSettings);

                var handlers = new List<Handler>();

                foreach (var i in responseHandlerParents)
                {
                    var handler = Converter.ConvertToHandler(i);
                    handlers.Add(handler);
                }

                return handlers.ToArray();
            }

            else
            {
                throw new HttpRequestException($"Call failed with code: '{response.StatusCode}' - '{responseContent}'");                
            }
        }

        // IF HANDLER IS NOT WIPED WHEN UPLOADING, EF WILL ATTEMPT TO UPLOAD HANDLER
        // DO WE HANDLE HERE OR IN THE API
        // I'M THINKING IN THE API - IF WE DO IT HERE WE HAVE A REFERENCE OF THE OBJECT AND WILL MODIFY THE ONE THAT'S RETURNED
        // ALSO WANT TO CHECK IF EF PICKS UP THE HANDLER BEFORE SENDING IT BACK

        public async Task<Unit> UploadUnitAsync(Unit unit)
        {
            var content = new StringContent(JsonConvert.SerializeObject(unit, GenerateSettings()), Encoding.UTF8, "application/json");

            var response = await PostAsync("log/unit", content);
            var responseContent = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var returnUnit = JsonConvert.DeserializeObject<Unit>(responseContent, Configurations.LogRepoJsonSettings);

                return returnUnit;
            }

            else
            {
                throw new HttpRequestException($"Call failed with code: '{response.StatusCode}' - '{responseContent}'");                
            }

        }

        public Unit UploadUnit(Unit unit)
        {
            // Configurations doesn't work properly?
            // var content = new StringContent(JsonConvert.SerializeObject(unit, Configurations.EcoLoggerJsonSettings), Encoding.UTF8, "application/json");
            var content = new StringContent(JsonConvert.SerializeObject(unit, GenerateSettings()), Encoding.UTF8, "application/json");

            var response = PostAsync("log/unit", content).Result;
            var responseContent = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {
                var returnUnit = JsonConvert.DeserializeObject<Unit>(responseContent, Configurations.LogRepoJsonSettings);

                return returnUnit;
            }

            else
            {
                throw new HttpRequestException($"Call failed with code: '{response.StatusCode}' - '{responseContent}'");                
            }
        }
    }
}