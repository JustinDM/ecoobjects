using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using AutoMapper;
using Economical.EcoObjects.General.DTOs;
using Economical.EcoObjects.General.Models;
using Economical.EcoObjects.General.DTOs.Update;
using System;
using System.Net;

namespace Economical.EcoObjects.General.Helpers.RepoCommunicator
{
    public abstract class RepoCommunicator : HttpClient
    {
        protected RepoConfig _config;
        protected readonly AuthCommunicator _authComm;
        protected IMapper _mapper;

        private RepoCommunicator(AuthCommunicator authComm, Profile profile)
        {
            _authComm = authComm;
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile(profile)));
        }

        public RepoCommunicator(RepoConfig config, AuthCommunicator authComm, Profile profile) : this(authComm, profile)
        {
            _config = config;
            BaseAddress = new Uri(_config.BaseAddress);
        }

        protected HttpResponseMessage Post(string callPath, StringContent content)
        {
            return PostAsync(callPath, content).Result;
        }

        protected HttpResponseMessage Put(string callPath, StringContent content)
        {
            return PutAsync(callPath, content).Result;
        }

        protected HttpResponseMessage Get(string callPath)
        {
            return GetAsync(callPath).Result;
        }

        

        
        protected async Task<AuthLoginResponse> LoginAsync()
        {
            var loginResponse = await _authComm.Login();

            DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginResponse.Token);

            return loginResponse;
        }

        protected AuthLoginResponse Login()
        {
            var loginResponse = _authComm.Login().Result;

            DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginResponse.Token);
            
            return loginResponse;
        }

        protected StringContent GenerateObjectContent(object obj)
        {
            var content = new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");

            return content;
        }

        protected T IngestResponse<T>(HttpResponseMessage response) where T: class
        {
            if (response.StatusCode == HttpStatusCode.NoContent)
            {
                return null;
            }

            else if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
            }

            else
            {
                throw new HttpRequestException($"request failed with response code '{response.StatusCode}'");
            }
        }

        public async Task<T> PostLogObjectAsync<T>(string callPath, object obj) where T: class
        {
            var loginReponse = await LoginAsync();

            var content = GenerateObjectContent(obj);

            var response = await PostAsync(callPath, content);

            var returnObject = IngestResponse<T>(response);

            return returnObject;
        }

        public T PostLogObject<T>(string callPath, object obj) where T: class
        {
            var loginReponse = Login();

            var content = GenerateObjectContent(obj);

            var response = Post(callPath, content);

            var returnObject = IngestResponse<T>(response);

            return returnObject;
        }

        public async Task<T> PutLogObjectAsync<T>(string callPath, object obj) where T: class
        {
            var loginReponse = await LoginAsync();

            var content = GenerateObjectContent(obj);

            var response = await PutAsync(callPath, content);

            var returnObject = IngestResponse<T>(response);

            return returnObject;
        }

        public T PutLogObject<T>(string callPath, object obj) where T: class
        {
            var loginResponse = Login();

            var content = GenerateObjectContent(obj);

            var response = Put(callPath, content);

            var returnObject = IngestResponse<T>(response);

            return returnObject;
        }

        public async Task<Handler> NewHandlerAsync()
        {
            return await PostLogObjectAsync<Handler>(_config.NewHandlerPath(), new object());
        }

        public Handler NewHandler()
        {
            return PostLogObject<Handler>(_config.NewHandlerPath(), new object());
        }

        public async Task UpdateHandlerAsync(Handler handler)
        {
            var handlerDto = _mapper.Map<UpdateHandlerDto>(handler);

            await PutLogObjectAsync<Handler>(_config.SpecificHandlerPath(handler.Id), handlerDto);
        }

        public void UpdateHandler(Handler handler)
        {
            var handlerDto = _mapper.Map<UpdateHandlerDto>(handler);

            PutLogObject<Handler>(_config.SpecificHandlerPath(handler.Id), handlerDto);
        }

        public async Task<Unit> NewUnitAsync(Guid handlerId)
        {
            var outboundDto = await PostLogObjectAsync<OutboundUnitDto>(_config.NewUnitPath(handlerId), new object());
            
            return _mapper.Map<Unit>(outboundDto);
        }

        public Unit NewUnit(Guid handlerId)
        {
            var outboundDto = PostLogObject<OutboundUnitDto>(_config.NewUnitPath(handlerId), new object());

            return _mapper.Map<Unit>(outboundDto);
        }

        public async Task UpdateUnitAsync(Unit log)
        {
            var transactDto = _mapper.Map<UpdateUnitDto>(log);

            await PutLogObjectAsync<OutboundUnitDto>(_config.SpecificUnitPath(log.Id), transactDto);
        }

        public void UpdateUnit(Unit log)
        {
            var transactDto = _mapper.Map<UpdateUnitDto>(log);

            PutLogObject<OutboundUnitDto>(_config.SpecificUnitPath(log.Id), transactDto);
        }

        public async Task<Trace> WriteTraceAsync(Trace trace, bool handlerTrace, Guid parentId)
        {
            var traceDto = _mapper.Map<InboundTraceDto>(trace);

            var endpoint = string.Empty;

            if (handlerTrace)
            {
                endpoint = _config.HandlerTracePath(parentId);
            }

            else
            {
                endpoint = _config.UnitTracePath(parentId);
            }

            var returnDto = await PostLogObjectAsync<OutboundTraceDto>(endpoint, traceDto);

            return _mapper.Map(returnDto, trace);
        }

        public Trace WriteTrace(Trace trace, bool handlerTrace, Guid parentId)
        {
            var traceDto = _mapper.Map<InboundTraceDto>(trace);

            var endpoint = string.Empty;

            if (handlerTrace)
            {
                endpoint = _config.HandlerTracePath(parentId);
            }
            else
            {
                endpoint = _config.UnitTracePath(parentId);
            }

            var returnDto = PostLogObject<OutboundTraceDto>(endpoint, traceDto);

            return _mapper.Map(returnDto, trace);
        }

        public abstract Task<UnitInfo> WriteUnitInfoAsync(UnitInfo info);
        public abstract UnitInfo WriteUnitInfo(UnitInfo info);
        public abstract Task<UnitInfo> UpdateUnitInfoAsync(UnitInfo info);
        public abstract UnitInfo UpdateUnitInfo(UnitInfo info);

        public abstract Task<Config> WriteConfigAsync(Config config);
        public abstract Config WriteConfig(Config config);
        public abstract Task<Config> UpdateConfigAsync(Config config);
        public abstract Config UpdateConfig(Config config);
    }
}