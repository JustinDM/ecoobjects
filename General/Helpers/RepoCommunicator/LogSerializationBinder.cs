using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;

namespace Economical.EcoObjects.General.Helpers.RepoCommunicator
{
    public class LogSerializationBinder : SerializationBinder, ISerializationBinder
    {
        private const string CoreLibAssembly = "System.Private.CoreLib";
        private const string MscorlibAssembly = "mscorlib";
        public override Type BindToType(string assemblyName, string typeName)
        {
            if (assemblyName == CoreLibAssembly)
            {
                assemblyName = MscorlibAssembly;
                typeName = typeName.Replace(CoreLibAssembly, MscorlibAssembly);
            }

            var resolvedTypeName = $"{typeName}, {assemblyName}";

    
            return Type.GetType(resolvedTypeName, true);
        }

        public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = null;
            typeName = serializedType.AssemblyQualifiedName;
        }

    }
}