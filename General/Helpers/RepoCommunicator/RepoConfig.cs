
using System;

namespace Economical.EcoObjects.General.Helpers
{
    public class RepoConfig
    {
        // endpoints have ids in them
        public string BaseAddress { get; set; }
        public string ControllerName { get; set; }
        public string HandlerEndpoint { get; set; } = "handler";
        public string UnitEndpoint { get; set; } = "Unit";
        public string UnitInfoEndpoint { get; set; } = "info";
        public string TraceEndpoint { get; set; } = "trace";
        public string ConfigEndpoint { get; set; } = "config";
        public string TimeoutEndpoint { get; set; } = "timeout";
        // etc... add more as you need em
        
        public RepoConfig(string controller)
        {
            ControllerName = controller;
        }

        public RepoConfig() { }

        public string NewHandlerPath()
        {
            return $"{ControllerName}/{HandlerEndpoint}";
        }

        public string SpecificHandlerPath(Guid handlerId)
        {
            return $"{NewHandlerPath()}/{handlerId}";
        }

        public string HandlerTracePath(Guid handlerId)
        {
            return $"{SpecificHandlerPath(handlerId)}/{TraceEndpoint}";
        }

        public string NewUnitPath(Guid handlerId)
        {
            return $"{SpecificHandlerPath(handlerId)}/{UnitEndpoint}";
        }

        public string SpecificUnitPath(Guid UnitId)
        {
            return $"{ControllerName}/{UnitEndpoint}/{UnitId}";
        }

        public string UnitInfoPath(Guid UnitId)
        {
            return $"{SpecificUnitPath(UnitId)}/{UnitInfoEndpoint}";
        }

        public string UnitTracePath(Guid UnitId)
        {
            return $"{SpecificUnitPath(UnitId)}/{TraceEndpoint}";
        }

        public string ConfigPath(Guid handlerId)
        {
            return $"{SpecificHandlerPath(handlerId)}/{ConfigEndpoint}";
        }

        public string TimeoutPath(Guid handlerId)
        {
            return $"{ConfigPath(handlerId)}/{TimeoutEndpoint}";
        }
    }
}