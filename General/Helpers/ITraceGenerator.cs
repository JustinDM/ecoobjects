using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.General.Helpers
{
    public interface ITraceGenerator
    {
        ObservableCollection<Trace> Traces { get; set; }
        Guid Id { get; set;}
    }
}