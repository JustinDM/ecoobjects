using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General.EcoUtils
{
    // CN=Justin Duff-Mailloux,OU=WAT1,OU=EIGUsers,DC=eig,DC=ecogrp,DC=ca"
    // CN=Justin Duff-Mailloux,OU=WAT1,DC=eig,DC=ecogrp,DC=ca,OU=EIGUsers"
    public class User : IEquatable<User>
    {
        public string UserName { get; set; }
        public List<string> OrganizationalUnits { get; set; } = new List<string>();
        public List<string> DomainControllers { get; set; } =  new List<string>();
        public string Email { get; set; }
        public User Manager { get; set; }
        public List<User> DirectReports { get; set; } = new List<User>();
        public string DistinguishedName { get { return GenerateDistinguishedName(); } }

        public User(string distinguishedName)
        {
            distinguishedName = distinguishedName.Replace("\"", "");
            var nameItems = distinguishedName.Split(',');

            OrganizationalUnits = new List<string>();
            DomainControllers = new List<string>();

            foreach (var i in nameItems)
            {
                switch (i.Substring(0, 2))
                {
                    case "CN":
                        UserName = i.Substring(3);
                        break;

                    case "OU":
                        OrganizationalUnits.Add(i.Substring(3));
                        break;

                    case "DC":
                        DomainControllers.Add(i.Substring(3));
                        break;
                    
                    default:
                        throw new ArgumentException($"unrecognized property type: '{i.Substring(0, 2)}'");
                }
            }

        }

        public User() { }

        public override string ToString()
        {
            return GenerateDistinguishedName();
        }

        private string GenerateDistinguishedName()
        {
            var dn = $"CN={UserName}";

            foreach (var i in OrganizationalUnits)
            {
                dn = dn + $",OU={i}";
            }

            foreach (var i in DomainControllers)
            {
                dn = dn + $",DC={i}";
            }

            return dn;
        }

        public bool Equals(User other)
        {
            return other.DistinguishedName == DistinguishedName;
        }
    }
}