using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Helpers;
using Newtonsoft.Json;

namespace Economical.EcoObjects.General.EcoUtils
{
    public class UserCaller
    {
        public static User[] GetUserByCharId(string charId)
        {
            User[] users;

            using(var client = GenerateUserClient())
            {
                var response = client.GetAsync($"charid/{charId}").Result;

                var responseContent = response.Content.ReadAsStringAsync().Result;

                users = JsonConvert.DeserializeObject<User[]>(responseContent);
            }    

            return users;
        }

        public static User[] GetUsersByName(string name)
        {
            User[] users;

            using(var client = GenerateUserClient())
            {
                var response = client.GetAsync($"name/{name}").Result;

                var responseContent = response.Content.ReadAsStringAsync().Result;

                users = JsonConvert.DeserializeObject<User[]>(responseContent);
            }

            return users;
        }

        public static User GetUserEmail(User user)
        {
            var userString = JsonConvert.SerializeObject(user);
            var stringContent = new StringContent(userString, Encoding.UTF8, "application/json");

            using (var client = GenerateUserClient())
            {
                var response = client.PostAsync("email", stringContent).Result;

                user = JsonConvert.DeserializeObject<User>(response.Content.ReadAsStringAsync().Result);
            }

            return user;
        }

        public static User GetUserManager(User user)
        {
            var userString = JsonConvert.SerializeObject(user);
            var stringContent = new StringContent(userString, Encoding.UTF8, "application/json");

            using (var client = GenerateUserClient())
            {
                var response = client.PostAsync("manager", stringContent).Result;

                user = JsonConvert.DeserializeObject<User>(response.Content.ReadAsStringAsync().Result);
            }

            return user;
        }

        private static HttpClient GenerateUserClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(Configurations.EcoUtilsBase + "user/");

            return client;
        }
    }
}