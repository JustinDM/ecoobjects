using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.EventArgs;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Interfaces;
using Economical.EcoObjects.General.Helpers.RepoCommunicator;

namespace Economical.EcoObjects.General.Models
{
    public class Handler : ITraceGenerator, INotifyPropertyUpdated
    {
        private Guid id;
        public Guid Id
        {
            get { return id; }
            set 
            { 
                id = value;
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("Id", value.ToString()));
                }
            }
        }

        private DateTime startDate;
        public DateTime StartDate
        {
            get { return startDate; }
            set 
            { 
                startDate = value;
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, 
                        new PropertyUpdateEventArgs(
                            "StartDate", 
                            value.ToString(Configurations.PrettyDateTimeFormat)
                        ));
                } 
            }
        }
        private DateTime endDate;
        public DateTime EndDate
        {
            get { return endDate; }
            set 
            { 
                endDate = value;
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this,
                        new PropertyUpdateEventArgs(
                            "EndDate",
                            value.ToString(Configurations.PrettyDateTimeFormat)
                        ));
                } 
            }
        }
        private RpaProcess process;
        public RpaProcess Process
        {
            get { return process; }
            set 
            { 
                process = value;
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("Process", value.ToString()));
                } 
            }
        }
        private CompletionStatus status;
        public CompletionStatus Status
        {
            get { return status; }
            set 
            { 
                status = value;
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("Status", value.ToString()));
                } 
            }
        }
        public ObservableCollection<Unit> Units { get; set; } = new ObservableCollection<Unit>();
        public ObservableCollection<Trace> Traces { get; set; } = new ObservableCollection<Trace>();

        // I'm going to not set up a fullprop for ConfigId
        public Guid? ConfigId { get; set; }
        
        private Config config;
        public Config Config
        {
            get { return config; }
            set 
            {
                // why block null?
                // if (value != null)
                // {
                //     config = value; 
                //     config.Handler = this;
                //     config.Id = Guid.NewGuid();
                // } 
                config = value; 
                if (value != null)
                {
                    config.Handler = this;
                    config.Id = Guid.NewGuid();
                }
            }
        }
        
        public event EventHandler<PropertyUpdateEventArgs> PropertyUpdated;
        public event EventHandler<PropertyInitializedEventArgs> PropertyInitialized;

        private RepoCommunicator _repoComm;

        // overall what do we want to update?
        //  - ID should be generated upon creation
        //  - start date should be set on creation
        //  - completion status should be set to incomplete
        //  - RPA process should come from constructor
        //  - when an item is added to Units, set up relationship properties
        //  - same thing for config

        public Handler()
        {
            
        }

        public Handler(RpaProcess process)
        {
            Process = process;
            Units.CollectionChanged += HandleCollectionChanged;
            Traces.CollectionChanged += HandleCollectionChanged;
            PropertyUpdated += HandlePropertyUpdated;
            PropertyInitialized += HandlePropertyInitialized;
            StartDate = DateTime.Now;
            Status = CompletionStatus.Incomplete;
            Id = Guid.NewGuid();
            EndDate = new DateTime();
        }

        private void HandleCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var addedItems = e.NewItems;

            foreach (var i in addedItems)
            {
                if (i is Unit Unit)
                {
                    Unit.PropertyUpdated += HandlePropertyUpdated;
                    Unit.PropertyInitialized += HandlePropertyInitialized;
                    Unit.Traces.CollectionChanged += HandleCollectionChanged;
                    Unit.Handler = this;
                    Unit.HandlerId = Id;
                    Unit.Process = Process;
                    Traces.Add(TraceExtensions.GenerateUnitAddedTrace(Unit.Id, this));
                }
            }
        }

        public Unit GenerateUnit()
        {
            var unit = new Unit();
            unit.Process = Process;
            return unit;
        }

        public DataTable GenerateReportTable()
        {
            var table = new DataTable();

            foreach (var log in Units)
            {
                var rowInfo = log.GenerateReportRow();

                if (table.Rows.Count == 0)
                {
                    foreach (var i in rowInfo)
                    {
                        if (!table.Columns.Contains(i.Key))
                        {
                            table.Columns.Add(i.Key);
                        }
                    }
                }

                var row = table.NewRow();

                foreach (var i in rowInfo)
                {
                    row[i.Key] = i.Value;
                }

                table.Rows.Add(row);
            }

            return table;
        }


        public DataTable GenerateOverviewTable()
        {
            // total processed
            // success
            // fail
            // fail business
            // fail application

            // maybe think about adding %s

            var totalProcessed = Units.Count;
            var successCount = Units.Where(x => x.Status == CompletionStatus.Complete).ToArray().Length;
            var failCount = Units.Where(x => x.Status == CompletionStatus.Failed).ToArray().Length;
            var businessFailCount = Units.Where(x => x.Status == CompletionStatus.Failed && x.FailureException is BusinessException).ToArray().Length;
            var applicationFailCount = failCount - businessFailCount;

            var overviewTable = new DataTable();

            overviewTable.Columns.Add("Item");
            overviewTable.Columns.Add("Count");

            var processedRow = overviewTable.NewRow();
            processedRow["Item"] = "Processed";
            processedRow["Count"] = totalProcessed;
            overviewTable.Rows.Add(processedRow);

            var successRow = overviewTable.NewRow();
            successRow["Item"] = "Success";
            successRow["Count"] = successCount;
            overviewTable.Rows.Add(successRow);
            
            var failRow = overviewTable.NewRow();
            failRow["Item"] = "Fail";
            failRow["Count"] = failCount;
            overviewTable.Rows.Add(failRow);
            
            var businessFailRow = overviewTable.NewRow();
            businessFailRow["Item"] = "Business failures";
            businessFailRow["Count"] = businessFailCount;
            overviewTable.Rows.Add(businessFailRow);
            
            var appFailRow = overviewTable.NewRow();
            appFailRow["Item"] = "Application failures";
            appFailRow["Count"] = applicationFailCount;
            overviewTable.Rows.Add(appFailRow);

            return overviewTable;
        }

        private void HandlePropertyUpdated(object sender, PropertyUpdateEventArgs e)
        {
            var trace = TraceExtensions.GeneratePropChangedTrace(
                $"{sender.GetType().Name}.{e.PropertyName}", 
                e.Value);

            if (sender is Unit Unit)
            {
                trace.Unit = Unit;
                Unit.Traces.Add(trace);
            }
            else if (sender is UnitInfo info)
            {
                trace.Unit = info.Unit;
                info.Unit.Traces.Add(trace);
            }
            else
            {
                trace.Handler = this;
                Traces.Add(trace);
            }
        }

        private void HandlePropertyInitialized(object sender, PropertyInitializedEventArgs e)
        {
            switch (e.Property)
            {
                case Config config:
                    var configTrace = TraceExtensions.GeneratePropertyInitializedTrace(e.PropertyName, config.Handler);
                    Traces.Add(configTrace);
                    break;

                case UnitInfo info:
                    var infoTrace = TraceExtensions.GeneratePropertyInitializedTrace(e.PropertyName, info.Unit);
                    info.Unit.Traces.Add(infoTrace);
                    break;

                default:
                    throw new ArgumentException($"Property type '{e.Property.GetType()}' is not valid");
            }
        }
        

        public DataTable GenerateFrenchReportTable()
        {
            var table = new DataTable();

            foreach (var log in Units)
            {
                var rowInfo = log.GenerateFrenchReportRow();

                if (table.Rows.Count == 0)
                {
                    foreach (var i in rowInfo)
                    {
                        if (!table.Columns.Contains(i.Key))
                        {
                            table.Columns.Add(i.Key);
                        }
                    }
                }

                var row = table.NewRow();

                foreach (var i in rowInfo)
                {
                    row[i.Key] = i.Value;
                }

                table.Rows.Add(row);
            }

            return table;
        }

        

        public DataTable GenerateFrenchOverviewTable()
        {
            throw new NotImplementedException();
        }

        public DataTable GenerateConfigTable()
        {
            throw new NotImplementedException();
        }

        public DataTable GenerateFrenchConfigTable()
        {
            throw new NotImplementedException();
        }

        public DataTable GenerateReadMeTable()
        {
            throw new NotImplementedException();
        }

        

    }
}