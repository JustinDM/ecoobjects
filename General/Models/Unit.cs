using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Linq;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.EventArgs;
using Economical.EcoObjects.General;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Interfaces;
using System.Globalization;

namespace Economical.EcoObjects.General.Models
{
    public class Unit : IRowGenerator, ITraceGenerator, INotifyPropertyUpdated, INotifyPropertyInitialized
    {
        private Guid id;
        public Guid Id
        {
            get { return id; }
            set 
            { 
                id = value;
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("Id", value.ToString()));
                } 
            }
        }
        
        private DateTime startDate;
        public DateTime StartDate
        {
            get { return startDate; }
            set 
            { 
                startDate = value; 
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("StartDate", value.ToString(Helpers.Configurations.PrettyDateFormat)));
                }
            }
        }

        private DateTime endDate;
        public DateTime EndDate
        {
            get { return endDate; }
            set 
            { 
                endDate = value; 
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("EndDate", value.ToString(Helpers.Configurations.PrettyDateFormat)));
                }
            }
        }
        
        private string botId;
        public string BotId
        {
            get { return botId; }
            set 
            { 
                botId = value; 
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("BotId", value));
                }
            }
        }
        private RpaProcess process;
        public RpaProcess Process
        {
            get { return process; }
            set 
            { 
                process = value; 
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("Process", value.ToString()));
                }
            }
        }
        private CompletionStatus status;
        public CompletionStatus Status
        {
            get { return status; }
            set 
            { 
                status = value;
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("Status", value.ToString()));
                } 
            }
        }
        private UnitInfo unitInfo;
        public UnitInfo UnitInfo
        {
            get { return unitInfo; }
            set 
            { 
                unitInfo = value;
                if (PropertyInitialized != null && value != null)
                {
                    UnitInfo.PropertyUpdated += PropertyUpdated;
                    value.Unit = this;
                    PropertyInitialized(this, new PropertyInitializedEventArgs("Unit.UnitInfo", value));
                } 
            }
        }

        public Guid? UnitInfoId { get; set; }

        private Exception failureException;
        public Exception FailureException
        {
            get { return failureException; }
            set 
            { 
                failureException = value; 
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("Unit.FailureException", value.GetType().Name));
                }

            }
        }

        private string statusMessage;
        public string StatusMessage
        {
            get { return statusMessage; }
            set { statusMessage = value; }
        }
        
        
        public ObservableCollection<Trace> Traces { get; set; } = new ObservableCollection<Trace>();
        
        // not setting up full properties for ef reflective relationship properties
        public Handler Handler { get; set; }
        public Guid HandlerId { get; set; }

        public event EventHandler<PropertyUpdateEventArgs> PropertyUpdated;
        public event EventHandler<PropertyInitializedEventArgs> PropertyInitialized;

        public Unit()
        {
            // when we create a new unit, the changes in the properties below are not logged
            // I will figure this out later
            StartDate = DateTime.Now;
            Status = CompletionStatus.Incomplete;
            BotId = Environment.UserName;
            Id = Guid.NewGuid();
        }

        public Dictionary<string, string> GenerateReportRow()
        {
            // retrieve input info dict
            var infoDictionary = UnitInfo.GenerateReportRow();

            infoDictionary.Add("StartDate", StartDate.ToString(Configurations.PrettyDateTimeFormat));
            infoDictionary.Add("EndDate", EndDate.ToString(Configurations.PrettyDateTimeFormat));
            infoDictionary.Add("BotId", BotId);

            // add attempt number
            infoDictionary.Add("AttemptNumber", UnitInfo.AttemptNumber.ToString());

            // append the completion message and status
            infoDictionary.Add("CompletionStatus", Status.GetDescription());

            if (FailureException != null)
            {
                infoDictionary.Add("FailureType", FailureException.GetType().AssemblyQualifiedName);
                infoDictionary.Add("ExceptionSource", FailureException is BusinessException ? "Business" : "Application");

                // want to add whether or not will be retried
                infoDictionary.Add("WillBeRetried?", FailureException is BusinessException || UnitInfo.AttemptNumber >= 3 ? "No" : "Yes");
            }
            else
            {
                infoDictionary.Add("FailureType", "");
                infoDictionary.Add("ExceptionSource", "");
                infoDictionary.Add("WillBeRetried?", "No");
            }

            infoDictionary.Add("CompletionMessage", StatusMessage);

            return infoDictionary;
        }

        public Dictionary<string, string> GenerateFrenchReportRow()
        {
            var infoDictionary = UnitInfo.GenerateFrenchReportRow();

            // add attempt number
            infoDictionary.Add("NombreDattentat", UnitInfo.AttemptNumber.ToString());

            // append the completion message and status
            infoDictionary.Add("ÉtatDachèvement", Status.GetDescription());

            // L'état d'achèvement
            // ÊTRE

            if (FailureException != null)
            {
                infoDictionary.Add("TypeDéchec", FailureException.GetType().AssemblyQualifiedName);
                infoDictionary.Add("SourceDéchec", FailureException is BusinessException ? "Business" : "Application");

                // want to add whether or not will be retried
                infoDictionary.Add("VaÊtreRéessayé?", FailureException is BusinessException || UnitInfo.AttemptNumber >= 3 ? "No" : "Yes");
            }
            else
            {
                infoDictionary.Add("TypeDéchec", "");
                infoDictionary.Add("SourceDéchec", "");
                infoDictionary.Add("VaÊtreRéessayé?", "No");
            }

            infoDictionary.Add("MessageDachèvement", StatusMessage);

            return infoDictionary;
        }

        public void IngestReportRow(DataRow row)
        {
            // but I don't know what type?
            // we are assuming the unit info is already created

            if (UnitInfo == null)
            {
                throw new ArgumentException("UnitInfo must be initialized");
            }

            UnitInfo.IngestReportRow(row);

            var exceptionString = "";
            
            if (row.Table.Columns.Contains("FailureType"))
            {
                exceptionString = row["FailureType"].ToString();
                if (exceptionString.Contains(" "))
                {
                    exceptionString = exceptionString.Substring(0, exceptionString.IndexOf(" ")).Trim();
                }
            }

            if (row.Table.Columns.Contains("BotId"))
            {
                BotId = row["BotId"].ToString();
            }

            if (!string.IsNullOrWhiteSpace(exceptionString))
            {
                // FailureException = Activator.CreateInstance(exceptionString, "") as Exception;
                var assembly = typeof(BusinessException).Assembly;
                var systemAssembly = typeof(Exception).Assembly;

                Type exceptionType;

                if (exceptionString.ToLower() == "exception")
                {
                    exceptionType = typeof(Exception);
                }
                else
                {
                    exceptionType = assembly.GetTypes().FirstOrDefault(x => exceptionString.Contains(x.AssemblyQualifiedName) || x.AssemblyQualifiedName.Contains(exceptionString));

                    // foreach (var i in assembly.GetTypes())
                    // {
                    //     Console.WriteLine(i.AssemblyQualifiedName);
                    // }
                    if (exceptionType == null)
                    {
                        exceptionType = systemAssembly.GetTypes().FirstOrDefault(x => exceptionString.Contains(x.AssemblyQualifiedName) || x.AssemblyQualifiedName.Contains(exceptionString));
                    }

                }


                if (exceptionType != null)
                {
                    FailureException = Activator.CreateInstance(exceptionType) as Exception;
                }
                else
                {
                    FailureException = new Exception();
                }

            }

            if (row.Table.Columns.Contains("StartDate"))
            {
                StartDate = DateTime.ParseExact(row["StartDate"].ToString(), Configurations.PrettyDateTimeFormat, CultureInfo.InvariantCulture);
            }

            if (row.Table.Columns.Contains("EndDate"))
            {
                EndDate = DateTime.ParseExact(row["EndDate"].ToString(), Configurations.PrettyDateTimeFormat, CultureInfo.InvariantCulture);
            }

            Status = row["CompletionStatus"].ToString().ParseEnumFromDescription<CompletionStatus>();

            try
            {
                StatusMessage = row["CompletionMessage"].ToString();
            }
            catch (Exception) { }
        }

    }
}