﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using Economical.EcoObjects.General.EventArgs;
using Economical.EcoObjects.General.Interfaces;

namespace Economical.EcoObjects.General.Models
{
    public abstract class Config : IConfigLoader, INotifyPropertyUpdated
    {
        private Guid id;
        public Guid Id
        {
            get { return id; }
            set 
            { 
                id = value; 
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("Id", value.ToString()));
                }
            }
        }

        public Handler Handler { get; set; }
        public Guid HandlerId { get; set; }

        public event EventHandler<PropertyUpdateEventArgs> PropertyUpdated;
        public event EventHandler<PropertyInitializedEventArgs> PropertyInitialized;

        public abstract void Duplicate(Config config);
        public abstract Config IngestConfigFile(string fileText);

        protected void CallUpdateEvent(object sender, PropertyUpdateEventArgs e)
        {
            if (PropertyUpdated != null)
            {
                PropertyUpdated(sender, e);
            }
        }

        protected void CallInitializeEvent(object sender, PropertyInitializedEventArgs e)
        {
            if (PropertyInitialized != null)
            {
                PropertyInitialized(sender, e);
            }
        }
    }
}