using System;
using System.Collections.Specialized;
using System.ComponentModel;
using Economical.EcoObjects.General.Enums;

namespace Economical.EcoObjects.General.Models
{
    public class QueueInfo
    {
        public Guid Id { get; set; }
        public OrchestratorQueue Queue { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime DeferDate { get; set; }
        public QueuePriority Priority { get; set; }
        public Unit Unit { get; set; }
        public Guid UnitId { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public event NotifyCollectionChangedEventHandler CollectionChanged;
    }
}