using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using Economical.EcoObjects.General.Attributes;
using Economical.EcoObjects.General.EventArgs;
using Economical.EcoObjects.General.Interfaces;

namespace Economical.EcoObjects.General.Models
{
    public abstract class UnitInfo : ICleanable, IRowGenerator, INotifyPropertyUpdated
    {
        private Guid id;
        [FrenchName("Id")]
        public Guid Id
        {
            get { return id; }
            set 
            { 
                id = value;
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("Id", value.ToString()));
                } 
            }
        }

        // why is this in the info?
        // make it flexible if we have retries?
        // if so we should have the logic for that
        // iretriable, check if unit info implements, if it does, call it
        private int attemptNumber;
        [FrenchName("NuméroDessai")]
        public int AttemptNumber
        {
            get { return attemptNumber; }
            set 
            { 
                attemptNumber = value; 
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("AttemptNumber", value.ToString()));
                }
            }
        }
        public Unit Unit { get; set; }
        public Guid UnitId { get; set; }

        public event EventHandler<PropertyUpdateEventArgs> PropertyUpdated;

        public abstract void CleanData();
        public abstract Dictionary<string, string> GenerateReportRow();
        public abstract Dictionary<string, string> GenerateFrenchReportRow();
        public abstract void IngestReportRow(DataRow row);

        protected void CallUpdateEvent(object sender, PropertyUpdateEventArgs e)
        {
            if (PropertyUpdated != null)
            {
                PropertyUpdated(sender, e);
            }
        }
    }
}