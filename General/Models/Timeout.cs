using System;
using System.Collections.Specialized;
using System.ComponentModel;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.EventArgs;

namespace Economical.EcoObjects.General.Models
{
    public class Timeout
    {
        private int _short;
        public int Short
        {
            get { return _short; }
            set 
            { 
                _short = value;
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("Short", value.ToString()));
                }
            }
        }

        private int medium;
        public int Medium
        {
            get { return medium; }
            set 
            { 
                medium = value; 
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("Medium", value.ToString()));
                }
            }
        }

        private int _long;
        public int Long
        {
            get { return _long; }
            set 
            { 
                _long = value;
                if (PropertyUpdated != null)
                {
                    PropertyUpdated(this, new PropertyUpdateEventArgs("Long", value.ToString()));
                }
            }
        }
        
        public event EventHandler<PropertyUpdateEventArgs> PropertyUpdated;

        public override string ToString()
        {
            return $"Short: '{Short}', Medium: '{Medium}', Long: '{Long}'";
        }
        public Timeout() { }
        public Timeout(string inputString)
        {
            var inputItems = inputString.Split(new string[] {","}, StringSplitOptions.RemoveEmptyEntries);

            var shortValue = inputItems[0].GetMatches(@"\d+")[0];
            Short = Convert.ToInt32(shortValue);
            
            var mediumValue = inputItems[1].GetMatches(@"\d+")[0];
            Medium = Convert.ToInt32(mediumValue);
            
            var longValue = inputItems[2].GetMatches(@"\d+")[0];
            Long = Convert.ToInt32(longValue);
        }

    }
}