using System;
using System.Collections.Specialized;
using System.ComponentModel;
using Economical.EcoObjects.General.Enums;

namespace Economical.EcoObjects.General.Models
{
    public class Trace
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }

        public Handler Handler { get; set; }
        public Nullable<Guid> HandlerId { get; set; }

        public Unit Unit { get; set; }
        public Nullable<Guid> UnitId { get; set; }

        // public event PropertyChangedEventHandler PropertyChanged;
        // public event NotifyCollectionChangedEventHandler CollectionChanged;

        public Trace() { }
        public Trace(string message)
        {
            Message = message;
            Date = DateTime.Now;
            Id = Guid.NewGuid();
        }
    }
}