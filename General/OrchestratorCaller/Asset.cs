using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General.OrchestratorCaller
{
    public class Asset
    {
        public string Name { get; set; }
        public bool CanBeDeleted { get; set; }
        public string ValueScope { get; set; }
        public string ValueType { get; set; }
        public string Value { get; set; }
        public string StringValue { get; set; }
        public bool BoolValue { get; set; }
        public int IntValue { get; set; }
        public string CredentialUsername { get; set; }
        public string CredentialPassword { get; set; }
        public string ExternalName { get; set; }
        public int CredentalStoreId { get; set; }
        public bool HasDefaultValue { get; set; }
        public string Description { get; set; }
        public int FoldersCount { get; set; }
        public int Id { get; set; }
        public string[] KeyValueList { get; set; }
    }
}