using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Economical.EcoObjects.General.OrchestratorCaller
{
    public class AssetListResponse
    {
        [JsonProperty("@odata.context")]
        public string OdataContext { get; set; }
        [JsonProperty("@odata.count")]
        public int OdataCount { get; set; }
        public Asset[] Value { get; set; }
    }
}