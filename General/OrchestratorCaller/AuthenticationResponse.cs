using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General.OrchestratorCaller
{
    public class AuthenticationResponse
    {
        public string Result { get; set; }
        public string TargetUrl { get; set; }
        public bool Success { get; set; }
        public string Error { get; set; }
        public bool UnauthorizedRequest { get; set; }
        public bool __apb { get; set; }
    }
}