using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General.OrchestratorCaller
{
    public class OrchestratorLogin
    {
        public string TenancyName { get; set; }
        public string UsernameOrEmailAddress { get; set; }
        public string Password { get; set; }
        
        public OrchestratorLogin(string usernameOrEmailAddress, string password, string tenancyName = "default")
        {
            UsernameOrEmailAddress = usernameOrEmailAddress;
            Password = password;
            TenancyName = tenancyName;
        }
    }
}