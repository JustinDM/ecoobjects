using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Economical.EcoObjects.General.OrchestratorCaller
{
    public class OrchestratorCaller
    {
        private HttpClient _client;
        public OrchestratorCaller(string orchAddress)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(orchAddress);
        }

        public async Task<HttpResponseMessage> AuthenticateAsync(OrchestratorLogin login)
        {
            var endpoint = @"/api/Account/Authenticate";

            var postContent = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(endpoint, postContent);

            if (!response.IsSuccessStatusCode)
            {
                throw new ArgumentException($"Authentication failed with code: '{response.StatusCode}'");
            }

            var responseContent = await response.Content.ReadAsStringAsync();

            var authenticationResponse = JsonConvert.DeserializeObject<AuthenticationResponse>(responseContent);

            // need ot add the bearer token to our client headers

            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", authenticationResponse.Result);

            return response;
        }

        public HttpResponseMessage Authenticate(OrchestratorLogin login)
        {
            return AuthenticateAsync(login).Result;
        }

        public async Task<List<Asset>> GetAssetsAsync()
        {
            var endpoint = @"/odata/Assets";

            var response = await _client.GetAsync(endpoint);

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Failed to retrieve assets: '{response.StatusCode}'");
            }

            var responseContent = await response.Content.ReadAsStringAsync();

            var assetList = JsonConvert.DeserializeObject<AssetListResponse>(responseContent);

            return assetList.Value.ToList();
        }

        public List<Asset> GetAssets()
        {
            return GetAssetsAsync().Result;
        }
    }
}