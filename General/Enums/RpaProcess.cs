using System.ComponentModel;

namespace Economical.EcoObjects.General.Enums
{
    public enum RpaProcess
    {
        Invalid,
        [Description("Contractor Connection")]
        ContractorConnection,
        CNLR,
        [Description("Alberta Compliance Letters")]
        AlbertaComplianceLetters,
        [Description("Nova Scotia Opening letters")]
        NovaScotiaOpening,
        [Description("Nova Scotia Closing letters")]
        NovaScotiaClosing,
        [Description("Quebec closing letters")]
        QuebecClosingLetter,
        [Description("ICC Flat Fees")]
        IccFlatFees,
        [Description("ICC Loss Payments")]
        IccLossPayments,
        [Description("Edjuster Flat Fees")]
        EdjusterFlatFees,
        [Description("Express Flat Fees")]
        ExpressFlatFees,
        [Description("CNLR Labelling and Keying")]
        CnlrLnk,
        [Description("Auto CERF payments")]
        AutoCerfPayments

    }
}
