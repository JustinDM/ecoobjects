namespace Economical.EcoObjects.General.Enums
{
    public enum EconomicalSystem
    {
        Invalid,
        CDS,
        Family,
        Windows,
        Outlook,
        Excel,
        SAS,
        EcoObjects,
        PDF,
        EcoOutlook,
        Orchestrator,
        OWC,
        Word,
        InternetExplorer,
        Edge,
        InsurCloud,
        CNLR,
        Audatex
    }
}