namespace Economical.EcoObjects.General.Enums
{
    public enum LogPolarity
    {
        Invalid,
        Positive,
        Neutral,
        Negative
    }
}
