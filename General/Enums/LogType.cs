namespace Economical.EcoObjects.General.Enums
{
    public enum LogType
    {
        Invalid,
        Info,
        Decision,
        Status,
        Error
    }
}
