using System.ComponentModel;

namespace Economical.EcoObjects.General.Enums
{
    public enum CompletionStatus
    {
        Invalid,
        Complete,
        Incomplete,
        Failed,
        [Description("Not complete")]
        NotComplete,
        [Description("Not started")]
        NotStarted
    }
}
