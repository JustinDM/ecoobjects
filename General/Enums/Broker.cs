namespace Economical.EcoObjects.General
{
    public enum Broker
    {
        Invalid,
        Aviva,
        Intact,
        Cooperators
    }
}
