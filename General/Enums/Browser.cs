namespace Economical.EcoObjects.General.Enums
{
    public enum Browser
    {
        Invalid,
        IE,
        Chrome,
        Edge
    }
}