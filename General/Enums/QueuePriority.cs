namespace Economical.EcoObjects.General.Enums
{
    public enum QueuePriority
    {
        Invalid,
        High,
        Normal,
        Low
    }
}
