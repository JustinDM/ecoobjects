using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Attributes;

namespace Economical.EcoObjects.General.Extensions
{
    
    public static class TranslationExtensions
    {
        public static String ClassToFrenchString<T>(this T input) where T: class
        {
            var properties = typeof(T).GetProperties();
            
            var classAttributes = Attribute.GetCustomAttribute(typeof(T), typeof(FrenchNameAttribute));

            var classString = $"{((FrenchNameAttribute)classAttributes).FrenchName}:";

            foreach (var i in properties)
            {
                var frenchAttr = Attribute.GetCustomAttribute(i, typeof(FrenchNameAttribute)) as FrenchNameAttribute;
                classString += $" {frenchAttr.FrenchName}: '{i.GetValue(input)}';";
            }

            return classString;
        }
    }
}