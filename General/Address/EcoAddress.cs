using System;
using System.Collections.Generic;
using System.Linq;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Address.Enums;
using Economical.EcoObjects.General.Helpers;

namespace Economical.EcoObjects.General.Address
{
    public class EcoAddress
    {
        public List<string> AddressLines { get; set; } = new List<string>();
        public string City { get; set; }
        public Province Province { get; set; }
        public PostalCode PostalCode { get; set; }

        public bool IsValid { get { return Province != Province.Invalid && PostalCode.IsValid; } }

        public EcoAddress() { }

        public EcoAddress(string addressLine, bool attemptPartial = false)
        {
            try
            {
                IngestAddress(addressLine);
            }
            catch (Exception)
            {
                if (attemptPartial)
                {
                    IngestPartialAddress(addressLine);
                }
                else
                {
                    throw;
                }
            }

            
        }

        public bool Equals(EcoAddress address, double addressLineThreshold)
        {
            if (addressLineThreshold < 0 || addressLineThreshold > 1)
            {
                throw new ArgumentException("addressLineThreshold should have a value between 0 and 1, inclusive");
            }

            if
            (
                City.SimilarTo(address.City)
                && Province == address.Province
                && PostalCode.Equals(address.PostalCode)
                && MatchAddressLines(address, addressLineThreshold)
            )
                return true;

            return false;
        }

        private bool MatchAddressLines(EcoAddress address, double threshold)
        {
            var addressComponents = BreakDownAddressLines(AddressLines);
            var checkAddressComponents = BreakDownAddressLines(address.AddressLines);

            var matchCount = 0;

            foreach (var item in addressComponents)
            {
                var checkItem = checkAddressComponents.FirstOrDefault(x => x.SimilarTo(item));

                if (checkItem != null)
                {
                    matchCount++;
                }
            }

            if ((double)matchCount / addressComponents.Length >= threshold)
            {
                return true;
            }

            return false;
        }

        private string[] BreakDownAddressLines(List<string> addressLines)
        {
            var addressComponents = new List<string>();

            foreach (var line in addressLines)
            {
                var lineItems = line.Split(' ');

                foreach (var item in lineItems)
                {
                    if (item.Length > 0)
                    {
                        addressComponents.Add(item);
                    }
                }
            }

            return addressComponents.ToArray();
        }

        public void IngestAddress(string addressLine)
        {
            // 1215 Route 177,  Wellington, PE, C0B2E0
            var addressItems = addressLine.Split(new string[] {", "}, StringSplitOptions.None);

            PostalCode = new PostalCode(addressItems[addressItems.Length - 1]);
            Province = addressItems[addressItems.Length - 2].ParseEnumFromDescription<Province>();
            City = addressItems[addressItems.Length - 3];
            AddressLines = addressItems.Take(addressItems.Length - 3).ToList();
        }

        public override string ToString()
        {
            return $"{string.Join(", ", AddressLines).Trim()}, {City}, {Province}, {PostalCode}";
        }

        private void IngestPartialAddress(string addressLine)
        {
            // try and pull out a postal code first:
            var postalCodeMatches = addressLine.GetMatches(@"[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d");

            if (postalCodeMatches.Length > 0)
            {
                var postalCodeText = postalCodeMatches[0].ToString();
                PostalCode = new PostalCode(postalCodeText);
                addressLine = addressLine.Replace(postalCodeText, "").Trim();
            }

            var addressItems = addressLine.Split(new string[] {","}, StringSplitOptions.RemoveEmptyEntries);

            //after that we should have city. It's possible it's part of the address line, however:
            // 923 Glasgow street kitchener, ON N2N 0B5

            if (addressItems.Length > 2)
            {
                try
                {
                    Province = addressItems.Last().ParseEnumFromDescription<Province>();
                    // this would be for an address like this:
                    // 923 glasgow st, kitchener, ON
                    // go two steps back to skip over the province
                    City = addressItems[addressItems.Length - 2].ToString().Trim();
                    var addressLineItems = addressItems.Take(addressItems.Length - 2);
                    foreach (var i in addressLineItems)
                    {
                        AddressLines.Add(i);
                    }
                }
                catch (System.Exception) { }
                
            }


            else if (addressItems.Length == 2)
            {
                try
                {
                    Province = addressItems.Last().ParseEnumFromDescription<Province>();
                    // this would be for an address like this:
                    // 923 Glasgow St Kitchener, ON
                    var cityAddressLine = addressItems[addressItems.Length - 2];
                    var cityAddressLineItems = cityAddressLine.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    City = cityAddressLineItems.Last();

                    AddressLines.Add(string.Join(" ", cityAddressLineItems.Take((cityAddressLineItems.Length - 1))).Trim());
                }
                catch (System.Exception) { }
                
            }

            else if (addressItems.Length == 1)
            {
                try
                {
                    // this would be for an address like this:
                    // ##321-323 LESMILL ROAD  DON MILLS  ON,
                    // length = 6
                    var items = addressItems[0].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    var provinceLine = items.Last();
                    Province = provinceLine.Trim().ParseEnumFromDescription<Province>();
                    City =  items.Skip(items.Length - 2).ToArray()[0];
                    AddressLines.Add(string.Join(" ", items.Take((items.Length - 2))).Trim());
                }
                catch (System.Exception) { }
            }

        }
    }
}