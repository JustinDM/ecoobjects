using System;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Address.Enums;
using Economical.EcoObjects.General.Helpers;

namespace Economical.EcoObjects.General.Address
{
    public class PostalCode
    {
        public string Code { get; set; }
        public bool IsValid { get; set; }
        public PostalCode() { }
        public PostalCode(string inputCode)
        {
            IsValid = false;
            Code = inputCode.Replace("-", "").Replace(" ", "").Trim().ToUpper();
            IsValid = ValidateInput(Code);
        }
        private bool ValidateInput(string input)
        {
            return input.IsMatch(@"[A-Z][0-9][A-Z](\s)?[0-9][A-Z][0-9]");
        }

        public bool Equals(PostalCode postalCode)
        {
            if (postalCode.Code.ToLower().Replace(" ", "") == Code.ToLower().Replace(" ", ""))
            {
                return true;
            }

            return false;
        }

        public override string ToString()
        {
            return Code;
        }

        public Province DetermineProvince()
        {
            var firstChar = Code.Substring(0, 1).ToLower();
            var thirdChar = Code.Substring(2, 1).ToLower();

            Province returnProvince;

            switch (firstChar)
            {
                case "a":
                    returnProvince = Province.NL;
                    break;

                case "b":
                    returnProvince = Province.NS;
                    break;

                case "c":
                    returnProvince = Province.PE;
                    break;

                case "e":
                    returnProvince = Province.NB;
                    break;

                case "g":
                case "h":
                case "j":
                    returnProvince = Province.QC;
                    break;

                case "k":
                case "l":
                case "m":
                case "n":
                case "p":
                    returnProvince = Province.ON;
                    break;

                case "r":
                    returnProvince = Province.MB;
                    break;

                case "s":
                    returnProvince = Province.SK;
                    break;

                case "t":
                    returnProvince = Province.AB;
                    break;

                case "v":
                    returnProvince = Province.BC;
                    break;

                case "x":
                    if (thirdChar == "a" || thirdChar == "b" || thirdChar == "c")
                    {
                        returnProvince = Province.NU;
                    }
                    else
                    {
                        returnProvince = Province.NT;
                    }
                    break;

                default:
                    throw new ArgumentException("Invalid postal code");
            }

            return returnProvince;
        }
    }
}