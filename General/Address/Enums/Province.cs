using System.ComponentModel;

namespace Economical.EcoObjects.General.Address.Enums
{
    public enum Province
    {
        Invalid,
        [Description("Alberta")]
        AB,
        [Description("British Columbia")]
        BC,
        [Description("Manitoba")]
        MB,
        [Description("New Brunswick")]
        NB,
        [Description("Newfoundland and Labrador")]
        NL,
        [Description("Nova Scotia")]
        NS,
        [Description("Ontario")]
        ON,
        [Description("Prince Edward Island")]
        PE,
        [Description("Quebec")]
        QC,
        [Description("Saskatchewan")]
        SK,
        [Description("Northwest Territories")]
        NT,
        [Description("Yukon")]
        YT,
        [Description("Nunavut")]
        NU
    }
}
