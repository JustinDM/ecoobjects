using System.ComponentModel;

namespace Economical.EcoObjects.General.OWC
{
    public enum SecurityGroup
    {
        [Description("Invalid")]
        Invalid,
        [Description("No Selection")]
        NoSelection,
        [Description("ABSTAFF_DOCS")]
        AbStaffDocs,
        [Description("PDSTAFF_DOCS")]
        PdStaffDocs,
        [Description("PD_DOCS")]
        PdDocs
    }
}
