using System.ComponentModel;

namespace Economical.EcoObjects.General.OWC
{
    public enum CheckInType
    {
        [Description("Invalid")]
        Invalid,
        [Description("AllClaims")]
        AllClaims,
        [Description("Application - Files owned by Content Server applications")]
        Application,
        [Description("Auto-AccidentBenefits")]
        AutoAccidentBenefits,
        [Description("Auto-BodilyInjury")]
        AutoBodilyInjury,
        [Description("Auto-PhysicalDamage")]
        AutoPhysicalDamage,
        [Description("Prop-BodilyInjury")]
        PropBodilyInjury,
        [Description("Prop-Cas-PD")]
        PropCasPD,
        [Description("Prop-PhysicalDamage")]
        PropPhysicalDamage,
        [Description("TBD")]
        TBD
    }
}
