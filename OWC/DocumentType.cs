using System.ComponentModel;

namespace Economical.EcoObjects.General.OWC
{
    public enum DocumentType
    {
        [Description("Invalid")]
        Invalid,
        [Description("No Selection")]
        NoSelection,
        [Description("Correspondence")]
        Correspondence,
        [Description("Corporate Information")]
        CorporateInformation,
        [Description("Internal documents")]
        InternalDocuments
    }
}
