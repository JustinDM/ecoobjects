using System.ComponentModel;

namespace Economical.EcoObjects.General.OWC
{
    public enum AdjusterRequired
    {
        [Description("Invalid")]
        Invalid,
        [Description("No")]
        No,
        [Description("Reviewed")]
        Reviewed,
        [Description("Yes")]
        Yes,
        [Description("Yes - In Progress")]
        YesInProgress
    }
}
