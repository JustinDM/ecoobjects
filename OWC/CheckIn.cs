using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economical.EcoObjects.General.OWC
{
    public class CheckIn
    {
        public CheckInType Type { get; set; }
        public DocumentType DocType { get; set; }
        public SecurityGroup SecurityGroup { get; set; }
        public string Title { get; set; }
        public string FilePath { get; set; }
        public string ClaimNumber { get; set; }
        public string Adjuster { get; set; }
        public DateTime DateOfLoss { get; set; }
        public AdjusterRequired AdjusterRequired { get; set; }
        public bool PerformCdsLookup { get; set; }
    }
}
