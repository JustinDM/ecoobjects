using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.QuebecClosing
{
    public class QcUnitGenerator
    {
        public static ObservableCollection<Unit> GenerateUnits(DataTable table, bool fromCds = true)
        {
            var logs = new ObservableCollection<Unit>();

            foreach (DataRow i in table.Rows)
            {
                Unit log;

                if (fromCds)
                {
                    log = GenerateUnitFromCds(i);
                }
                else
                {
                    log = GenerateUnitFromReport(i);
                }

                logs.Add(log);
            }

            return logs;
        }

        public static Unit GenerateUnitFromCds(DataRow row)
        {
            var log = new Unit
            {
                UnitInfo = new QcUnitInfo()
                {
                    ClaimNumber = Convert.ToDouble(row["CLAIMNUMBER"].ToString()),
                },
                Status = CompletionStatus.Incomplete,
                BotId = Environment.MachineName,
                Process = RpaProcess.QuebecClosingLetter
            };

            return log;
        }

        public static Unit GenerateUnitFromReport(DataRow row)
        {
            var log = new Unit
            {
                UnitInfo = new QcUnitInfo()
                {
                    ClaimNumber = Convert.ToDouble(row["ClaimNumber"].ToString()),
                    Company = row["Company"].ToString(),
                    LetterDate = DateTime.ParseExact(row["LetterDate"].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture),
                    DateOfLoss = DateTime.ParseExact(row["DateOfLoss"].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture),
                    Policy = row["Policy"].ToString(),
                    Adjuster = row["Adjuster"].ToString(),
                    FaultPercentage = Convert.ToInt32(row["FaultPercentage"].ToString()),
                    LossPaid = Convert.ToDouble(row["LossPaid"].ToString()),
                    CustomerEmail = row["CustomerEmail"].ToString(),
                    DriverEmail = row["DriverEmail"].ToString(),
                    ApprovedForMailing = Convert.ToBoolean(row["ApprovedForMailing"]),
                    IsUberClaim = Convert.ToBoolean(row["IsUberClaim"].ToString()),
                    LetterType = (LetterType) Enum.Parse(typeof(LetterType), row["LetterType"].ToString()),
                    IsCommercialOffline = Convert.ToBoolean(row["IsCommercialOffline"].ToString()),
                    AttemptNumber = Convert.ToInt32(row["AttemptNumber"].ToString())
                },
                Status = (CompletionStatus) Enum.Parse(typeof(CompletionStatus), row["CompletionStatus"].ToString()),

            };

            return log;
        }
    }
}