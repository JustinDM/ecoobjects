using System;
using System.ComponentModel;
using System.Text.Json;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.QuebecClosing
{
    public class QcConfig : Config
    {
    public int DelayBefore { get; set; }        
        public int DelayAfter { get; set; }
        public string[] ReviewerEmails { get; set; }
        public string LogFolder { get; set; }
        public string NoteText { get; set; }
        public Timeout Timeout { get; set; }
        public Guid? TimeoutId { get; set; }
        public string ResourceFolder { get; set; }
        public string FallbackEmail { get; set; }


        public QcConfig() { }
        
        public QcConfig(string fileText)
        {
            var config = IngestConfigFile(fileText);

            Duplicate(config);
        }

        public override void Duplicate(Config config)
        {
            var qcConfig = config as QcConfig;
            
            DelayBefore = qcConfig.DelayBefore;
            DelayAfter = qcConfig.DelayAfter;
            ReviewerEmails = qcConfig.ReviewerEmails;
            LogFolder = qcConfig.LogFolder;
            NoteText = qcConfig.NoteText;
            Timeout = qcConfig.Timeout;
            ResourceFolder = qcConfig.ResourceFolder;
            FallbackEmail = qcConfig.FallbackEmail;
        }

        public override Config IngestConfigFile(string fileText)
        {
            var config = JsonSerializer.Deserialize<QcConfig>(fileText);

            return config;
        }

    }
}