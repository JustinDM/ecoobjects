using System;
using System.Collections.Generic;
using System.Data;
using Economical.EcoExtensions;
using Economical.EcoObjects.CDS.Enums;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.QuebecClosing
{
    public class QcUnitInfo : UnitInfo
    {
        public ClaimStatus ClaimStatus { get; set; }
        public int FaultPercentage { get; set; }
        public double LossPaid { get; set; }
        public double ClaimNumber { get; set; }
        public string Company { get; set; }
        public DateTime LetterDate { get; set; }
        public DateTime DateOfLoss { get; set; }
        public string Policy { get; set; }
        public string Adjuster { get; set; }
        public string CustomerEmail { get; set; }
        public string DriverEmail { get; set; }
        public bool IsUberClaim { get; set; }
        public LetterType LetterType { get; set; }
        public bool IsCommercialOffline { get; set; }
        public bool ApprovedForMailing { get; set; }

        public override void CleanData()
        {
            // should be voided but need it to be returned to us
            //CustomerEmail = "REDACTED";
        }

        public override Dictionary<string, string> GenerateFrenchReportRow()
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, string> GenerateReportRow()
        {
            var reportInfo = new Dictionary<string, string>()
            {
                { "ClaimNumber", ClaimNumber.ToString() },
                { "Company", Company },
                { "LetterDate", LetterDate.ToString(Configurations.ReportDateFormat) },
                { "DateOfLoss", DateOfLoss.ToString(Configurations.ReportDateFormat) },
                { "Policy", Policy },
                { "Adjuster", Adjuster },
                { "FaultPercentage", FaultPercentage.ToString() },
                { "LossPaid", LossPaid.ToString() },
                { "CustomerEmail", CustomerEmail },
                { "DriverEmail", DriverEmail },
                { "ApprovedForMailing", ApprovedForMailing.ToString() },
                { "IsUberClaim", IsUberClaim.ToString() },
                { "LetterType", LetterType.ToString() },
                { "IsCommercialOffline", IsCommercialOffline.ToString() },
            };

            return reportInfo;
        }

        public override void IngestReportRow(DataRow row)
        {
            throw new NotImplementedException();
        }
    }
}