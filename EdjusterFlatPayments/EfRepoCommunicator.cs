using System.Threading.Tasks;
using AutoMapper;
using Economical.EcoObjects.EdjusterFlatPayments.DTOs;
using Economical.EcoObjects.General.DTOs;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;
using Economical.EcoObjects.General.Helpers.RepoCommunicator;

namespace Economical.EcoObjects.EdjusterFlatPayments
{
    public class EfRepoCommunicator : RepoCommunicator
    {
        public EfRepoCommunicator(RepoConfig config, AuthCommunicator authComm, Profile profile)
            : base (config, authComm, profile) { }

        public override Config UpdateConfig(Config config)
        {
            throw new System.NotImplementedException();
        }

        public override async Task<Config> UpdateConfigAsync(Config config)
        {
            var ifConfig = config as EfConfig;

            var loginResponse = await LoginAsync();

            var configDto = _mapper.Map<EfInboundConfigDto>(ifConfig);

            var content = GenerateObjectContent(configDto);

            var response = await PostAsync(_config.ConfigPath(ifConfig.Handler.Id), content);

            var outboundDto = IngestResponse<EfOutboundConfigDto>(response);

            _mapper.Map(outboundDto, ifConfig);

            return ifConfig;
        }

        public override UnitInfo UpdateUnitInfo(UnitInfo info)
        {
            throw new System.NotImplementedException();
        }

        public override async Task<UnitInfo> UpdateUnitInfoAsync(UnitInfo info)
        {
            var ifInfo = info as EfUnitInfo;

            var loginResponse = await LoginAsync();

            var infoDto = _mapper.Map<EfInboundUnitInfoDto>(ifInfo);

            var content = GenerateObjectContent(infoDto);

            var response = await PutAsync(_config.UnitInfoPath(ifInfo.Unit.Id), content);

            var transact = IngestResponse<OutboundUnitDto>(response);

            _mapper.Map(transact.UnitInfo as EfUnitInfo, ifInfo);

            return transact.UnitInfo;
        }

        public override Config WriteConfig(Config config)
        {
            throw new System.NotImplementedException();
        }

        public override async Task<Config> WriteConfigAsync(Config config)
        {
            var ifConfig = config as EfConfig;

            var loginResponse = await LoginAsync();

            var configDto = _mapper.Map<EfInboundConfigDto>(ifConfig);

            var content = GenerateObjectContent(configDto);

            var response = await PostAsync(_config.ConfigPath(ifConfig.Handler.Id), content);

            var outboundDto = IngestResponse<EfOutboundConfigDto>(response);

            _mapper.Map(outboundDto, ifConfig);

            return ifConfig;
        }

        public override UnitInfo WriteUnitInfo(UnitInfo info)
        {
            throw new System.NotImplementedException();
        }

        public override async Task<UnitInfo> WriteUnitInfoAsync(UnitInfo info)
        {
            var ifInfo = info as EfUnitInfo;

            var loginResponse = await LoginAsync();

            var infoDto = _mapper.Map<EfInboundUnitInfoDto>(ifInfo);

            var content = GenerateObjectContent(infoDto);

            var response = await PostAsync(_config.UnitInfoPath(ifInfo.Unit.Id), content);

            var transact = IngestResponse<OutboundUnitDto>(response);

            _mapper.Map(transact.UnitInfo as EfUnitInfo, ifInfo);

            return transact.UnitInfo;
        }
    }
}