using Economical.EcoObjects.EdjusterFlatPayments.DTOs;
using Economical.EcoObjects.General.Helpers;

namespace Economical.EcoObjects.EdjusterFlatPayments
{
    public class EfMapperProfiles : MapperProfiles
    {
        public EfMapperProfiles()
        {
            CreateMap<EfConfig, EfInboundConfigDto>();

            CreateMap<EfUnitInfo, EfInboundUnitInfoDto>();

            CreateMap<EfInboundConfigDto, EfConfig>();

            CreateMap<EfInboundUnitInfoDto, EfUnitInfo>();

            CreateMap<EfUpdateUnitInfoDto, EfUnitInfo>();

            CreateMap<EfUnitInfo, EfOutboundUnitInfoDto>();

            CreateMap<EfConfig, EfOutboundConfigDto>();
        }
    }
}