using Economical.EcoObjects.General.Address;
using Economical.EcoObjects.General.DTOs;

namespace Economical.EcoObjects.EdjusterFlatPayments.DTOs
{
    public class EfInboundUnitInfoDto : InboundUnitInfoDto
    {
        public double ClaimNumber { get; set; }
        public string ClaimModifier { get; set; }
        public string Insured { get; set; }
        public EcoAddress Address { get; set; }
        public double Amount { get; set; }
        public string ClaimId { get; set; }
        public string Invoice { get; set; }
    }
}