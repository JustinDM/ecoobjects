using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Address;
using Economical.EcoObjects.General.Address.Enums;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.EdjusterFlatPayments
{
    public class EfUnitInfo : UnitInfo
    {
        public double ClaimNumber { get; set; }
        public string ClaimModifier { get; set; }
        public string Insured { get; set; }
        public EcoAddress Address { get; set; }
        public double Amount { get; set; }
        public string ClaimId { get; set; }
        public string Invoice { get; set; }
        
        // I know this is a date but taking in a date and redoing the format might confuse business, lead to complications of ingestion, etc.
        public string ReportStart { get; set; }
        public string ReportEnd { get; set; }

        public EfUnitInfo() { }

        public EfUnitInfo(DataRow row, string invoice)
        {
            // claim number can have '-' in it
            var claimString = row["Claim No"].ToString();

            var hyphenPresent = claimString.Contains("-");

            if (hyphenPresent)
            {
                var hyphenIndex = claimString.IndexOf('-');
                ClaimNumber = Convert.ToDouble(claimString.Substring(0, hyphenIndex));
            }
            
            else
            {
                double claimNumber;
                var parseSuccess = Double.TryParse(claimString, out claimNumber);
                
                if (parseSuccess)
                {
                    ClaimNumber = claimNumber;
                }

                else
                {
                    ClaimNumber = 0;
                }
            }

            try
            {
                Address = ParseAddress(row["Insured Address"].ToString());
            }
            catch (System.Exception)
            {
                Address = new EcoAddress()
                {
                    City = string.Empty,
                    PostalCode = new PostalCode(""),
                    Province = Province.Invalid
                };
            }
            if (Address.Province == Province.QC)
            {
                ClaimModifier = "-Bien-PD-1";
            }
            
            else
            {
                ClaimModifier = "-Prop-PD-1";
            }
            
            Insured = row["Insured Name"].ToString();

            // total due is a formula - will this work?
            Amount = Convert.ToDouble(row["Total Due"].ToString());
            ClaimId = row["Claim ID"].ToString();
            Invoice = invoice;
            ReportStart = row["Report Start"].ToString();
            ReportEnd = row["Report End"].ToString();
        }

        public override void CleanData()
        {
            Insured = "REDACTED";
        }

        public override Dictionary<string, string> GenerateReportRow()
        {
            var reportInfo = new Dictionary<string, string>()
            {
                { "ClaimNumber", ClaimNumber.ToString() },
                { "ClaimModifier", ClaimModifier },
                { "Insured", Insured },
                { "Amount", Amount.ToString("0.00") },
                { "Address", $"{String.Join(", ", Address.AddressLines).Trim()}, {Address.City} {Address.Province} {Address.PostalCode}" },
                { "Invoice", Invoice },
                { "ReportStart", ReportStart },
                { "ReportEnd", ReportEnd },
                { "ClaimId", ClaimId }
            };

            return reportInfo;
        }

        private EcoAddress ParseAddress(string input)
        {
            var inputItems = input.Split( new string[] {","}, StringSplitOptions.RemoveEmptyEntries);

            // TODO: finish

            // postal code is last item in list - not always
            if (!inputItems.Last().Trim().IsMatch(@"^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$"))
            {
                throw new ArgumentException("invalid postal code");
            }

            var postalCode = new PostalCode(inputItems.Last());

            // now remove the postal code

            inputItems = inputItems.Take(inputItems.Length - 1).ToArray();

            var province = inputItems.Last().ParseEnumFromDescription<Province>();
            
            inputItems = inputItems.Take(inputItems.Length - 1).ToArray();

            var city = inputItems.Last().Trim();
    
            inputItems = inputItems.Take(inputItems.Length - 1).ToArray();

            var address = new EcoAddress()
            {
                PostalCode = postalCode,
                Province = province,
                City = city
            };

            foreach (var i in inputItems)
            {
                address.AddressLines.Add(i);
            }

            return address;
        }

        public bool IsCdsClaim()
        {
            if (ClaimNumber >= 1000000)
            {
                return true;
            }

            return false;
        }

        public override void IngestReportRow(DataRow row)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, string> GenerateFrenchReportRow()
        {
            throw new NotImplementedException();
        }
    }
}