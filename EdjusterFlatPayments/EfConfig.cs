using System;
using System.Text.Json;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;
using Newtonsoft.Json;

namespace Economical.EcoObjects.EdjusterFlatPayments
{
    public class EfConfig : Config
    {
        public int DelayBefore { get; set; }
        public int DelayAfter { get; set; }
        public string ReportSender { get; set; }
        public string[] ReviewerEmails { get; set; }
        public string NoteText { get; set; }
        public string ResourceFolder { get; set; }
        public string ReportName { get;set; }
        public int MaxProcessCount { get; set; }
        public string ReportEmailSubject { get; set; }
        public string CdsQueueName { get; set; }
        public string FamilyQueueName { get; set; }
        public string ReferenceText { get; set; }

        // now we don't need?
        // public double ValidationThreshold { get; set; }
        public Timeout Timeout { get; set; }
        public Guid? TimeoutId { get; set; }

        public EfConfig() { }

        public EfConfig(string fileText)
        {
            var config = IngestConfigFile(fileText);

            Duplicate(config);
        }

        public override void Duplicate(Config config)
        {
            var efConfig = config as EfConfig;

            DelayBefore = efConfig.DelayBefore;
            DelayAfter = efConfig.DelayAfter;
            ReportSender = efConfig.ReportSender;
            ReviewerEmails = efConfig.ReviewerEmails;
            NoteText = efConfig.NoteText;
            Timeout = efConfig.Timeout;
            ResourceFolder = efConfig.ResourceFolder;
            ReportName = string.Format(efConfig.ReportName, DateTime.Now.ToString(Configurations.ReportDateTimeFormat));
            ReportEmailSubject = efConfig.ReportEmailSubject;
            MaxProcessCount = efConfig.MaxProcessCount;
            CdsQueueName = efConfig.CdsQueueName;
            FamilyQueueName = efConfig.FamilyQueueName;
            ReferenceText = efConfig.ReferenceText;
        }

        public override Config IngestConfigFile(string fileText)
        {
            var config = JsonConvert.DeserializeObject<EfConfig>(fileText);

            return config;
        }
    }
}