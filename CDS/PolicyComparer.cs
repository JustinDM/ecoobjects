using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoExtensions;

namespace Economical.EcoObjects.CDS
{
    public class PolicyComparer : IEqualityComparer<Policy>
    {
        public bool Equals(Policy x, Policy y)
        {
            // number should be unique - just possible we get different names that are acceptable
            var isEqual = 
                x.Number.SimilarTo(y.Number)
                && x.Type.SimilarTo(y.Type)
                && x.InsuredName.SimilarTo(x.InsuredName)
                && x.Status == y.Status
                && x.IsOnline == y.IsOnline;
            
            return isEqual;
        }

        public int GetHashCode(Policy obj)
        {
            // should be a better way to convert string to int, will just use length for now
            long hCode = obj.InsuredName.Length ^ Convert.ToInt32(obj.IsOnline) * (uint)obj.Status;

            return hCode.GetHashCode();
        }
    }
}