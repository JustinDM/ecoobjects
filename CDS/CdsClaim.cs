using System;
using System.Collections.Generic;
using System.Linq;
using Economical.EcoExtensions;
using Economical.EcoObjects.CDS.Enums;
using Economical.EcoObjects.CDS.Task;
using Economical.EcoObjects.ContractorConnection.Enums;
using Economical.EcoObjects.General.Address;

namespace Economical.EcoObjects.CDS
{
    public class CdsClaim
    {
        public int ClaimNumber { get; set; }
        public DateTime DateOfLoss { get; set; }
        public ClaimType ClaimType { get; set; }
        public ClaimStatus ClaimStatus { get; set; }
        // this is the fully qualified policy - not just the number
        // 6962422-1-C-A-O-1
        public string Policy { get; set; }
        public bool IsCommercialOffline { get { return Policy.IsMatch(@"([a-zA-Z]-){3}\d"); }}
        public ClaimScore Score { get; set; }
        public string Name { get; set; }
        public EcoAddress Address { get; set; }
        public List<ClaimLog> Logs { get; set; } = new List<ClaimLog>();
        public List<ClaimTransaction> Transactions { get; set; } = new List<ClaimTransaction>();
        public List<ClaimReserve> Reserves { get; set; } = new List<ClaimReserve>();
        public List<LossLocationDetails> LossAddresses { get; set; } = new List<LossLocationDetails>();
        public string Broker { get; set; }
        public string Insurer { get; set; }
        public List<string> Insureds { get; set; } = new List<string>();
        public string Lessor { get; set; }
        public CustomerInfo CustomerInfo { get; set; } = new CustomerInfo();
        public Policy PolicyInfo { get; set; } = new Policy();
        public int FaultPercentage { get; set; }
        public string Owner { get; set; }
        public string LossCode { get; set; }
        public double LossPaid { get; set; }
        public double TotalPaid { get; set; }
        public double TotalSalvage { get; set; }
        public double ExpensePaid { get; set; }
        public double TotalSubrogation { get; set; }
        public List<Vehicle> Vehicles { get; set; } = new List<Vehicle>();
        public List<VehicleCoverage> Coverages { get; set; } = new List<VehicleCoverage>();
        public List<PartyDetail> PartyDetails { get; set; } = new List<PartyDetail>();
        public List<CdsClaim> RelatedClaims { get; set; } = new List<CdsClaim>();
        public List<CdsTask> Tasks { get; set; } = new List<CdsTask>();
        public string LossDescription { get; set; }

        public CdsClaim() { }

        // removing for now

        // public List<ClaimLog> GenerateLogs(string logText)
        // {
        //     var logLines = logText.Split("***".ToCharArray());
        //     var claimLogs = new List<ClaimLog>();

        //     foreach (var i in logLines)
        //     {
        //         if (!string.IsNullOrWhiteSpace(i))
        //         {
        //             var claimLog = new ClaimLog(i);
        //             claimLogs.Add(claimLog);
        //         }
        //     }

        //     return claimLogs;
        // }

        // public List<ClaimReserve> GenerateReserves(string inputText)
        // {
        //     var reserves = new List<ClaimReserve>();

        //     var lineNames = Enum.GetNames(typeof(UnitLine));

        //     var fileLines = inputText.Split(Environment.NewLine.ToCharArray()).Where(i => i.Split(' ').ItemPresent(lineNames)).ToArray();

        //     foreach (var i in fileLines)
        //     {
        //         var reserve = new ClaimReserve(i);
        //         reserves.Add(reserve);
        //     }

        //     return reserves;
        // }

        // need to bring this back based on claim type
        // still using Cc typed enums -> should be more general enums
        public List<ClaimTransaction> GenerateTransactions(string inputText, CcExpenseType expenseType, string claimantName)
        {
            var Units = new List<ClaimTransaction>();

            var UnitLines = inputText.Split(Environment.NewLine.ToCharArray())
                                        .Where(i => !i.Contains("Date") && !i.Contains("Submit") && !string.IsNullOrWhiteSpace(i)).ToArray();

            switch (expenseType)
            {
                case CcExpenseType.PROPPD:

                    foreach (var i in UnitLines)
                    {
                        var Unit = new PropertyTransaction(i, claimantName);
                        Units.Add(Unit);
                    }

                    return Units;

                default:
                    throw new ArgumentException($"Expense type '{expenseType.ToString()}' is not supported");
            }
        }

        public bool PolicyCheck(string policyNumber)
        {
            return Policy.Contains(policyNumber.RemoveLeadingZeroes());
        }

        public bool VehiclePresent (Vehicle vehicle)
        {
            var foundVehicle = Vehicles.FirstOrDefault(x => x.Vin == vehicle.Vin);

            if (foundVehicle == null)
            {
                return false;
            }

            return true;
        }

        public bool IngestTasks(string inputText)
        {
            var inputLines = inputText.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            // skip header row
            inputLines = inputLines.Skip(1).ToArray();

            foreach (var i in inputLines)
            {
                var task = new CdsTask(i);

                Tasks.Add(task);
            }

            // returning bool here just makes it easier to call the method in UiPath
            return true;
        }
    }
}