using System.ComponentModel;

namespace Economical.EcoObjects.CDS.Enums
{
    public enum LossLocation
    {
        [Description("On Premises")]
        OnPremises,
        [Description("Off Premises")]
        OffPremises
        
    }
}
