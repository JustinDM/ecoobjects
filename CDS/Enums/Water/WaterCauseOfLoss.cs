using System.ComponentModel;

namespace Economical.EcoObjects.CDS.Enums
{
    public enum WaterCauseOfLoss
    {
        Invalid,
        [Description("Sewer/Drain Backup")]
        SewerDrainBackup,
        [Description("Sump Failure")]
        SumpFailure,
        [Description("Watermain")]
        Watermain,
        [Description("Unknown")]
        Unknown,
        [Description("Other")]
        Other
    }
}
