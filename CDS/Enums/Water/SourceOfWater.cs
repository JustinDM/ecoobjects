using System.ComponentModel;

namespace Economical.EcoObjects.CDS.Enums
{
    public enum SourceOfWater
    {
        Invalid,
        [Description("Appliance")]
        Appliance,
        [Description("Foundation")]
        Foundation,
        [Description("Pipe/Plumbing")]
        PipePlumbing,
        [Description("Roof")]
        Roof,
        [Description("Sewer/Drain")]
        SewerDrain,
        [Description("Unknown")]
        Unknown,
        [Description("Other")]
        Other
    }
}
