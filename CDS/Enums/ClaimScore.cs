using System.ComponentModel;

namespace Economical.EcoObjects.CDS.Enums
{
    public enum ClaimScore
    {
        [Description("5 Star")]
        _5,
        [Description("4 Star")]
        _4,
        [Description("3 Star")]
        _3,
        [Description("2 Star")]
        _2,
        [Description("1 Star")]
        _1,
        [Description("Indeterminate Score")]
        IN,
        [Description("Not Scored")]
        NS,
        [Description("Cat Claims")]
        _99

    }
}
