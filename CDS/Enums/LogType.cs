namespace Economical.EcoObjects.CDS.Enums
{
    public enum CdsLogType
    {
        Invalid,
        Audit,
        Notes,
        Knowledge,
        Activity,
        Task,
        Interaction
    }
}
