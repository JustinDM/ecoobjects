namespace Economical.EcoObjects.CDS.Enums
{
    public enum ClaimType
    {
        // these can be a lot more complex, maybe don't use
        Invalid,
        PropBI,
        PropPD,
        PropTP,
        AutoPD,
        AutoBI,
        AutoAB
    }
}
