using System.ComponentModel;

namespace Economical.EcoObjects.CDS.Enums
{
    public enum ClaimStatus
    {
        Invalid,
        Clear,
        Closed,
        [Description("Created In Error")]
        CreatedInError,
        Open,
        Pending,
        [Description("Re-opened")]
        ReOpened,
        [Description("Reviewed - Not Validated")]
        ReviewedNotValidated,
        [Description("Claim is with the Payment Unit")]
        Payment,
        Subrogation,
        Salvage
    }
}
