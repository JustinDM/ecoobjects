namespace Economical.EcoObjects.CDS.Enums
{
    public enum VehicleInfoSource
    {
        ClaimDetails,
        PolicyCoverage
    }
}
