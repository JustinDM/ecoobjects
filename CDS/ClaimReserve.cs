using System;
using System.Linq;
using Economical.EcoExtensions;
using Economical.EcoObjects.General.Attributes;

namespace Economical.EcoObjects.CDS
{
    //système
    //é
    [FrenchNameAttribute("RéserveDeRéclamation")]
    public class ClaimReserve
    {
        [FrenchName("Action")]
        public string Action { get; set; }
        [FrenchName("Ligne")]
        public string Line { get; set; }
        [FrenchName("Couverture")]
        public int Coverage { get; set; }
        [FrenchName("Object")]
        public int Item { get; set; }
        [FrenchName("TypeDePerte")]
        public string LossType { get; set; }
        [FrenchName("RéserveDePerte")]
        public double LossReserve { get; set; }
        [FrenchName("RéserveDeDépense")]
        public double ExpReserve { get; set; }
        [FrenchName("PaiementDePerte")]
        public double LossPayment { get; set; }
        [FrenchName("PaiementDeDépense")]
        public double ExpPayment { get; set; }
        [FrenchName("PaiementTotalDePerte")]
        public double LossPaid { get; set; }
        [FrenchName("PaiementTotalDeDépense")]
        public double ExpPaid { get; set; }
        [FrenchName("NuméroDeLigne")]
        public int LineNumber { get; set; }

        public override string ToString()
        {
            return this.ClassToString();
        }

        public ClaimReserve() { }

        public ClaimReserve(string classString)
        {
            var typeName = typeof(ClaimReserve);
            classString = classString.Replace(typeName + ":", "").Trim();

            // I think this is a unique enough indicator, but it's possible this shows up in a string - maybe a more unique seperator should be used
            var classStringItems = classString.Split(new string[] { "'; " }, StringSplitOptions.RemoveEmptyEntries);

            var properties = typeName.GetProperties();

            foreach (var i in classStringItems)
            {
                var propertyItems = i.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);

                var classProperty = properties.First(x => x.Name == propertyItems[0].Trim());

                var propertyValueString = propertyItems[1].Trim().Replace("'", "").Replace(";", "");

                object propertyValue;

                switch(true)
                {
                    case true when classProperty.PropertyType.IsEnum:
                        propertyValue = Enum.Parse(classProperty.PropertyType, propertyValueString);
                        break;

                    case true when classProperty.PropertyType == typeof(double):
                        propertyValue = Convert.ToDouble(propertyValueString);
                        break;

                    case true when classProperty.PropertyType == typeof(int):
                        propertyValue = Convert.ToInt32(propertyValueString);
                        break;

                    default:
                        propertyValue = propertyValueString;
                        break;

                }
                
                classProperty.SetValue(this, propertyValue);
            }

        }

    }
}