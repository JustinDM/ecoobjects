using System;
using Economical.EcoObjects.CDS.Enums;

namespace Economical.EcoObjects.CDS
{
    public class Vehicle
    {
        public int Year { get; set; }
        public string MakeModel { get; set; }
        public string Vin { get; set; }
        // there are others but honestly don't see them being useful, vin should be enough
        public VehicleInfoSource InfoSource { get; set; }

        public Vehicle()
        {
            // for basic use

            // since the other constructor is for policy coverage vehicles
            InfoSource = VehicleInfoSource.ClaimDetails;
        }

        public Vehicle(string input)
        {
            // looking to pull this : 2008 DODGE/RAM TRUCK RAM 1500 LARAMIE QUA 1D7HU18258S618178
            // first item is the year, last is the vin

            var items = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            Year = Convert.ToInt32(items[0]);
            Vin = items[items.Length - 1];

            // can't count on any kind of uniform formatting - just lump it all together
            // vin is the only thing that matters anyway

            for (int x = 1; x < items.Length - 1; x++)
            {
                MakeModel += items[x] + " ";
            }

            MakeModel = MakeModel.Trim();

            // since we use the single line constructor for policy coverage vehicles
            InfoSource = VehicleInfoSource.PolicyCoverage;
        }
    }
}