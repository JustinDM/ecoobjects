using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class NoteNotFoundException : BusinessException
    {
        public NoteNotFoundException()
        {
        }

        public NoteNotFoundException(string message) : base(message)
        {
        }

        public NoteNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoteNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}