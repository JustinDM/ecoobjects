using System;
using System.Runtime.Serialization;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class FailedToOpenFinancialsScreenException : ApplicationException
    {
        public FailedToOpenFinancialsScreenException()
        {
        }

        public FailedToOpenFinancialsScreenException(string message) : base(message)
        {
        }

        public FailedToOpenFinancialsScreenException(string message, System.Exception innerException) : base(message, innerException)
        {
        }

        protected FailedToOpenFinancialsScreenException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}