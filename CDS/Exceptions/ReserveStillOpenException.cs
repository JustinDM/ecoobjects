using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class ReserveStillOpenException : ApplicationException
    {
        public ReserveStillOpenException()
        {
        }

        public ReserveStillOpenException(string message) : base(message)
        {
        }

        public ReserveStillOpenException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ReserveStillOpenException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}