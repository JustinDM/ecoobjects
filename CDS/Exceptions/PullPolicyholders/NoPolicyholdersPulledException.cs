using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class NoPolicyholdersPulledException : BusinessException
    {
        public NoPolicyholdersPulledException()
        {
        }

        public NoPolicyholdersPulledException(string message) : base(message)
        {
        }

        public NoPolicyholdersPulledException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoPolicyholdersPulledException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}