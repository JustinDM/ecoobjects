using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class FailedVehicleMatchException : BusinessException
    {
        public FailedVehicleMatchException()
        {
        }

        public FailedVehicleMatchException(string message) : base(message)
        {
        }

        public FailedVehicleMatchException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FailedVehicleMatchException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}