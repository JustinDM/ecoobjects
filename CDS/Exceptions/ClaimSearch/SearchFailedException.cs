using System;
using System.Runtime.Serialization;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class SearchFailedException : ApplicationException
    {
        public SearchFailedException()
        {
        }

        public SearchFailedException(string message) : base(message)
        {
        }

        public SearchFailedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SearchFailedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}