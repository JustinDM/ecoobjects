using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class InvalidSearchMethodException : BusinessException
    {
        public InvalidSearchMethodException()
        {
        }

        public InvalidSearchMethodException(string message) : base(message)
        {
        }

        public InvalidSearchMethodException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidSearchMethodException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}