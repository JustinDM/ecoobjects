using System;
using System.Runtime.Serialization;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class LaunchFailureException : ApplicationException
    {
        public LaunchFailureException()
        {
        }

        public LaunchFailureException(string message) : base(message)
        {
        }

        public LaunchFailureException(string message, System.Exception innerException) : base(message, innerException)
        {
        }

        protected LaunchFailureException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}