using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class PostalCodeParsingException : BusinessException
    {
        public PostalCodeParsingException()
        {
        }

        public PostalCodeParsingException(string message) : base(message)
        {
        }

        public PostalCodeParsingException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PostalCodeParsingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}