using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class NoReservesException : ApplicationException
    {
        public NoReservesException()
        {
        }

        public NoReservesException(string message) : base(message)
        {
        }

        public NoReservesException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoReservesException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}