using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class DestructionDateRequiredException : ApplicationException
    {
        public DestructionDateRequiredException()
        {
        }

        public DestructionDateRequiredException(string message) : base(message)
        {
        }

        public DestructionDateRequiredException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DestructionDateRequiredException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}