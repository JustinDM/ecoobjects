using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class NoResultsException : BusinessException
    {
        public NoResultsException()
        {
        }

        public NoResultsException(string message) : base(message)
        {
        }

        public NoResultsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoResultsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}