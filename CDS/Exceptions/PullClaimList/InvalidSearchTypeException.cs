using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class InvalidSearchTypeException : BusinessException
    {
        public InvalidSearchTypeException()
        {
        }

        public InvalidSearchTypeException(string message) : base(message)
        {
        }

        public InvalidSearchTypeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidSearchTypeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}