using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class NullNoteException : BusinessException
    {
        public NullNoteException()
        {
        }

        public NullNoteException(string message) : base(message)
        {
        }

        public NullNoteException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NullNoteException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}