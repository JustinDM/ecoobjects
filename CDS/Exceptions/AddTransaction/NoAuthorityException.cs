using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class NoAuthorityException : BusinessException
    {
        public NoAuthorityException()
        {
        }

        public NoAuthorityException(string message) : base(message)
        {
        }

        public NoAuthorityException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoAuthorityException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}