using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class ClaimantNotUpdatedToLegacyException : BusinessException
    {
        public ClaimantNotUpdatedToLegacyException()
        {
        }

        public ClaimantNotUpdatedToLegacyException(string message) : base(message)
        {
        }

        public ClaimantNotUpdatedToLegacyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ClaimantNotUpdatedToLegacyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}