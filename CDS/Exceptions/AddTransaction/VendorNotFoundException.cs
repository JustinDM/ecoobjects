using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class VendorNotFoundException : BusinessException
    {
        public VendorNotFoundException()
        {
        }

        public VendorNotFoundException(string message) : base(message)
        {
        }

        public VendorNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected VendorNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}