using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class UserDefinedException : BusinessException
    {
        public UserDefinedException()
        {
        }

        public UserDefinedException(string message) : base(message)
        {
        }

        public UserDefinedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UserDefinedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}