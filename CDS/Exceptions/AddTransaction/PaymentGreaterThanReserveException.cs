using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class PaymentGreaterThanReserveException : BusinessException
    {
        public PaymentGreaterThanReserveException()
        {
        }

        public PaymentGreaterThanReserveException(string message) : base(message)
        {
        }

        public PaymentGreaterThanReserveException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PaymentGreaterThanReserveException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}