using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class LineNotOpenException : BusinessException
    {
        public LineNotOpenException()
        {
        }

        public LineNotOpenException(string message) : base(message)
        {
        }

        public LineNotOpenException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LineNotOpenException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}