using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class ClaimStillOpenException : BusinessException
    {
        public ClaimStillOpenException()
        {
        }

        public ClaimStillOpenException(string message) : base(message)
        {
        }

        public ClaimStillOpenException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ClaimStillOpenException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}