using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class InvalidRepIdException : BusinessException
    {
        public InvalidRepIdException()
        {
        }

        public InvalidRepIdException(string message) : base(message)
        {
        }

        public InvalidRepIdException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidRepIdException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}