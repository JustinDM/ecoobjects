
using System;
using System.Runtime.Serialization;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class NoResponseException : ApplicationException
    {
        public NoResponseException()
        {
        }

        public NoResponseException(string message) : base(message)
        {
        }

        public NoResponseException(string message, System.Exception innerException) : base(message, innerException)
        {
        }

        protected NoResponseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}