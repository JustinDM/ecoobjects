using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class ClaimNotValidatedToLegacyException : BusinessException
    {
        public ClaimNotValidatedToLegacyException()
        {
        }

        public ClaimNotValidatedToLegacyException(string message) : base(message)
        {
        }

        public ClaimNotValidatedToLegacyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ClaimNotValidatedToLegacyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}