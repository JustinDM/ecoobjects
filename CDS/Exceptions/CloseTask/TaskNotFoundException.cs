using System;
using System.Runtime.Serialization;
using Economical.EcoObjects.General;

namespace Economical.EcoObjects.CDS.Exceptions
{
    [Serializable]
    public class TaskNotFoundException : BusinessException
    {
        public TaskNotFoundException()
        {
        }

        public TaskNotFoundException(string message) : base(message)
        {
        }

        public TaskNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected TaskNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}