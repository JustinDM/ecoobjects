using System;
using System.Globalization;
using Economical.EcoExtensions;

namespace Economical.EcoObjects.CDS.Task
{
    public class CdsTask
    {
        public int Number { get; set; }
        public string TaskType { get; set; }
        public string Description { get; set; }
        public string Subject { get; set; }
        public TaskStatus Status { get; set; }
        public string Owner { get; set; }
        public TaskPriority Priority { get; set; }
        public DateTime PlannedStartDate { get; set; }
        public int WorkflowProcessId { get; set; }
        public TaskConfirmation Confirmation { get; set; }

        public CdsTask() { }
        public CdsTask(string inputLine)
        {
            var inputItems = inputLine.Split('\t');
            Number = Convert.ToInt32(inputItems[0]);
            TaskType = inputItems[1];
            Description = inputItems[2];
            Subject = inputItems[3];
            Status = inputItems[4].ParseEnumFromDescription<TaskStatus>();
            Owner = inputItems[5];
            Priority = inputItems[6].ParseEnumFromDescription<TaskPriority>();
            PlannedStartDate = DateTime.ParseExact(inputItems[7], "dd-MMM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            if (string.IsNullOrWhiteSpace(inputItems[8]))
            {
                WorkflowProcessId = 0;
            }
            else
            {
                WorkflowProcessId = Convert.ToInt32(inputItems[8]);
            }
            Confirmation = inputItems[9].ParseEnumFromDescription<TaskConfirmation>();
        }
    }
}