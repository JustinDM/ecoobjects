namespace Economical.EcoObjects.CDS.Task
{
    public enum TaskPriority
    {
        Normal,
        Unprioritized,
        Urgent
    }
}
