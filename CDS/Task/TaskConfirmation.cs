using System.ComponentModel;

namespace Economical.EcoObjects.CDS.Task
{
    public enum TaskConfirmation
    {
        Required,
        [Description("Not Required")]
        NotRequired
    }
}
