using System.ComponentModel;

namespace Economical.EcoObjects.CDS.Task
{
    public enum TaskStatus
    {
        
        Accepted,
        Assigned,
        [Description("Auto In Planning")]
        AutoInPlanning,
        AutoReject,
        [Description("Blocked Assigned")]
        BlockedAssigned,
        [Description("Blocked Planned")]
        BlockedPlanned,
        Cancelled,
        Closed,
        Completed,
        [Description("Created In Error")]
        CreatedInError,
        [Description("In Planning")]
        InPlanning,
        [Description("In Progress")]
        InProgress,
        Interrupted,
        [Description("Not Started")]
        NotStarted,
        [Description("On Hold")]
        OnHold,
        Open,
        Pending,
        Planned,
        Received,
        Rejected,
        Traveling,
        Unassigned,
        Working
    }
}
