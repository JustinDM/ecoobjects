using System;
using System.Collections.Generic;
using Economical.EcoObjects.General.Address;

namespace Economical.EcoObjects.CDS
{
    public class Policy
    {
        public string Number { get; set; }
        public string Type { get; set; }
        public string InsuredName { get; set; }
        public DateTime EffectiveDate { get; set; }
        public int BrokerCode { get; set; }
        public string Company { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime InceptionDate { get; set; }
        public string BrokerName { get; set; }
        public string PolicySource { get; set; }
        public PolicyStatus Status { get; set; }
        public bool KeyClient { get; set; }
        public EcoAddress Address { get; set; } = new EcoAddress();
        public bool IsOnline { get; set; }
        public List<CdsClaim> Claims { get; set; } = new List<CdsClaim>();

    }
}