using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.CDS
{
    public enum PolicyStatus
    {
        Invalid,
        Active,
        Cancelled
    }
}