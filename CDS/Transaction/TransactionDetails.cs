using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Address;

namespace Economical.EcoObjects.CDS
{
    public class TransactionDetails
    {
        public int ControlNumber { get; set; }
        public int Company { get; set; }
        public double ChequeAmount { get; set; }
        public double QST { get; set; }
        public int ChequeNumber { get; set; }
        public DateTime ProcessDate { get; set; }
        public string Status { get; set; }
        public double AmountIssued { get; set; }
        public double AmountOutstanding { get; set; }
        public string Payee { get; set; }
        public string Mailee { get; set; }
        public EcoAddress Address { get; set; }
        public string Reference { get; set; }
        public DateTime DatePaid { get; set; }
        public DateTime DateRequested { get; set; }
        public DateTime DatePrint { get; set; }
    }
}