namespace Economical.EcoObjects.CDS
{
    public enum VendorSearchType
    {
        Invalid,
        Name,
        PostalCode
    }
}