using System;

namespace Economical.EcoObjects.CDS
{
    public abstract class ClaimTransaction
    {
        public DateTime Date { get; set; }
        public string Action { get; set; }
        public double Amount { get; set; }
        // not sure what this is
        public string Qst { get; set; }
        public string Line { get; set; }
        public string Type { get; set; }
        public string Claimant { get; set; }
        // not sure what this is either
        public string ExpInd { get; set; }
        public string Payee { get; set; }
        public string Mailee { get; set; }
        public int? ControlNumber { get; set; }
        public string Status { get; set; }
        public string Representative { get; set; }

        public bool IsLossPayment { get; set; }
        public TransactionDetails TransactionDetails { get; set; } = new TransactionDetails();

        // to be used for the 'CDS Add Claim transaction' activity from 'EcoCDS'
        public ClaimTransaction(double amount, string payee, string reference, string action = "S", bool lossPayment = false, string mailee = "")
        {
            Action = action;
            Amount = amount;
            Payee = payee;
            if (string.IsNullOrWhiteSpace(mailee))
            {
                Mailee = payee;
            } 
            else
            {
                Mailee = mailee;
            }
            TransactionDetails.Reference = reference;
            IsLossPayment = lossPayment;
        }

        public ClaimTransaction() { }
    }
}