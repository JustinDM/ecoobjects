using System;
using System.Collections.Generic;
using Economical.EcoExtensions;
using Economical.EcoObjects.CDS;

namespace Economical.EcoObjects.CDS
{
    public class ClaimTransactionComparer : IEqualityComparer<ClaimTransaction>
    {
        public bool Equals(ClaimTransaction x, ClaimTransaction y)
        {
            // short circtuit if one is long and one short, return false
            if (x.Type is null && y.Type != null)
                return false;

            if (
                x.Date == y.Date
                && x.Action.SimilarTo(y.Action)
                && x.Amount == y.Amount
                && x.Claimant.SimilarTo(y.Claimant)
                && x.ControlNumber == y.ControlNumber
                && x.Line.SimilarTo(y.Line)
                && x.Mailee.SimilarTo(y.Mailee)
                && x.Payee.SimilarTo(y.Payee)
                && x.Status.SimilarTo(y.Status)
                && x.Representative.SimilarTo(y.Representative)
                && x.Type.SimilarTo(y.Type))
                return true;

            return false;
        }

        public int GetHashCode(ClaimTransaction obj)
        {
            long hCode = Convert.ToInt64(obj.Amount) ^ Convert.ToInt64(obj.Date.ToString("ddMMyyyyhhmmss"));

            return hCode.GetHashCode();
        }
    }
}