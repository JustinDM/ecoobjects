using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.General.Address;

namespace Economical.EcoObjects.CDS
{
    public class VendorSearchParameters
    {
        // so name and code would function identically, right?
        // what do we verify back to?

        // one string search value
        // enum of what it is
        // optional secondary value that can be used to validate if multiple results are present
        public string SearchValue { get; set; }
        public string SecondarySearchValue { get; set; }
        public VendorSearchParameters(string searchValue, string secondarySearchType = "")
        {
            
        }
    }
}