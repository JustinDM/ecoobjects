using System;
using System.Globalization;
using System.Linq;

namespace Economical.EcoObjects.CDS
{
    public class PropertyTransaction : ClaimTransaction
    {
        private readonly string[] UnitStatuses = new string[] { "Paid", "Print Pending", "Issued", "Suspense", "Cancelled", "Stopped", "Spoiled", "Accumulated", "Written Off" };

        public PropertyTransaction(double amount, string payee, string reference, string action = "S", bool lossPayment = false, string mailee = "")
            : base(amount, payee, reference, action, lossPayment, mailee)
        {

        }

        private void ParseDate(string[] inputItems)
        {
            if (inputItems.Length != 2)
                throw new ArgumentException("inputItems should only contain 2 items");

            ParseDate(inputItems[0], inputItems[1]);
        }

        private void ParseDate(string dateLine, string timeLine)
        {
            Date = DateTime.ParseExact($"{dateLine} {timeLine}", "dd-MMM-yyyy hh:mm:ss", CultureInfo.InvariantCulture);
        }

        public PropertyTransaction(string line, string claimantName)
        {
            // remove claimant name
            Claimant = claimantName;
            line = line.Replace(claimantName, "").Replace("\t", " ");

            // split into items
            var lineItems = line.Split(' ');

            lineItems = lineItems.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

            // get the amountField - only one with $ - assuming as well that we always find one
            var amountItem = lineItems.First(x => x.Contains("$"));
            Amount = Convert.ToDouble(amountItem.Replace("$", ""));

            // get the position of the amountField
            var amountItemIndex = Array.IndexOf(lineItems, amountItem);

            // parse the fields before the amount field
            ParsePreAmountFields(lineItems.Take(amountItemIndex).ToArray());

            // parse the fields after the amount field
            ParsePostAmountFields(lineItems.Skip(amountItemIndex + 1).ToArray());

        }

        private void ParsePreAmountFields(string[] preAmountItems)
        {
            // first two items are the date fields
            ParseDate(preAmountItems.Take(2).ToArray());

            // remaining fields are the action
            Action = string.Join(" ", preAmountItems.Skip(2)).Trim();
        }

        private void ParsePostAmountFields(string[] postAmountItems)
        {
            // first need to find the status field
            // since it can be multiple items we construct the items as a string
            var postAmountLine = string.Join(" ", postAmountItems).Trim();

            // need to find and remove a status from the string

            int statusEndIndex = 0;
            int statusStartIndex = 0;

            foreach (var i in UnitStatuses)
            {
                if (postAmountLine.Contains(i))
                {
                    Status = i;
                    // split the status into items, remove each item from the postAmountItems
                    var statusItems = i.Split(' ');

                    // convert to list to use the remove method
                    var postItemsList = postAmountItems.ToList();

                    // find the position of the last satus item in the original item array
                    statusEndIndex = Array.IndexOf(postAmountItems, statusItems.Last());

                    // need to know what the first status time is as well because that's where we want to cutoff
                    statusStartIndex = Array.IndexOf(postAmountItems, statusItems[0]);

                    postItemsList.RemoveAll(x => statusItems.Contains(x));

                    // assign the cleaned list back to the original array
                    postAmountItems = postItemsList.ToArray();

                    // stop after the first status - assuming that there shouldn't be multiple statuses
                    break;
                }
            }

            // if statusEndIndex is non-zero then we found the status and we can pull out the representative items
            if (statusStartIndex != 0)
            {
                // if status is one word - need to not subtract one from the index
                // if the status is two words - need to subtract one from the index
                Representative = string.Join(" ", postAmountItems.Skip(statusStartIndex)).Trim();

                // remove the representative items from the array
                postAmountItems = postAmountItems.Take(statusStartIndex).ToArray();

                // once we've removed the representative we can determine if the post amount items contains an ExpInd value or not
                // based on the amount of items present
                // there is a vulnerability here if both a QST item is present but not an ExpInd item, as they'll appear the same
                // we don't use either value right now so I'll accept it

                if (postAmountItems.Length >= 6)
                {
                    Line = postAmountItems[0];
                    Type = postAmountItems[1];
                    Payee = postAmountItems[2];
                    ExpInd = postAmountItems[3];
                    Mailee = postAmountItems[4];
                    ControlNumber = Convert.ToInt32(postAmountItems[5]);
                }
                else
                {
                    Line = postAmountItems[0];
                    Type = postAmountItems[1];
                    Payee = postAmountItems[2];
                    Mailee = postAmountItems[3];
                    ControlNumber = Convert.ToInt32(postAmountItems[4]);
                }

            }
            // if statusEndIndex is zero then we didn't find a status which means it's a short Unit so only the Line and the representative are present
            else
            {
                // need to handle if there's no line field - which seems to be only for 'Closed' action Units
                if (Action == "Closed")
                {
                    Representative = string.Join(" ", postAmountItems);
                }

                else
                {
                    Line = postAmountItems[0];
                    Representative = string.Join(" ", postAmountItems.Skip(1)).Trim();
                }
            }
        }
    }
}