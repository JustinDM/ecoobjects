using System;
using Economical.EcoObjects.General.Address;
using Economical.EcoExtensions;

namespace Economical.EcoObjects.CDS
{
    public class PartyDetail
    {
        public string Role { get; set; }
        public string Name { get; set; }
        public string LicenseNumber { get; set; }
        public EcoAddress Address { get; set; }
        public string ContactDetails { get; set; }

        public string PullEmail()
        {
            var emailMatches = ContactDetails.GetMatches(@"(\s|^)[a-zA-Z0-9\._]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+(\s|$)");

            if (emailMatches.Length == 0)
            {
                throw new ArgumentException("contact details doesn't contain an email");
            }

            return emailMatches[0];
        }
    }
}