using System.ComponentModel;

namespace Economical.EcoObjects.CDS
{
    public enum AuthorType
    {
        ALE,
        Agenda,
        Building,
        [Description("Claims Assistant")]
        ClaimsAssistant,
        [Description("Clearing House Remarks")]
        ClearingHouseRemarks,
        Contents,
        [Description("Head Office CTA Remarks")]
        HeadOfficeCtaRemarks,
        [Description("Head Office Claims Assistant")]
        HeadOfficeClaimsAssistant,
        Inquiry,
        Intake,
        Investigation,
        [Description("Regional CTA Remarks")]
        RegionalCtaRemarks,
        [Description("Salvage Work Plan")]
        SalvageWorkPlan,
        [Description("Subrogation Work Plan")]
        SubrogationWorkPlan,
        [Description("Team Leader")]
        TeamLeader,
        [Description("Underwriting Notice")]
        UnderwritingNotice,
        [Description("Work Plan")]
        WorkPlan,
        [Description("Limitation & POL Communication")]
        LimitationAndPolCommunication
    }
}
