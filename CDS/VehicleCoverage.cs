using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.CDS
{
    public class VehicleCoverage
    {
        public string CoverageType { get; set; }
        public string Limits { get; set; }
        public double Deductible { get; set; }
    }
}