namespace Economical.EcoObjects.CDS
{
    public class CustomerInfo
    {
        public string CustomerEmail { get; set; }
        public double CustomerNumber { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string CustomerType { get; set; }
    }
}