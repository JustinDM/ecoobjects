using Economical.EcoObjects.CDS.Enums;
using Economical.EcoObjects.General.Address;

namespace Economical.EcoObjects.CDS
{
    public class LossLocationDetails
    {
        public string Owner { get; set; }
        public string Occupancy { get; set; }
        public string ClaimType { get; set; }
        public EcoAddress UpperAddress { get; set; }
        public LossLocation LossLocation { get; set; }
        public string UpperDetails { get; set; }
        public EcoAddress LossDetailsAddress { get; set; }
        public string LossDiscoverer { get; set; }
        public string TimeLossDiscovered { get; set; }
        // these fields are based on what type of claim it is
        
        // water escape/rupture
        public SourceOfWater SourceOfWater { get; set; }
        public WaterCauseOfLoss WaterCauseOfLoss { get; set; }
        public string Details { get; set; }
        public string DescriptionOfLoss { get; set; }
        public string HasThisProblemOccuredBefore { get; set; }
        public string RecentWorkDescription { get; set; }
        public string ConditionOfRoofEavestroughs { get; set; }
        public string OtherLocalPremisesAffected { get; set; }
    }
}