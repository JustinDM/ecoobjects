using System;
using Economical.EcoObjects.CDS.Enums;

namespace Economical.EcoObjects.CDS
{
    public class ClaimLog
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public CdsLogType Type { get; set; }

        // don't like this - pulling employees has been an utter shitshow in relation to Units
        // don't bother
        
        // public EconomicalEmployee Author { get; set; }
        public string Content { get; set; }
        public AuthorType AuthorType { get; set; }

        // need to revisit this - make sure employee is workable
        
        // public ClaimLog(string logLine)
        // {
        //     var index = 0;

        //     logLine = logLine.Replace(Environment.NewLine, " ").Trim();
        //     var lineItems = logLine.Split(' ');

        //     Date = DateTime.ParseExact($"{lineItems[index++]} {lineItems[index++]}", "dd-MMM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        //     Type = (LogType) Enum.Parse(typeof(LogType), lineItems[index++].Replace(":", ""));
            
        //     if (Type == LogType.Interaction)
        //         Id = int.Parse(lineItems[index++]);

        //     Author = new EconomicalEmployee($"{lineItems[index++]} {lineItems[index++]}", lineItems[index++]);
        //     for (var i = index; i <= lineItems.Length - 1; i++, index++)
        //         Content += lineItems[i] + " ";

        //     Content = Content.Trim();
        // }

        public ClaimLog() { }
    }
}