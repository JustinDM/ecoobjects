using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.CDS
{
    public struct PolicySearchParameters
    {
        public string FirstName { get; }
        public string LastName { get; }
        public string CityProvince { get; }
        public PolicySearchType SearchType { get; }
        public string PolicyNumber { get; }

        public PolicySearchParameters(string lastName, string firstName = "", string cityProvince = "")
        {
            FirstName = firstName;
            LastName = lastName;
            CityProvince = cityProvince;
            SearchType = PolicySearchType.Name;
            PolicyNumber = string.Empty;

            if ( string.IsNullOrWhiteSpace(lastName))
            {
                throw new ArgumentException("please provide at least one search criteria");
            }
        }

        public PolicySearchParameters(string policyNumber)
        {
            PolicyNumber = policyNumber;
            SearchType = PolicySearchType.Number;
            FirstName = "";
            LastName = "";
            CityProvince = "";
        }

    }
}