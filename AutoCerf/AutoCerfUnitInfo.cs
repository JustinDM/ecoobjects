using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Economical.EcoExtensions;
using Economical.EcoObjects.CDS;
using Economical.EcoObjects.CDS.Enums;
using Economical.EcoObjects.General.Address;
using Economical.EcoObjects.General.Attributes;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.AutoCerf
{
    [FrenchName("ÉlémentInfoAutoCerf")]
    public class AutoCerfUnitInfo : UnitInfo
    {
        [FrenchName("NuméroDeRéclamation")]
        public int ClaimNumber { get; set; }
        [FrenchName("Modificateur")]
        public string ClaimModifier { get; set; } = string.Empty;
        [FrenchName("MontantDeRéparation")]
        public double RepairAmount { get; set; }
        [FrenchName("MontantDeLocation")]
        public double RentalAmount { get; set; }
        [FrenchName("NomDeLassuré")]
        public string InsuredName { get; set; } = string.Empty;
        public double GrossTotal { get; set; }
        [FrenchName("PreviousNetTotal")]
        public double PreviousNetTotal { get; set; }
        public double NetTotal { get; set; }
        public double NetSupplement { get; set; }
        [FrenchName("AddresseAssuré")]
        public EcoAddress InsuredAddress { get; set; } = new EcoAddress();
        [FrenchName("NomDuVendeur")]
        public string VendorName { get; set; } = string.Empty;
        [FrenchName("PaiementDeRéparationPrécédentTrouvé")]
        public bool PreviousRepairPaymentPresent { get; set; }
        [FrenchName("PaiementDeLocationPrécédentTrouvé")]
        public bool PreviousRentalPaymentPresent { get; set; }
        [FrenchName("CodeDePerte")]
        public string LossCode { get; set; } = string.Empty;
        [FrenchName("RéservesDeRéparation")]
        public List<ClaimReserve> RepairReserveLines { get; set; } = new List<ClaimReserve>();
        [FrenchName("RéservesDeLocation")]
        public List<ClaimReserve> RentalReserveLines { get; set; } = new List<ClaimReserve>();
        [FrenchName("DéductibleDeRéparation")]
        public double RepairDeductible { get; set; }
        [FrenchName("PaimentSupplémentaire")]
        public bool IsSupplement { get; set; }
        [FrenchName("ConditionDeLaRéclamation")]
        public ClaimStatus ClaimStatus { get; set; }
        [FrenchName("ConditionDuPaiementDeRéparation")]
        public string RepairPaymentStatus { get; set; } = string.Empty;
        [FrenchName("ConditionDuPaiementDeLocation")]
        public string RentalPaymentStatus { get; set; } = string.Empty;
        [FrenchName("PaiementDeLocationRequis")]
        public bool RentalPaymentRequired { get; set; }
        [FrenchName("Ajusteur")]
        public string ClaimOwner { get; set; } = string.Empty;
        [FrenchName("AddresseCourrielDeLajusteur")]
        public string ClaimOwnerEmail { get; set; } = string.Empty;
        [FrenchName("TypDinspection")]
        public string InspectionType { get; set; } = string.Empty;
        [FrenchName("NombreDeJourDeLocation")]
        public int RentalNumberOfDays { get; set; }
        [FrenchName("TauxDeLocation")]
        public double RentalDailyRate { get; set; }
        [FrenchName("SupplémentDeLocation")]
        public double RentalAdditionalCosts { get; set; }
        [FrenchName("ImpôtDeLocation")]
        public double RentalTax { get; set; }
        public bool RepairPaymentMade { get; set; }
        public bool RentalPaymentMade { get; set; }
        public string RentalCommentText { get; set; } = string.Empty;
        public double BettermentDeduction { get; set; }
        public double FranchiseDeduction { get; set; }
        public double AutreCoutsDeduction { get; set; }
        public double RevalorisationDeduction { get; set; }
        public double BonsEtServicesDeduction { get; set; }
        public double HstDeduction { get; set; }
        public double GoodsAndServicesDeduction { get; set; }
        public override void CleanData()
        {
            // no cleaning required
        }

        public override Dictionary<string, string> GenerateReportRow()
        {
            var properties = typeof(AutoCerfUnitInfo).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
            var reportInfo = new Dictionary<string, string>();
            
            foreach (var i in properties)
            {
                var propertyName = i.Name;
                var value = i.GetValue(this);

                if (value.GetType() == typeof(List<ClaimReserve>))
                {
                    value = string.Join(",", (List<ClaimReserve>)value);
                }

                reportInfo.Add(propertyName, value.ToString());  
            }

            return reportInfo;
        }

        public override Dictionary<string, string> GenerateFrenchReportRow()
        {
            var properties = typeof(AutoCerfUnitInfo).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
            var reportInfo = new Dictionary<string, string>();
            
            foreach (var i in properties)
            {
                var propertyName = string.Empty;

                Attribute[] attrs = Attribute.GetCustomAttributes(i, typeof(FrenchNameAttribute));  
  
                propertyName = ((FrenchNameAttribute)attrs[0]).FrenchName;

                var value = i.GetValue(this);

                if (value.GetType() == typeof(List<ClaimReserve>))
                {
                    value = string.Join(",", (List<ClaimReserve>)value);
                }

                reportInfo.Add(propertyName, value.ToString());  
            }

            return reportInfo;
            // get their name based off the attribute
            // check if the object has a translate method?
            // above code not going to work I think?
            // no should be able to check
        }

        // ingest report row french?
        // why the fuck

        public override void IngestReportRow(DataRow row)
        {
            var properties = typeof(AutoCerfUnitInfo).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
            properties = properties.Where(x => row.Table.Columns.Contains(x.Name)).ToArray();

            foreach (var i in properties)
            {
                if (i.GetType() == typeof(List<string>))
                {
                    i.SetValue(this, row[i.Name].ToString()
                        .Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList());
                }
                else if (i.PropertyType == typeof(EcoAddress))
                {
                    i.SetValue(this, new EcoAddress(row[i.Name].ToString()));
                }
                else if (i.PropertyType == typeof(double))
                {
                    i.SetValue(this, Convert.ToDouble(row[i.Name].ToString()));
                }
                else if (i.PropertyType == typeof(int))
                {
                    i.SetValue(this, Convert.ToInt32(row[i.Name].ToString()));
                }
                else if (i.PropertyType == typeof(bool))
                {
                    i.SetValue(this, Convert.ToBoolean(row[i.Name].ToString()));
                }
                else if (i.PropertyType == typeof(List<ClaimReserve>))
                {
                    var reservesLine = row[i.Name].ToString();
                    var reserveLines = reservesLine.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                    var reserves = new List<ClaimReserve>();

                    foreach (var line in reserveLines)
                    {
                        var reserve = new ClaimReserve(line);
                        reserves.Add(reserve);
                    }

                    i.SetValue(this, reserves);
                }
                else if (i.PropertyType == typeof(ClaimStatus))
                {
                    i.SetValue(this, row[i.Name].ToString().ParseEnumFromDescription<ClaimStatus>());
                }
                else
                {
                    i.SetValue(this, row[i.Name].ToString());
                }
            }
        }

        public string IngestClaimNumber(string claimNumber)
        {
            var claimNumberItems = claimNumber.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
            ClaimNumber = Convert.ToInt32(claimNumberItems[0].Trim());

            if (claimNumberItems.Length > 1)
            {
                ClaimModifier = "-" + string.Join("-", claimNumberItems.Skip(1).ToArray());
            }

            return claimNumber;
        }
    }
}