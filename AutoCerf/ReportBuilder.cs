using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.AutoCerf
{
    public static class ReportBuilder
    {
        public static DataTable GenerateEnglishEndOfDayReport(DataTable combinedReport)
        {
            // Claim Number
            // Adjuster Name
            // Insured Name
            // Cerf Name
            // Repair Amt Requested
            // Repair payment status – Successful/Unsuccessful
            // If Repair Payment failed – Reason
            // Rental Amt Requested
            // Rental Payment Status – Successful/Unsuccessful
            // If Rental Payment Failed – Reason

            var eodTable = new DataTable();

            eodTable.Columns.Add("Claim Number");
            eodTable.Columns.Add("Adjuster Name");
            eodTable.Columns.Add("Insured Name");
            eodTable.Columns.Add("Cerf Name");
            eodTable.Columns.Add("Supplement Payment?");
            eodTable.Columns.Add("Repair Amt Requested");
            eodTable.Columns.Add("Repair payment made?");
            eodTable.Columns.Add("Repair payment status");
            eodTable.Columns.Add("Rental Amount Requested");
            eodTable.Columns.Add("Rental payment made?");
            eodTable.Columns.Add("Rental Payment Status");

            foreach (DataRow i in combinedReport.Rows)
            {
                var eodRow = eodTable.NewRow();

                var unit = new Unit();
                unit.UnitInfo = new AutoCerfUnitInfo();
                unit.IngestReportRow(i);
                var info = unit.UnitInfo as AutoCerfUnitInfo;

                eodRow["Claim Number"] = info.ClaimNumber.ToString() + info.ClaimModifier;
                eodRow["Adjuster Name"] = info.ClaimOwner;
                eodRow["Insured Name"] = info.InsuredName;
                eodRow["Cerf Name"] = info.VendorName;
                eodRow["Repair Amt Requested"] = info.RepairAmount;
                eodRow["Supplement Payment?"] = info.IsSupplement.ToString();
                eodRow["Repair payment made?"] = info.RepairPaymentMade.ToString();
                eodRow["Repair payment status"] = info.RepairPaymentStatus;
                eodRow["Rental payment made?"] = info.RentalPaymentMade.ToString();
                eodRow["Rental Amount Requested"] = info.RentalAmount;
                eodRow["Rental Payment Status"] = info.RentalPaymentStatus;

                eodTable.Rows.Add(eodRow);
            }

            return eodTable;
        }

        public static DataTable GenerateFrenchEndOfDayReport(DataTable combinedReport)
        {
            // Numéro du sinistre
            // Nom de l'expert
            // Nom de l'assuré
            // Nom du Cerf
            // Montant de la réparation demandée
            // Statut du paiement des réparations - Succès/échec
            // Si le paiement des réparations a échoué - Raison
            // Montant de la location demandé
            // Statut du paiement de la location - Succès/Échec
            // Si le paiement de la location a échoué - Raison

            var eodTable = new DataTable();

            eodTable.Columns.Add("Numéro du sinistre");
            eodTable.Columns.Add("Nom de l'expert");
            eodTable.Columns.Add("Nom de l'assuré");
            eodTable.Columns.Add("Nom du Cerf");
            eodTable.Columns.Add("Montant de la réparation demandée");
            eodTable.Columns.Add("Paiement supplémentaire?");
            eodTable.Columns.Add("Paiement des réparations accomplit?");
            eodTable.Columns.Add("Statut du paiement des réparations - Succès/échec");
            eodTable.Columns.Add("Montant de la location demandé");
            eodTable.Columns.Add("Paiement de la location accomplit?");
            eodTable.Columns.Add("Statut du paiement de la location - Succès/Échec");

            foreach (DataRow i in combinedReport.Rows)
            {
                var eodRow = eodTable.NewRow();

                var unit = new Unit();
                unit.UnitInfo = new AutoCerfUnitInfo();
                unit.IngestReportRow(i);
                var info = unit.UnitInfo as AutoCerfUnitInfo;

                eodRow["Numéro du sinistre"] = info.ClaimNumber.ToString() + info.ClaimModifier;
                eodRow["Nom de l'expert"] = info.ClaimOwner;
                eodRow["Nom de l'assuré"] = info.InsuredName;
                eodRow["Nom du Cerf"] = info.VendorName;
                eodRow["Paiement supplémentaire?"] = info.IsSupplement;
                eodRow["Montant de la réparation demandée"] = info.RepairAmount;
                eodRow["Paiement des réparations accomplit?"] = info.RepairPaymentMade.ToString();
                eodRow["Statut du paiement des réparations - Succès/échec"] = info.RepairPaymentStatus;
                eodRow["Montant de la location demandé"] = info.RentalAmount;
                eodRow["Statut du paiement de la location - Succès/Échec"] = info.RentalPaymentStatus;
                eodRow["Paiement de la location accomplit?"] = info.RentalPaymentMade.ToString();

                eodTable.Rows.Add(eodRow);
            }

            return eodTable;
        }

        public static string CreateEodReport(string eodReportPath, DataTable combinedReport)
        {
            var englishTable = GenerateEnglishEndOfDayReport(combinedReport);
            var frenchTable = GenerateFrenchEndOfDayReport(combinedReport);

            if (string.IsNullOrWhiteSpace(eodReportPath))
            {
                throw new ArgumentNullException("eodReportPath is null");
            }

            var wb = new XLWorkbook();
            wb.Worksheets.Add(englishTable, "English");
            wb.Worksheets.Add(frenchTable, "French");
            wb.SaveAs(eodReportPath);

            return eodReportPath;
        }

        public static DataTable CreateCumulativeReport(Handler[] handlers)
        {
            handlers = handlers.Where(h => h.Process == RpaProcess.AutoCerfPayments).ToArray();

            var reportTable = new DataTable();

            reportTable.Columns.Add("HandlerStartDate");
            reportTable.Columns.Add("HandlerEndDate");
            reportTable.Columns.Add("UnitStartDate");
            reportTable.Columns.Add("UnitEndDate");
            reportTable.Columns.Add("Status");
            reportTable.Columns.Add("FailureException");
            reportTable.Columns.Add("StatusMessage");
            reportTable.Columns.Add("ClaimNumber");
            reportTable.Columns.Add("ClaimModifier");
            reportTable.Columns.Add("RepairAmount");
            reportTable.Columns.Add("RentalAmount");
            reportTable.Columns.Add("InsuredName");
            reportTable.Columns.Add("GrossTotal");
            reportTable.Columns.Add("NetTotal");
            reportTable.Columns.Add("PreviousNetTotal");
            reportTable.Columns.Add("InsuredAddress");
            reportTable.Columns.Add("VendorName");
            reportTable.Columns.Add("PreviousRepairPaymentPresent");
            reportTable.Columns.Add("PreviousRentalPaymentPresent");
            reportTable.Columns.Add("LossCode");
            reportTable.Columns.Add("RepairReserveLines");
            reportTable.Columns.Add("RentalReserveLines");
            reportTable.Columns.Add("RepairDeductible");
            reportTable.Columns.Add("IsSupplement");
            reportTable.Columns.Add("ClaimStatus");
            reportTable.Columns.Add("RepairPaymentStatus");
            reportTable.Columns.Add("RentalPaymentStatus");
            reportTable.Columns.Add("RentalPaymentRequired");
            reportTable.Columns.Add("ClaimOwner");
            reportTable.Columns.Add("ClaimOwnerEmail");
            reportTable.Columns.Add("InspectionType");
            reportTable.Columns.Add("RentalCommentText");
            reportTable.Columns.Add("RentalNumberOfDays");
            reportTable.Columns.Add("RentalDailyRate");
            reportTable.Columns.Add("RentalAdditionalCosts");
            reportTable.Columns.Add("RentalTax");
            reportTable.Columns.Add("RepairPaymentMade");
            reportTable.Columns.Add("RentalPaymentMade");


            foreach (var h in handlers)
            {
                foreach (var u in h.Units)
                {
                    var row = reportTable.NewRow();
                    var info = u.UnitInfo as AutoCerfUnitInfo;
                    row["HandlerStartDate"] = h.StartDate.ToString(Configurations.PrettyDateTimeFormat);
                    row["HandlerEndDate"] = h.EndDate.ToString(Configurations.PrettyDateTimeFormat);
                    row["UnitStartDate"] = u.StartDate.ToString(Configurations.PrettyDateTimeFormat);
                    row["UnitEndDate"] = u.EndDate.ToString(Configurations.PrettyDateTimeFormat);
                    row["Status"] = u.Status.ToString();
                    row["StatusMessage"] = u.StatusMessage;
                    if (u.FailureException != null)
                    {
                        row["FailureException"] = u.FailureException.GetType().AssemblyQualifiedName;
                    }
                    row["ClaimNumber"] = info.ClaimNumber.ToString();
                    row["ClaimModifier"] = info.ClaimModifier;
                    row["RepairAmount"] = info.RepairAmount.ToString("0.00");
                    row["RentalAmount"] = info.RentalAmount.ToString("0.00");
                    row["InsuredName"] = info.InsuredName;
                    row["GrossTotal"] = info.GrossTotal.ToString("0.00");
                    row["NetTotal"] = info.NetTotal.ToString("0.00");
                    row["PreviousNetTotal"] = info.PreviousNetTotal.ToString("0.00");
                    row["InsuredAddress"] = info.InsuredAddress.ToString();
                    row["VendorName"] = info.VendorName;
                    row["PreviousRepairPaymentPresent"] = info.PreviousRepairPaymentPresent.ToString();
                    row["PreviousRentalPaymentPresent"] = info.PreviousRentalPaymentPresent.ToString();
                    row["RepairPaymentStatus"] = info.RepairPaymentStatus;
                    row["RentalPaymentStatus"] = info.RentalPaymentStatus;
                    row["LossCode"] = info.LossCode;
                    row["RepairReserveLines"] = string.Join(",", info.RepairReserveLines);
                    row["RentalReserveLines"] = string.Join(",", info.RentalReserveLines);
                    row["RentalPaymentRequired"] = info.RentalPaymentRequired.ToString();
                    row["ClaimOwner"] = info.ClaimOwner;
                    row["ClaimOwnerEmail"] = info.ClaimOwnerEmail;
                    row["InspectionType"] = info.InspectionType;
                    row["RentalCommentText"] = info.RentalCommentText;
                    row["RentalNumberOfDays"] = info.RentalNumberOfDays.ToString();
                    row["RentalDailyRate"] = info.RentalDailyRate.ToString();
                    row["RentalAdditionalCosts"] = info.RentalAdditionalCosts.ToString();
                    row["RentalTax"] = info.RentalTax.ToString();
                    row["RepairPaymentMade"] = info.RepairPaymentMade.ToString();
                    row["RentalPaymentMade"] = info.RentalPaymentMade.ToString();

                    reportTable.Rows.Add(row);
                }
            }

            return reportTable;
        }
    }
}