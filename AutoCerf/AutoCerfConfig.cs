using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoExtensions;
using Economical.EcoObjects.Audatex;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Models;

namespace Economical.EcoObjects.AutoCerf
{
    public class AutoCerfConfig : Config
    {
        public Timeout Timeout { get; set; } = new Timeout();
        public AudatexFilterConfiguration FilterConfiguration { get; set; } = new AudatexFilterConfiguration();
        public string ResourceFolderPath { get; set; }
        public string[] Reviewers { get; set; }
        public string CdsNoteTemplate { get; set; }
        public string AudatexNoteTemplate { get; set; }
        public string RepairTaskName { get; set; }
        public Browser Browser { get; set; }

        public AutoCerfConfig() { }

        public AutoCerfConfig(DataTable table)
        {
            foreach (DataRow i in table.Rows)
            {
                var rowValue = i["Value"].ToString();
                // if we're calling this it should be created already
                // but it won't have the id yet so we can't set that stuff up

                switch (i["Name"].ToString())
                {
                    case "TimeoutShort":
                        Timeout.Short = Convert.ToInt32(rowValue);
                        break;
                    
                    case "TimeoutMedium":
                        Timeout.Medium = Convert.ToInt32(rowValue);
                        break;
                    
                    case "TimeoutLong":
                        Timeout.Long = Convert.ToInt32(rowValue);
                        break;

                    case "Reviewers":
                        var reviewerLineItems = rowValue.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        Reviewers = reviewerLineItems;
                        break;

                    case "CdsNoteTemplate":
                        CdsNoteTemplate = rowValue;
                        break;

                    case "AudatexNoteTemplate":
                        AudatexNoteTemplate = rowValue;
                        break;

                    case "RepairTaskName":
                        RepairTaskName = rowValue;
                        break;

                    case "FilterProcessStatus":
                        FilterConfiguration.FilterProcessStatus = rowValue.ParseEnumFromDescription<FilterProcessStatus>();
                        break;

                    case "FilterNotifyStatus":
                        FilterConfiguration.FilterNotifyStatus = rowValue.ParseEnumFromDescription<FilterNotifyStatus>();
                        break;

                    case "FilterTransactionType":
                        FilterConfiguration.TransactionType = rowValue.ParseEnumFromDescription<TransactionType>();
                        break;

                    case "FilterTotalLoss":
                        FilterConfiguration.TotalLoss = rowValue.ParseEnumFromDescription<TotalLoss>();
                        break;

                    case "FilterLoginLevel":
                        FilterConfiguration.LoginLevel = rowValue.ParseEnumFromDescription<LoginLevel>();
                        break;
                    
                    case "FilterWorkgroup":
                        FilterConfiguration.Workgroup = rowValue.ParseEnumFromDescription<Workgroup>();
                        break;

                    case "FilterDateFrom":
                        FilterConfiguration.DateFrom = DateTime.Now.AddDays(Convert.ToInt32(rowValue));
                        break;

                    case "FilterDateTo":
                        FilterConfiguration.DateTo = DateTime.Now.AddDays(Convert.ToInt32(rowValue));
                        break;

                    case "Browser":
                        Browser = rowValue.ParseEnumFromDescription<Browser>();
                        break;

                    default:
                        break;
                }
            }
        }

        public override void Duplicate(Config config)
        {
            throw new NotImplementedException();
        }

        public override Config IngestConfigFile(string fileText)
        {
            throw new NotImplementedException();
        }
    }
}