using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.Audatex
{
    public class Comment
    {
        public string Subject { get; set; }
        public string Text { get; set; }
        public string Author { get; set; }
        public List<string> EmailedTo { get; set; } = new List<string>();
        public bool Private { get; set; }
        public DateTime Date { get; set; }
    }
}