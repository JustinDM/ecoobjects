using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.Audatex
{
    public class AudatexFilterConfiguration
    {
        public FilterProcessStatus FilterProcessStatus { get; set; }
        public FilterNotifyStatus FilterNotifyStatus { get; set; }
        public TransactionType TransactionType { get; set; }
        public TotalLoss TotalLoss { get; set; }
        public LoginLevel LoginLevel { get; set; }
        public Workgroup Workgroup { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}