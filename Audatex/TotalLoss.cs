using System.ComponentModel;


namespace Economical.EcoObjects.Audatex
{
    public enum TotalLoss
    {
        Invalid,
        [Description("Repairable Only")]
        RepairableOnly
    }
}