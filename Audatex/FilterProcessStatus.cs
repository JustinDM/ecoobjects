using System.ComponentModel;

namespace Economical.EcoObjects.Audatex
{
    public enum FilterProcessStatus
    {
        Invalid,
        [Description("Audited Only")]
        AuditedOnly,
        [Description("Completed Only")]
        CompletedOnly
    }
}