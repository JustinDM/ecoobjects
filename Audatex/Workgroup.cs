using System.ComponentModel;

namespace Economical.EcoObjects.Audatex
{
    public enum Workgroup
    {
        Invalid,
        [Description("ALL BC SHOPS")]
        AllBcShops,
        [Description("ATLANTIC PAYMENTS")]
        AtlanticPayments,
        [Description("ONT PAYMENTS")]
        OntPayments,
        [Description("WESTERN ALL CLAIMS")]
        WesternAllClaims
    }
}