using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.LogRepo
{
    public enum ValueType
    {
        Invalid,
        Handler,
        Config,
        Unit,
        UnitInfo,
        Trace
    }
}