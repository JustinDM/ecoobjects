using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.LogRepo
{
    public class UnitParent : Parent
    {
        public List<TraceParent> TraceParents { get; set; }
        public UnitInfoParent UnitInfoParent { get; set; }
        public Guid HandlerParentId { get; set; }
        public HandlerParent HandlerParent { get; set; }
    }
}