using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.LogRepo
{
    public class ConfigParent : Parent
    {
        public Guid HandlerParentId { get; set; }
        public HandlerParent HandlerParent { get; set; }
    }
}