using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.LogRepo
{
    public abstract class Parent
    {
        public Guid Id { get; set; }
        public String Data { get; set; }
    }
}