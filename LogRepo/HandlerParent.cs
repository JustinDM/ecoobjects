using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.LogRepo
{
    public class HandlerParent : Parent
    {
        public ConfigParent ConfigParent { get; set; }
        public List<UnitParent> UnitParents { get; set; }
        public List<TraceParent> TraceParents { get; set; }
    }
}