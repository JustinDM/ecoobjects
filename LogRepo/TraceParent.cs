using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.LogRepo
{
    public class TraceParent : Parent
    {
        public Nullable<Guid> UnitParentId { get; set; }
        public UnitParent UnitParent { get; set; }
        public Nullable<Guid> HandlerParentId { get; set; }
        public HandlerParent HandlerParent { get; set; }
    }
}