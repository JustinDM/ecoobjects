using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Economical.EcoObjects.LogRepo
{
    public class UnitInfoParent : Parent
    {
        public Guid UnitParentId { get; set; }
        public UnitParent UnitParent { get; set; }
    }
}