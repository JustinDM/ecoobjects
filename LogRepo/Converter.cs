using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Economical.EcoObjects.AutoCerf;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Models;
using Newtonsoft.Json;

namespace Economical.EcoObjects.LogRepo
{
    public static class Converter
    {
        private static JsonSerializerSettings _settings { get { return new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }; } }

        // When you create a parent - use its id
        public static HandlerParent ConvertToHandlerParent(Handler handler)
        {
            var parent = new HandlerParent();
            parent.Id = handler.Id;
            if (handler.Config != null)
            {
                parent.ConfigParent = ConvertToConfigParent(handler.Config);
                parent.ConfigParent.HandlerParent = parent;
                parent.ConfigParent.HandlerParentId = parent.Id;
            }
            
            parent.TraceParents = new List<TraceParent>();
            foreach (var i in handler.Traces)
            {
                var traceParent = ConvertToTraceParent(i);
                traceParent.HandlerParent = parent;
                traceParent.HandlerParentId = parent.Id;
                parent.TraceParents.Add(traceParent);
            }
            
            parent.UnitParents = new List<UnitParent>();

            foreach (var i in handler.Units)
            {
                var unitParent = ConvertToUnitParent(i);
                unitParent.HandlerParent = parent;
                unitParent.HandlerParentId = parent.Id;
                parent.UnitParents.Add(unitParent);
            }
            

            parent.Data = CreateHandlerDataString(handler);

            return parent;
        }

        private static string CreateHandlerDataString(Handler handler)
        {
            // can't overwrite the handler's children since that will just remove it entirely
            // need to go bottom up

            // blank any children
            handler.Config = null;
            handler.ConfigId = Guid.Empty;
            handler.Traces = null;
            handler.Units = null;

            var dataString = JsonConvert.SerializeObject(handler, _settings);

            return dataString;
        }

        private static UnitParent ConvertToUnitParent(Unit unit)
        {
            var parent = new UnitParent();
            parent.Id = unit.Id;
            var unitInfoParent = ConvertToUnitInfoParent(unit.UnitInfo);
            unitInfoParent.UnitParent = parent;
            unitInfoParent.UnitParentId = parent.Id;
            parent.UnitInfoParent = unitInfoParent;

            parent.TraceParents = new List<TraceParent>();
            foreach (var i in unit.Traces)
            {
                var traceParent = ConvertToTraceParent(i);
                traceParent.UnitParent = parent;
                traceParent.UnitParentId = parent.Id;
                parent.TraceParents.Add(traceParent);
            }

            parent.Data = CreateUnitDataString(unit);

            return parent;
        }

        private static string CreateUnitDataString(Unit unit)
        {
            unit.Traces = null;
            unit.Handler = null;
            unit.HandlerId = Guid.Empty;
            unit.UnitInfo = null;
            unit.UnitInfoId = Guid.Empty;

            var dataString = JsonConvert.SerializeObject(unit, _settings);

            return dataString;
        }

        private static TraceParent ConvertToTraceParent(Trace trace)
        {
            var parent = new TraceParent();
            parent.Id = trace.Id;

            parent.Data = CreateTraceDataString(trace);

            return parent;
        }

        private static string CreateTraceDataString(Trace trace)
        {
            trace.Handler = null;
            trace.HandlerId = Guid.Empty;
            trace.Unit = null;
            trace.UnitId = Guid.Empty;

            var dataString = JsonConvert.SerializeObject(trace, _settings);

            return dataString;
        }

        private static UnitInfoParent ConvertToUnitInfoParent(UnitInfo unitInfo)
        {
            var parent = new UnitInfoParent();
            parent.Id = unitInfo.Id;
            parent.Data = CreateUnitInfoDataString(unitInfo);

            return parent;
        }

        private static string CreateUnitInfoDataString(UnitInfo unitInfo)
        {
            unitInfo.Unit = null;
            unitInfo.UnitId = Guid.Empty;

            var dataString = JsonConvert.SerializeObject(unitInfo, _settings);

            return dataString;
        }

        private static ConfigParent ConvertToConfigParent(Config config)
        {
            var parent = new ConfigParent();
            parent.Id = config.Id;
            parent.Data = CreateConfigDataString(config);

            return parent;
        }

        private static string CreateConfigDataString(Config config)
        {
            config.Handler = null;
            config.HandlerId = Guid.Empty;

            var dataString = JsonConvert.SerializeObject(config, _settings);

            return dataString;
        }

        public static Handler ConvertToHandler(HandlerParent handlerParent)
        {
            var handler = new Handler();
            handler.Id = handlerParent.Id;

            handler = JsonConvert.DeserializeObject<Handler>(handlerParent.Data);

            if (handlerParent.ConfigParent != null)
            {
                handler.Config = ConvertToConfig(handler.Process, handlerParent.ConfigParent);
                handler.Config.Handler = handler;
                handler.Config.HandlerId = handler.Id;
            }

            
            handler.Units = new ObservableCollection<Unit>();

            foreach (var i in handlerParent.UnitParents)
            {
                var unit = ConvertToUnit(i);
                unit.Handler = handler;
                unit.HandlerId = handler.Id;
                handler.Units.Add(unit);
            }

            handler.Traces = new ObservableCollection<Trace>();

            if (handlerParent.TraceParents != null)
            {
                foreach (var i in handlerParent.TraceParents)
                {
                    var trace = ConvertToTrace(i);
                    trace.Handler = handler;
                    trace.HandlerId = handler.Id;
                    handler.Traces.Add(trace);                
                }
            }

            
            
            return handler;
        }

        private static Unit ConvertToUnit(UnitParent unitParent)
        {
            var unit = new Unit();
            unit.Id = unitParent.Id;

            unit = JsonConvert.DeserializeObject<Unit>(unitParent.Data);

            unit.UnitInfo = ConvertToUnitInfo(unitParent.UnitInfoParent, unit.Process);
            unit.UnitInfo.UnitId = unit.Id;
            unit.UnitInfo.Unit = unit;

            unit.Traces = new ObservableCollection<Trace>();

            if (unitParent.TraceParents != null)
            {
                foreach(var i in unitParent.TraceParents)
                {
                    var trace = ConvertToTrace(i);
                    trace.Unit = unit;
                    trace.UnitId = unit.Id;
                    unit.Traces.Add(trace);
                }
            }

            return unit;
        }

        private static Trace ConvertToTrace(TraceParent parent)
        {
            var trace = new Trace();
            trace.Id = parent.Id;

            trace = JsonConvert.DeserializeObject<Trace>(parent.Data);

            return trace;
        }

        private static UnitInfo ConvertToUnitInfo(UnitInfoParent parent, RpaProcess process)
        {
            UnitInfo unitInfo;

            switch (process)
            {
                case RpaProcess.AutoCerfPayments:
                    unitInfo = new AutoCerfUnitInfo();
                    unitInfo = JsonConvert.DeserializeObject<AutoCerfUnitInfo>(parent.Data);
                    break;
                default:
                    throw new Exception("ruh-roh");
            }

            unitInfo.Id = parent.Id;

            return unitInfo;
        }

        private static Config ConvertToConfig(RpaProcess process, ConfigParent configParent)
        {
            Config config;

            switch (process)
            {
                case RpaProcess.AutoCerfPayments:
                    config = new AutoCerfConfig();
                    config = JsonConvert.DeserializeObject<AutoCerfConfig>(configParent.Data);

                    break;

                default:
                    throw new Exception("maybe don't mess with this just yet");
                
            }

            config.Id = configParent.Id;

            // not redoing this everytime would require an abstraction


            return config;
        }

    }
}